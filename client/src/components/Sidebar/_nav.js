export default {
  items: [
    {
      name: 'Dashboard',
      url: '/admin',
      icon: 'icon-speedometer',
      badge: {
        variant: 'info',
        text: 'NEW'
      }
    },
    {
      title: true,
      name: 'Manage',
      wrapper: {            // optional wrapper object
        element: "span",      // required valid HTML5 element tag
        attributes: {}        // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: ""             // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: 'Account',
      url: '/admin/accounts',
      icon: 'icon-puzzle'
    },
    {
      name: 'Company',
      url: '/admin/companies',
      icon: 'icon-puzzle'
    },
    {
      name: 'Department',
      url: '/admin/departments',
      icon: 'icon-puzzle'
    },
    {
      name: 'Hospital',
      url: '/admin/hospitals',
      icon: 'icon-puzzle'
    },
    {
      name: 'Desease',
      url: '/admin/deseases',
      icon: 'icon-puzzle'
    },
    {
      name: 'Policy Holder',
      url: '/admin/policyholders',
      icon: 'icon-puzzle'
    },
    {
      name: 'Benefit',
      url: '/admin/benefits',
      icon: 'icon-puzzle'
    },
    {
      name: 'Policy',
      url: '',
      icon: 'icon-star',
      children: [
        {
          name: 'New business',
          url: '/admin/businesses',
          icon: 'icon-calculator'
        },
      ]
    }
    // {
    //   name: 'Icons',
    //   url: '/icons',
    //   icon: 'icon-star',
    //   children: [
    //     {
    //       name: 'Font Awesome',
    //       url: '/icons/font-awesome',
    //       icon: 'icon-star',
    //       badge: {
    //         variant: 'secondary',
    //         text: '4.7'
    //       }
    //     },
    //     {
    //       name: 'Simple Line Icons',
    //       url: '/icons/simple-line-icons',
    //       icon: 'icon-star'
    //     }
    //   ]
    // },
    // {
    //   name: 'Widgets',
    //   url: '/widgets',
    //   icon: 'icon-calculator',
    //   badge: {
    //     variant: 'info',
    //     text: 'NEW'
    //   }
    // },
    // {
    //   name: 'Charts',
    //   url: '/charts',
    //   icon: 'icon-pie-chart'
    // },


    // {
    //   divider: true
    // },
    // {
    //   title: true,
    //   name: 'Policy'
    // },
    // {
    //   name: 'Policy',
    //   url: '/admin/policies',
    //   icon: 'icon-star',
    //   children: [
    //     {
    //       name: 'Login',
    //       url: '/login',
    //       icon: 'icon-star'
    //     },
        // {
        //   name: 'Register',
        //   url: '/register',
        //   icon: 'icon-star'
        // },
        // {
        //   name: 'Error 404',
        //   url: '/404',
        //   icon: 'icon-star'
        // },
        // {
        //   name: 'Error 500',
        //   url: '/500',
        //   icon: 'icon-star'
        // }
    //   ]
    // }
  ]
};
