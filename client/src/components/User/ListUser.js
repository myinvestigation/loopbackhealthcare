import React from 'react';
import $ from 'jquery'
import {
  Badge,
  Row,
  Col,
  Card,
  CardHeader,
  CardBlock,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink
} from "reactstrap";

class App extends React.Component {
   constructor(props) {
      super(props);
      
      this.state = {
         data: []
      }
      this.updateState = this.updateState.bind(this);
      this.updateState();

   };
   // updateState(e) {
   //    this.setState({data: e.target.value});
   // }

   updateState() {
      console.log("updateState")
      this.setState({data: []})
      var this_tmp = this;

      $.ajax( {        
        url: "http://localhost:3000/api/accounts", 
        type: "GET",
        contentType: "application/json",
        dataType: "json",
        success: function(rs){
          this_tmp.setState({data:rs});
        }
      });

   };

   delete_user(e) {
      var id = e.target.attributes['data-id'].value;
      console.log("delete_user: "+ id);

      var this_tmp = this;

      $.ajax( {        
        url: "http://localhost:3000/api/accounts/"+id, 
        type: "DELETE",
        contentType: "application/json",
        dataType: "json",
        success: function(rs){
          // this_tmp.setState({data:rs});
          console.log(rs);
          if (rs.success){
            this_tmp.updateState();
          }
          else{
            alert("Delete error");
          }
        },
        error(e, rs){
          alert("Delete error");
        }
      });
   }
   render() {
      var this_t = this;
      return (

        <div className="animated fadeIn">
          <Row>
            <Col>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i> List Account
                  <button onClick={this.updateState} className="pull-right btn">Refresh</button>
                </CardHeader>
                <CardBlock className="card-body">
                  <Table hover bordered striped responsive size="sm">
                    <thead>
                    <tr>
                      <th>Username</th>
                      <th>Fullname</th>
                      <th>Email</th>
                      <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                      this.state.data.map(function(u){
                        return (
                        <tr>
                          <td>{u.username}</td>
                          <td>{u.fullname}</td>
                          <td>{u.email}</td>
                          <td>
                            <a className="badge badge-pill badge-danger" href="#" onClick={(e)=>this_t.delete_user(e)} data-id={u._id}>Delete</a>
                            <a className="badge badge-pill badge-info" href={"edit/"+u._id}>edit</a>
                          </td>
                        </tr>
                        );
                      })
                    }
                    </tbody>
                  </Table>
                  
                </CardBlock>
              </Card>
            </Col>
          </Row>
         </div>
      );
   }
}
export default App;