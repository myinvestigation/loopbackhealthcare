import { commonConstants } from '../constants';

export const policyholderService = {
    load_policyholders,
    delete_policyholder,
    create_policyholder,
    get_policyholder,
    update_policyholder
};


function load_policyholders(token) {
    console.log("service load_policyholders");
    
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
    };

    let url  = commonConstants.SERVER_URL + "policyholders?access_token="+token

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(policyholders => {
            console.log("Result load policyholders: ", policyholders)
            return policyholders;
        });
}

function delete_policyholder(token, id) {
    console.log("service delete_policyholder");
    const requestOptions = {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' }
    };

    let url  = commonConstants.SERVER_URL + "policyholders/"+id+"?access_token="+token

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(count => {
            return count;
        });
}

function create_policyholder (token, policyholder) {
    console.log("service create_policyholder");
    console.log(policyholder)

    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(policyholder)
    };

    let url  = commonConstants.SERVER_URL + "policyholders/?access_token="+token.id

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(policyholder => {
            return policyholder;
        });
}

function get_policyholder (token, id) {
    console.log("service get_policyholder");

    console.log("get_policyholder:", token, id)

    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
    };

    let url  = commonConstants.SERVER_URL + "policyholders/"+id+"/?access_token="+token.id

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(policyholder => {
            return policyholder;
        });
}

function update_policyholder (token, id, data) {
    console.log("service update_policyholder");

    const requestOptions = {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(data)
    };

    let url  = commonConstants.SERVER_URL + "policyholders/"+id+"/?access_token="+token.id

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(policyholder => {
            return policyholder;
        });
}