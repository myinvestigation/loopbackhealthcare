import { commonConstants } from '../constants';

export const hospitalService = {
    load_hospitals,
    delete_hospital,
    create_hospital,
    get_hospital,
    update_hospital
};


function load_hospitals(token) {
    console.log("service load_hospitals");
    
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
    };

    let url  = commonConstants.SERVER_URL + "hospitals?access_token="+token

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(hospitals => {
            console.log("Result load hospitals: ", hospitals)
            return hospitals;
        });
}

function delete_hospital(token, id) {
    console.log("service delete_hospital");
    const requestOptions = {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' }
    };

    let url  = commonConstants.SERVER_URL + "hospitals/"+id+"?access_token="+token

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(count => {
            return count;
        });
}

function create_hospital (token, hospital) {
    console.log("service create_hospital");
    console.log(hospital)

    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(hospital)
    };

    let url  = commonConstants.SERVER_URL + "hospitals/?access_token="+token.id

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(hospital => {
            return hospital;
        });
}

function get_hospital (token, id) {
    console.log("service get_hospital");

    console.log("get_hospital:", token, id)

    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
    };

    let url  = commonConstants.SERVER_URL + "hospitals/"+id+"/?access_token="+token.id

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(hospital => {
            return hospital;
        });
}

function update_hospital (token, id, data) {
    console.log("service update_hospital");

    const requestOptions = {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(data)
    };

    let url  = commonConstants.SERVER_URL + "hospitals/"+id+"/?access_token="+token.id

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(hospital => {
            return hospital;
        });
}