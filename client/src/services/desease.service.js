import { commonConstants } from '../constants';

export const deseaseService = {
    load_deseases,
    delete_desease,
    create_desease,
    get_desease,
    update_desease
};


function load_deseases(token) {
    console.log("service load_deseases");
    
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
    };

    let url  = commonConstants.SERVER_URL + "deseases?access_token="+token

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(deseases => {
            console.log("Result load deseases: ", deseases)
            return deseases;
        });
}

function delete_desease(token, id) {
    console.log("service delete_desease");
    const requestOptions = {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' }
    };

    let url  = commonConstants.SERVER_URL + "deseases/"+id+"?access_token="+token

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(count => {
            return count;
        });
}

function create_desease (token, desease) {
    console.log("service create_desease");
    console.log(desease)

    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(desease)
    };

    let url  = commonConstants.SERVER_URL + "deseases/?access_token="+token.id

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(desease => {
            return desease;
        });
}

function get_desease (token, id) {
    console.log("service get_desease");

    console.log("get_desease:", token, id)

    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
    };

    let url  = commonConstants.SERVER_URL + "deseases/"+id+"/?access_token="+token.id

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(desease => {
            return desease;
        });
}

function update_desease (token, id, data) {
    console.log("service update_desease");

    const requestOptions = {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(data)
    };

    let url  = commonConstants.SERVER_URL + "deseases/"+id+"/?access_token="+token.id

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(desease => {
            return desease;
        });
}