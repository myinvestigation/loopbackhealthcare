import { commonConstants } from '../constants';

function login(username, password) {
    console.log("service login: "+username);
    
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ "username": username, "password": password })
    };

    return fetch('http://localhost:3000/api/accounts/login', requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(user => {
            return user;
        });
}

function get_info(token, id) {
    console.log("get_info: "+id);
    
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
    };

    let url = 'http://localhost:3000/api/accounts/'+id+"?access_token="+token;

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(userInfo => {
            return userInfo;
        });
}

function logout(token) {
    console.log("service logout: "+token);
    
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' }
    };

    let url = commonConstants.SERVER_URL + 'accounts/logout?access_token='+token;

    return fetch(url, requestOptions)
        .then(response => {
            console.log(response)
            // if (!response.ok) { 
            //     console.log("Service not ok")
            //     return Promise.reject   (response.statusText);
            // }

            return response;
        })
}


export const authService = {
    login,
    logout,
    get_info
};