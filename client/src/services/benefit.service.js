import { commonConstants } from '../constants';

export const benefitService = {
    load_benefits,
    delete_benefit,
    create_benefit,
    get_benefit,
    update_benefit
};


function load_benefits(token) {
    console.log("service load_benefits");
    
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
    };

    let url  = commonConstants.SERVER_URL + "benefits?access_token="+token

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(benefits => {
            console.log("Result load benefits: ", benefits)
            return benefits;
        });
}

function delete_benefit(token, id) {
    console.log("service delete_benefit");
    const requestOptions = {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' }
    };

    let url  = commonConstants.SERVER_URL + "benefits/"+id+"?access_token="+token

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(count => {
            return count;
        });
}

function create_benefit (token, benefit) {
    console.log("service create_benefit");
    console.log(benefit)

    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(benefit)
    };

    let url  = commonConstants.SERVER_URL + "benefits/?access_token="+token.id

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(benefit => {
            return benefit;
        });
}

function get_benefit (token, id) {
    console.log("service get_benefit");

    console.log("get_benefit:", token, id)

    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
    };

    let url  = commonConstants.SERVER_URL + "benefits/"+id+"/?access_token="+token.id

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(benefit => {
            return benefit;
        });
}

function update_benefit (token, id, data) {
    console.log("service update_benefit");
    console.log("DATA: ", data)

    const requestOptions = {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(data)
    };

    let url  = commonConstants.SERVER_URL + "benefits/"+id+"/?access_token="+token.id

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(benefit => {
            return benefit;
        });
}