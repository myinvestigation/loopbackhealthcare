import { commonConstants } from '../constants';
export const policyPlansService = {
    getPlansByPolicy,
    addPlansPolicy,
    deletePolicyPlans
};


function getPlansByPolicy(token, policyId) {
    console.log("service getPlansPolicy");
    
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
    };

    let fillter = 'fillter={"where":{"policyId":"'+ policyId + '"}}';

    let url  = commonConstants.SERVER_URL + "policyPlans?"+fillter+"&access_token="+token.id

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(plans => {
            console.log("Result load plans policies: ", plans)
            return plans;
        });
}

function addPlansPolicy(token, plans) {
    console.log("service addPlansPolicy");
    console.log(plans)

    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(plans)
    };

    let url  = commonConstants.SERVER_URL + "policyPlans/?access_token="+token.id

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(plans => {
            return plans;
        });
}

function deletePolicyPlans(token, id) {
    console.log("service deletePolicyPlans");
    const requestOptions = {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' }
    };

    let url  = commonConstants.SERVER_URL + "policyPlans/"+id+"?access_token="+token.id

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(count => {
            return count;
        });
}
