import { commonConstants } from '../constants';

export const policyService = {
    load_policies,
    delete_policy,
    create_policy,
    get_policy,
    update_policy
};


function load_policies(token) {
    console.log("service load_policies");
    
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
    };

    let url  = commonConstants.SERVER_URL + "policies?access_token="+token

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(policies => {
            console.log("Result load policies: ", policies)
            return policies;
        });
}

function delete_policy(token, id) {
    console.log("service delete_policy");
    const requestOptions = {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' }
    };

    let url  = commonConstants.SERVER_URL + "policies/"+id+"?access_token="+token

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(count => {
            return count;
        });
}

function create_policy (token, policy) {
    console.log("service create_policy");
    console.log(policy)

    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(policy)
    };

    let url  = commonConstants.SERVER_URL + "policies/?access_token="+token.id

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(policy => {
            return policy;
        });
}

function get_policy (token, id) {
    console.log("service get_policy");

    console.log("get_policy:", token, id)

    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
    };

    let url  = commonConstants.SERVER_URL + "policies/"+id+"/?access_token="+token.id

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(policy => {
            return policy;
        });
}

function update_policy (token, id, data) {
    console.log("service update_policy");

    const requestOptions = {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(data)
    };

    let url  = commonConstants.SERVER_URL + "policies/"+id+"/?access_token="+token.id

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(policy => {
            return policy;
        });
}

