import { commonConstants } from '../constants';

export const companyService = {
    load_companies,
    delete_company,
    create_company,
    get_company,
    update_company
};


function load_companies(token) {
    console.log("service load_companies");
    
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
    };

    let url  = commonConstants.SERVER_URL + "companies?access_token="+token

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(companies => {
            console.log("Result load companies: ", companies)
            return companies;
        });
}

function delete_company(token, id) {
    console.log("service delete_company");
    const requestOptions = {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' }
    };

    let url  = commonConstants.SERVER_URL + "companies/"+id+"?access_token="+token

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(count => {
            return count;
        });
}

function create_company (token, company) {
    console.log("service create_company");
    console.log(company)

    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(company)
    };

    let url  = commonConstants.SERVER_URL + "companies/?access_token="+token.id

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(company => {
            return company;
        });
}

function get_company (token, id) {
    console.log("service get_company");

    console.log("get_company:", token, id)

    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
    };

    let url  = commonConstants.SERVER_URL + "companies/"+id+"/?access_token="+token.id

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(company => {
            return company;
        });
}

function update_company (token, id, data) {
    console.log("service update_company");

    const requestOptions = {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(data)
    };

    let url  = commonConstants.SERVER_URL + "companies/"+id+"/?access_token="+token.id

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(company => {
            return company;
        });
}