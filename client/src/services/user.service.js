import { commonConstants } from '../constants';

export const userService = {
    load_users,
    delete_user,
    create_user,
    get_user,
    update_user
};


function load_users(token) {
    console.log("service load_users");
    
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
    };

    let url  = commonConstants.SERVER_URL + "accounts?access_token="+token

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(users => {
            console.log("NGHIA2======")
            console.log(users)
            console.log(users[0].username)
            return users;
        });
}

function delete_user(token, id) {
    console.log("service delete_user");
    const requestOptions = {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' }
    };

    let url  = commonConstants.SERVER_URL + "accounts/"+id+"?access_token="+token

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(count => {
            return count;
        });
}

function create_user (token, user) {
    console.log("service create_user");

    console.log(user)

    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    let url  = commonConstants.SERVER_URL + "accounts/?access_token="+token

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(user => {
            return user;
        });
}

function get_user (token, user_id) {
    console.log("service create_user");

    console.log("GETUSER:", token, user_id)

    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
    };

    let url  = commonConstants.SERVER_URL + "accounts/"+user_id+"/?access_token="+token.id

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(user => {
            return user;
        });
}

function update_user (user_id, user) {
    console.log("service create_user");

    let token = JSON.parse(localStorage.getItem('user'));
    console.log("TOKEN:", token)

    const requestOptions = {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    let url  = commonConstants.SERVER_URL + "accounts/"+user_id+"/?access_token="+token.id

    return fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) { 
                return Promise.reject(response.statusText);
            }
            return response.json();
        })
        .then(user => {
            return user;
        });
}