import React from 'react';
import ReactDOM from 'react-dom';
import {HashRouter, Route, Router, Switch, Redirect, BrowserRouter} from 'react-router-dom';

// Styles
// Import Font Awesome Icons Set
import 'font-awesome/css/font-awesome.min.css';
  // Import Simple Line Icons Set
import 'simple-line-icons/css/simple-line-icons.css';
// Import Main styles for this application
import '../scss/style.scss'


// Views
import App from './admin';
import Auth from './auth_layout';

import rootReducer from './reducers';

import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { Provider } from 'react-redux'

const loggerMiddleware = createLogger();

const store = createStore(
    rootReducer,
    applyMiddleware(
        thunkMiddleware,
        loggerMiddleware
    )
);
const unsubscribe = store.subscribe(() =>
  console.log(store.getState())
)
console.log("==========================")
console.log(store.getState())
console.log("==========================")
let loggedIn = store.getState().authentication.loggedIn;

let AppRouter = null;


ReactDOM.render((
  <Provider store={store}>
    <HashRouter history={ history }>
            <Switch >
            <Route path="/admin/" name="App" component={App}/>
            <Route path="/auth/" name="Auth" component={Auth}/>
            <Redirect to="/admin/" />
            </Switch>
    </HashRouter>
  </Provider>
), document.getElementById('root'));