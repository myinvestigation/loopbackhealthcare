import React, { Component } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
// import './App.css';
import Login from './views/pages/login';

class App extends Component {
  constructor(props) {
    super(props)

    if (props.loggedIn) {
      console.log("Redirect to /admin")
      this.props.history.push("/admin")
    }
  };

  render() {
    return (
      <div className="App">
        <main className="main">
          <div>
             <Switch>
                <Route path='/auth/login' component={Login}/>
             </Switch>
          </div>
        </main>
      </div>
    );
  }
}

function mapStateToProps(state) {
    const { loggedIn} = state.authentication;
    return {
        loggedIn
    };
}

const Page = connect(mapStateToProps, null)(App);
export default Page; 