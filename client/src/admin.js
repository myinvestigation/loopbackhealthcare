import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { connect } from 'react-redux'; 
import { bindActionCreators } from 'redux';
import { authActions } from './actions';
// import './App.css';
import Header from './components/Header';
import Sidebar from './components/Sidebar'
import Breadcrumb from './components/Breadcrumb';
import Aside from './components/Aside/';
import Footer from './components/Footer/';

import ListUserPage from './views/pages/listUser/listUser';
import CompaniesListPage from './views/pages/company';
import DepartmentPage from './views/pages/department';
import HospitalPage from './views/pages/hospital';
import DeseasePage from './views/pages/desease';
import PolicyHolderPage from './views/pages/policyholder';
import BenefitPage from './views/pages/benefit';
import PolicyPage from './views/pages/business/policy';
import NewBusinessPage from './views/pages/business/newBusiness';

class App extends Component {

  constructor(props) {
      super(props);
      this.state = {
        is_login : false,
        user:{}
      }

      const {loggedIn, token, userInfo} = props;

      if (!loggedIn || !token || !token.id) {
        console.log("Redirect to /auth/login")
        this.props.history.push("/auth/login")
      }
      else if (!userInfo || !userInfo.id) {
        this.props.dispatch(authActions.get_info(token.id, token.userId))
      }

      this.logout = this.logout.bind(this)
   };

   componentWillUpdate (nextProps, nextState) {
    console.log("componentWillUpdate")
    var {loggedIn} = nextProps;

    console.log("PROPS}: ", nextProps)

    if (!loggedIn) {
      this.props.history.push("/auth/login")
    }

   }

   componentDidMount() {}

   logout() {
    console.log("LOGOUT")
    console.log(this.props)
    this.props.dispatch(authActions.logout(this.props.token.id))
   }

  render() {
    return (
      <div className="App">
        
        <Header logout={this.logout}/>
        <div className="app-body">
          <Sidebar {...this.props}/>
          <main className="main">
            <Breadcrumb />

                  <div>
                     <Switch>
                        <Route exact path='/admin/accounts' component={ListUserPage} />
                        <Route exact path='/admin/companies' component={CompaniesListPage} />
                        <Route exact path='/admin/departments' component={DepartmentPage} />
                        <Route exact path='/admin/hospitals' component={HospitalPage} />
                        <Route exact path='/admin/deseases' component={DeseasePage} />
                        <Route exact path='/admin/policyholders' component={PolicyHolderPage} />
                        <Route exact path='/admin/benefits' component={BenefitPage} />
                        <Route exact path='/admin/businesses' component={PolicyPage} />
                        <Route exact path='/admin/businesses/new' component={NewBusinessPage} />
                        <Route exact path='/admin/businesses/edit/:id' component={NewBusinessPage} />
                     </Switch>
                  </div>

          </main>
         </div>
        <Footer />
      </div>

    );
  }
}

function mapStateToProps(state) {
    const { loggedIn, user, userInfo } = state.authentication;
    return {
        loggedIn, token: user, userInfo
    };
}
function registActions (dispatch, props) {
  return {
    dispatch,
    ...bindActionCreators({
        ...authActions
      }, dispatch)
  }
}

const Page = connect(mapStateToProps, registActions)(App);
export default Page; 
