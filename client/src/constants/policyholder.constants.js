export const policyholderConstants = {
    LOAD_POLICYHOLDERS_SUCCESS: 'LOAD_POLICYHOLDERS_SUCCESS',
    LOAD_POLICYHOLDERS_FAILURE: 'LOAD_POLICYHOLDERS_FAILURE',
    DELETE_POLICYHOLDER_SUCCESS: 'DELETE_POLICYHOLDER_SUCCESS'
};
