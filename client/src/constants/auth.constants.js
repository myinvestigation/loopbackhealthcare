export const authConstants = {
    LOGIN_REQUEST: 'USERS_LOGIN_REQUEST',
    LOGIN_SUCCESS: 'USERS_LOGIN_SUCCESS',
    LOGIN_FAILURE: 'USERS_LOGIN_FAILURE',
    LOGOUT_SUCCESS: 'LOGOUT_SUCCESS',
    USER_INFO_REQUEST_SUCCESS: 'USER_INFO_REQUEST_SUCCESS',
    USER_INFO_REQUEST_FAILURE: 'USER_INFO_REQUEST_FAILURE',
};
