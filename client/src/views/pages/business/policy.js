import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'; 
import $ from 'jquery'
import { Link } from 'react-router-dom';
import {
  Badge,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink
} from "reactstrap";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";
// var reactNotice = require('react-notice-message');
import { ToastContainer, toast } from 'react-toastify';


import { policyActions } from '../../../actions';

import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';

// import EditForm from './editForm'

class PoliciesList extends React.Component {

   constructor(props) {
      super(props);
      
      this.state = {
         policies: []
      }
      this.loadDataList = this.loadDataList.bind(this);
      this.loadDataList();
      console.log("TAB POLICY")
   };

  componentWillReceiveProps(nextProp) {
    console.log("componentWillReceiveProps");
    var policies = nextProp.policies || [];
    var this_t = this;
    console.log(policies)
    policies.map(function(u){
      u.context = {
        this_t: this_t
      };
      u.clientCode = u.header?u.header.clientCode:"";
      u.clientName = u.header?u.header.clientName:"";
      u.clientAddress = u.header?u.header.clientAddress:"";
      u.effectiveDate = u.header?u.header.effectiveDate:"";
    })
    this.setState({policies})
  }


  loadDataList() {
    console.log("loadDataList");

    this.props.dispatch(policyActions.load_policies(this.props.token));
    // reactNotice.show(<p>messages</p>, {closeTime:"3000"});
  }

  delete_policy(e, id) {
      if (!confirm("You are sure?")) {
        return;
      }
      if (!id)
        return;
      this.props.dispatch(policyActions.delete_policy(this.props.token, id, this.deleteCallback.bind(this)));
      
  }

   deleteCallback(success, rs) {
      if (success) {
        toast("Deleting successfully", { autoClose: 3000 ,type: toast.TYPE.SUCCESS})
      }
      else {
        console.log("show notify error")
        toast("Deleting failure", { autoClose: 3000 ,type: toast.TYPE.ERROR})
      }
   }


   editFormatter(cell, row) {
    var {context} = row
    return (
      <div>
        <a className="badge badge-pill badge-danger" onClick={(e)=>context.this_t.delete_policy(e, row.id)} data-id={row.id}>Delete</a>
        <Link  to={"/admin/businesses/edit/"+row.id} className="badge badge-pill badge-info" >Edit</Link>
      </div>
    );
   }
   // render
   render() {
      var this_t = this;
      var { token, userInfo } = this.props;
      var { policies } = this.state;

      return (

        <div className="animated fadeIn">
          <Row>
            <Col>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i> Companies list
                  <button className="pull-right btn">Refresh</button>
                  <Link  to="/admin/businesses/new" className="btn btn-primary active pull-right">Add Policy</Link>

                </CardHeader>
                <CardBody className="card-body">
                  <BootstrapTable data={policies} hover striped className=" table-sm">
                      <TableHeaderColumn isKey dataField='id' hidden={true}>id</TableHeaderColumn>
                      <TableHeaderColumn dataField='policyCode' filter={ { type: 'TextFilter' } }>Policy Code</TableHeaderColumn>
                      <TableHeaderColumn dataField='clientCode' filter={ { type: 'TextFilter' } }>Client Code</TableHeaderColumn>
                      <TableHeaderColumn dataField='clientName' filter={ { type: 'TextFilter' } }>Client Name</TableHeaderColumn>
                      <TableHeaderColumn dataField='effectiveDate' filter={ { type: 'TextFilter' } }>Effective Date</TableHeaderColumn>
                      <TableHeaderColumn dataField='policyHolderAddress' filter={ { type: 'TextFilter' } }>Address</TableHeaderColumn>
                      <TableHeaderColumn dataFormat={ this.editFormatter }>Operations</TableHeaderColumn>
                  </BootstrapTable>

                </CardBody>
              </Card>
            </Col>
          </Row>


          <ToastContainer autoClose={5000} />
         </div>
      );
   }
}




function mapStateToProps(state) {
    const { policies, error_msg } = state.policy;
    const { user, userInfo } = state.authentication;
    return {
        policies, error_msg,
        token: user, userInfo
    };
}
function registActions (dispatch, props) {
  return {
    dispatch,
    ...bindActionCreators({
        ...policyActions
      }, dispatch)
  }
}

const Page = connect(mapStateToProps, registActions)(PoliciesList);
export default Page; 