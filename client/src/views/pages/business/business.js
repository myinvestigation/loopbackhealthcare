import React from 'react';
import { 
  TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col,
  CardHeader, CardBody
} from 'reactstrap';
import classnames from 'classnames';
import PolicyList from "./policy";
import NewBusiness from "./newBusiness";

export default class Example extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: '1'
    };
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }
  render() {
    return (

      <div className="animated fadeIn">
          <Row>
            <Col>
              <Card>
                <CardBody className="card-body">


                  <div>
                    <Nav tabs>
                      <NavItem>
                        <NavLink
                          className={classnames({ active: this.state.activeTab === '1' })}
                          onClick={() => { this.toggle('1'); }}
                        >
                          Policy list
                        </NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink
                          className={classnames({ active: this.state.activeTab === '2' })}
                          onClick={() => { this.toggle('2'); }}
                        >
                          New Business
                        </NavLink>
                      </NavItem>
                    </Nav>
                    <TabContent activeTab={this.state.activeTab}>
                      <TabPane tabId="1">
                        <Row>
                          <Col sm="12">
                            <PolicyList/>
                          </Col>
                        </Row>
                      </TabPane>
                      <TabPane tabId="2">
                        <Row>
                          <Col sm="12">
                            <NewBusiness/>
                          </Col>
                        </Row>
                      </TabPane>
                    </TabContent>
                  </div>

                  
                </CardBody>
              </Card>
            </Col>
          </Row>


         </div>



      
    );
  }
}