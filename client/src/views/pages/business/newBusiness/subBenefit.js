import React from 'react';
import { Link } from 'react-router-dom';
import {
  Badge,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink
} from "reactstrap";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";
// var reactNotice = require('react-notice-message');
import { ToastContainer, toast } from 'react-toastify';


import { benefitActions } from '../../../../actions';

import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';

// import EditForm from './editForm'
// import AddEditForm from './form'

class SubBenefit extends React.Component {

   constructor(props) {
      super(props);
      
      this.state = {
         benefits: this.props.subBenefits || [],
         plans: this.props.plans || []
      }

      console.log("SUBBENEFIT: props: ", this.props)
   };


   showEditSubForm(rootBenefit, row) {
    console.log("SUBBENEFIT: showEditSubForm()")
    row.context.this_t.props.parent.showEditSubForm(rootBenefit, row)
   }

  componentWillReceiveProps(nextProp) {
    console.log("componentWillReceiveProps");
    var benefits = nextProp.subBenefits || []
    this.setState({benefits})
  }

  editFormatter(cell, row) {
    var rootBenefit = row.context.this_t.props.benefit;
    var parent = row.context.this_t.props.parent;
    return (

      <div>
      <a className="badge badge-pill badge-danger" onClick={(e)=>parent.delete_benefit(e, row)}>Delete</a>
      <a className="badge badge-pill badge-info" onClick={(e)=>parent.showEditForm(e, row)} >Edit</a>
      </div>
    );
  }

  // format for plan
   planFormatter(cell, row, extra) {
      // console.log("editFormatter", cell, row, extra)
      var plan = {}
      row.plans.map(function(p, key) {
        if (p.code == extra.planCode)
          plan = p;
      })
      var preminum = plan.preminum || "";
      var sum = plan.sumInsured || ""
      return (preminum+ " - "+sum)
   }

   render() {
      
      var context = {
        this_t: this
      };
      // var { token, userInfo } = this.props;
      var { benefits, state, plans } = this.state;
      
      benefits.forEach(function(u, key){
        u.context = {...context};
        u.context.key = key
        // u.nameEn = u.name.en;
        // u.nameVn = u.name.vn;
      })

      return (

        <div className="animated fadeIn">
          <Row>
            <Col>
              <Card>
                
                <CardBody className="card-body">
                  <BootstrapTable data={benefits} hover striped className=" table-sm"
                  >
                      
                      <TableHeaderColumn isKey dataField='code' hidden={true}>Code</TableHeaderColumn>
                      <TableHeaderColumn dataField='nameEn'>Name En</TableHeaderColumn>
                      <TableHeaderColumn dataField='nameVn'>Name Vn</TableHeaderColumn>
                      {
                        plans.map(function(plan, key) {
                          return (
                            <TableHeaderColumn key={key} dataFormat={ context.this_t.planFormatter }
                              formatExtraData={{planCode:plan.code}}
                            >
                            {plan.name}</TableHeaderColumn>
                          )
                        })
                      }

                      <TableHeaderColumn editable={false} dataFormat={ this.editFormatter }>Edit</TableHeaderColumn>
                  </BootstrapTable>
                  
                </CardBody>
              </Card>
            </Col>
          </Row>

         </div>
      );
   }
}

export default SubBenefit; 