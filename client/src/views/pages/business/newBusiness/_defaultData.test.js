export default {
  benefitConfig: {
    product:"",
    numberPlan: "",
    plans: [
      {
        name: "Plan 1", code:"plan1"
      },
      {
        name: "Plan 2", code:"plan2"
      }
    ],
    benefitGroups: [
      {
        groupName: "Medical Expenses due to Accident 1111111",
        groupType: "basic",
        // path: [0],
        benefits: [
          {
            // index: 0,
            // path:[0, "benefits", 0],
            code: "B001",
            nameEn: "noi dung tieng anh 1",
            nameVn: "noi dung tieng viet 1",
            preminum: "",
            sumInsured: "",
            subBenefits: [
              {
                // index: 0,
                // path: [0, "benefits", 0, "subBenefits", 0],
                code: "B001",
                nameEn: "noi dung tieng anh 2",
                nameVn: "noi dung tieng viet 2",
                preminum: "",
                sumInsured: "",
                subBenefits: [
                  
                  
                ],
                plans:[
                  {code: "plan1", preminum: 10, sumInsured: 1000},
                  {code: "plan2", preminum: 20, sumInsured: 1000},
                ]
              },
              {
                // index: 1,
                // path:[0, "benefits", 0, "subBenefits", 1],
                code: "B001",
                nameEn: "noi dung tieng anh 3",
                nameVn: "noi dung tieng viet 3",
                preminum: "",
                sumInsured: "",
                subBenefits: [],
                plans:[
                  {code: "plan1", preminum: 220, sumInsured: 1000},
                  {code: "plan2", preminum: 33, sumInsured: 1000},
                ]
              }
              
            ],
            plans:[
              {code: "plan1", preminum: 10, sumInsured: 1000},
              {code: "plan2", preminum: 20, sumInsured: 1000},
            ]
          },
          {
            // index: 1,
            // path:[0, "benefits", 1],
            code: "B001",
            nameEn: "noi dung tieng anh 4",
            nameVn: "noi dung tieng viet 4",
            preminum: "",
            sumInsured: "",
            subBenefits: [],
            plans:[
              {code: "plan1", preminum: 220, sumInsured: 1000},
              {code: "plan2", preminum: 33, sumInsured: 1000},
            ]
          }
        ]

      },
      {
        groupName: "Medical Expenses due to Accident 22222",
        groupType: "additional",
        // path: [1],
        benefits: [
          {
            index: 0,
            path: [1, "benefits", 0],
            code: "B001",
            nameEn: "noi dung tieng anh 5",
            nameVn: "noi dung tieng viet 5",
            preminum: "",
            sumInsured: "",
            subBenefits: [],
            plans:[
              {code: "plan1", preminum: 43, sumInsured: 1000},
              {code: "plan2", preminum: 56, sumInsured: 1000},
            ]
          },
          {
            // index: 1,
            // path: [1, "benefits", 1],
            code: "B001",
            nameEn: "noi dung tieng anh 6",
            nameVn: "noi dung tieng viet 6",
            preminum: "",
            sumInsured: "",
            subBenefits: [],
            plans:[
              {code: "plan1", preminum: 34, sumInsured: 1000},
              {code: "plan2", preminum: 55, sumInsured: 1000},
            ]
          }
        ]
      }
    ]
  },
  premium: {
    currency: "USD",
    exchangeRate: 22000,
    discountRate: 5,
    premiumMode: "Monthly",
    sumAssured: {
      USD: 1000,
      VND: 20000
    },
    grossPremium: {
      USD: 2000,
      VND: 12000
    },
    discountAmount: {
      USD: 1000,
      VND: 10000
    },
    netPremium: {
      USD: 1000,
      VND: 20000
    },
    premiumNote: {
      en: "",
      vn: ""
    },
    exchangeRateNote: {
      en: "",
      vn: ""
    },
    premiumTermNote: {
      en: "",
      vn: ""
    }
  },
  termCondition: {
    waitingPeriod:{
      en: "",
      vn:""
    },
    termCondition: {
      en: "",
      vn: ""
    },
    territorical: {
      en: "",
      vn: ""
    }
  },
  clauses: [
    {
      clauseType: 1,
      en: "clause 1 en",
      vn: "clause 1 en"
    },
    {
      clauseType: 1,
      en: "clause 2 en",
      vn: "clause 2 en"
    },
    {
      clauseType: 2,
      en: "clause 3 en",
      vn: "clause 3 en"
    },
    {
      clauseType: 2,
      en: "clause 4 en",
      vn: "clause 4 en"
    },
  ]
 };