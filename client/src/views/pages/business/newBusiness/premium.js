import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'; 
import { policyActions } from '../../../../actions';
import { Link } from 'react-router-dom';

import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBlock,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton
} from "reactstrap";

class PremiumForm extends React.Component {
  constructor(props) {
      super(props);

      var defaultData = {
        currency: "USD",
        exchangeRate: "",
        discountRate: "",
        premiumMode: "Monthly",
        sumAssured: {
          USD: "",
          VND: ""
        },
        grossPremium: {
          USD: "",
          VND: ""
        },
        discountAmount: {
          USD: "",
          VND: ""
        },
        netPremium: {
          USD: "",
          VND: ""
        },
        premiumNote: {
          en: "",
          vn: ""
        },
        exchangeRateNote: {
          en: "",
          vn: ""
        },
        premiumTermNote: {
          en: "",
          vn: ""
        }
       };
      
      this.state = {
        data: {
          ...defaultData
        },
        dataDefault: {
          ...defaultData
        },
        errorField:{},
        error_msg:"",
        message:"",
        isSync: true
      }
      this.changeHandle = this.changeHandle.bind(this)
      this.changeHandle2 = this.changeHandle2.bind(this)
      this.reset = this.reset.bind(this)
   };

  componentWillUpdate(nextProp, nextState) {
    console.log("Premium componentWillUpdate")
    if (!nextState.isSync) {
      var state = {
        ...nextState,
        isSync: true
      }

      this.setState(state);
      var policy = nextProp.currentPolicy;
      policy.premium = nextState.data;
      this.props.dispatch(policyActions.setCurrentPolicy(policy))
    }
  }

  componentWillReceiveProps(nextProp) {
    console.log("Premium componentWillReceiveProps")

    var data = nextProp.currentPolicy.premium;
    if (!data)
      data = this.state.dataDefault;

    this.setState({data:data})
  }



   changeHandle(e) {
      console.log(this.state)
      var requireField = ["name"];
      const { name, value } = e.target;
      var data = this.state.data;
      var errorField = this.state.errorField

      data[name] = value;
      errorField[name] = (requireField.indexOf(name) != -1 && value == "")

      this.setState({ ...this.state, data , errorField, isSync:false});
   }

   changeHandle2(e, child) {
      console.log(this.state)
      var requireField = ["name"];
      const { name, value } = e.target;
      var data = this.state.data;
      var errorField = this.state.errorField

      data[name][child] = value;
      if (!errorField[name])
        errorField[name] = {};
      errorField[name][child] = (requireField.indexOf(name) != -1 && value == "")

      this.setState({ ...this.state, data , errorField, isSync:false});
   }

   checkValidate() {
    var errorField = this.state.errorField;
    var data = this.state.data;
    var rs = true;

    ["name"].forEach(function(name) {
      let err = (data[name] == "");
      errorField[name] = err;
      rs &= !err;
    })

    this.setState({data, errorField})
    return rs;
   }

   reset() {
    console.log(this.state)
    this.setState({
      dataDefault: {...this.state.dataDefault},
      data: {...this.state.dataDefault},
      errorField: {}
    })

   }

   render() {
      var {data, errorField} = this.state;
      var {error_msg, message} = this.state;

      return (
         <div>
                <Form action="" method="post">
                  {
                    error_msg? (<div className="alert alert-danger">{error_msg}</div>):("")
                  }
                  {
                    message? (<div className="alert alert-primary">{message}</div>):("")
                  }

                  <Row>
                    <Col xs="12" md="4">
                      <h5>Premiums & Sum Assured</h5>

                      <FormGroup row>
                        <Col md="6">
                          <Label htmlFor="currency">Currency(*)</Label>
                        </Col>
                        <Col xs="12" md="6">
                          <Input type="select" id="currency" name="currency" value={data.currency} 
                          onChange={this.changeHandle} className={errorField.currency?"is-invalid":""}>
                            <option value="USD">USD</option>
                            <option value="VND">VND</option>
                          </Input>
                        </Col>
                      </FormGroup>


                      <FormGroup row>
                        <Col md="6">
                          <Label htmlFor="exchangeRate">Exchange rate (*)</Label>
                        </Col>
                        <Col xs="12" md="6">
                          <Input type="number" id="exchangeRate" name="exchangeRate" value={data.exchangeRate}
                          onChange={this.changeHandle} className={errorField.exchangeRate?"is-invalid":""}/>
                          {
                            errorField.exchangeRate?(<FormText color="muted">Please enter Exchange rate</FormText>):("")
                          }
                        </Col>
                      </FormGroup>

                      <FormGroup row>
                        <Col md="6">
                          <Label htmlFor="discountRate">Discount (%)</Label>
                        </Col>
                        <Col xs="12" md="6">
                          <Input type="number" id="discountRate" name="discountRate" value={data.discountRate}
                          onChange={this.changeHandle} className={errorField.discountRate?"is-invalid":""}/>
                          {
                            errorField.discountRate?(<FormText color="muted">Please enter Discount rate</FormText>):("")
                          }
                        </Col>
                      </FormGroup>

                      <FormGroup row>
                        <Col md="6">
                          <Label htmlFor="premiumMode">Premium mode</Label>
                        </Col>
                        <Col xs="12" md="6">
                          <Input type="text" id="premiumMode" name="premiumMode" value={data.premiumMode}
                          onChange={this.changeHandle} className={errorField.premiumMode?"is-invalid":""}/>
                          {
                            errorField.premiumMode?(<FormText color="muted">Please enter Premium mode</FormText>):("")
                          }
                        </Col>
                      </FormGroup>
                    </Col>

                    <Col xs="12" md="8">
                      <h5>Sum Assured</h5>
                      <Row>
                        <Col md="6"/>
                        <Col md="3">USD</Col>
                        <Col md="3">VND</Col>
                      </Row>

                      <FormGroup row>
                        <Col md="6">
                          <Label htmlFor="grossPremium">Gross Premium (before discount)</Label>
                        </Col>
                        <Col xs="12" md="3">
                          <Input type="number" name="grossPremium" value={data.grossPremium.USD}
                          onChange={(e)=>this.changeHandle2(e,"USD")} className={errorField.grossPremium&&errorField.grossPremium.USD?"is-invalid":""}/>
                        </Col>
                        <Col xs="12" md="3">
                          <Input type="number" name="grossPremium" value={data.grossPremium.VND}
                          onChange={(e)=>this.changeHandle2(e,"VND")} className={errorField.grossPremium&&errorField.grossPremium.VND?"is-invalid":""}/>
                        </Col>
                      </FormGroup>

                      <FormGroup row>
                        <Col md="6">
                          <Label htmlFor="discountAmount">Discount Amount </Label>
                        </Col>
                        <Col xs="12" md="3">
                          <Input type="number" name="discountAmount" value={data.discountAmount.USD}
                          onChange={(e)=>this.changeHandle2(e,"USD")} className={errorField.discountAmount&&errorField.discountAmount.USD?"is-invalid":""}/>
                        </Col>
                        <Col xs="12" md="3">
                          <Input type="number" name="discountAmount" value={data.discountAmount.VND}
                          onChange={(e)=>this.changeHandle2(e,"VND")} className={errorField.discountAmount&&errorField.discountAmount.VND?"is-invalid":""}/>
                        </Col>
                      </FormGroup>

                      <FormGroup row>
                        <Col md="6">
                          <Label htmlFor="netPremium">Net Premium (after discount)</Label>
                        </Col>
                        <Col xs="12" md="3">
                          <Input type="number" name="netPremium" data-curency="USD" value={data.netPremium.USD}
                          onChange={(e)=>this.changeHandle2(e,"USD")} className={errorField.netPremium&&errorField.netPremium.USD?"is-invalid":""}/>
                        </Col>
                        <Col xs="12" md="3">
                          <Input type="number" name="netPremium" data-curency="VND" value={data.netPremium.VND}
                          onChange={(e)=>this.changeHandle2(e,"VND")} className={errorField.netPremium&&errorField.netPremium.VND?"is-invalid":""}/>
                        </Col>
                      </FormGroup>

                    </Col>
                  </Row>

                  <FormGroup row>
                    <Col md="12">
                      <Label htmlFor="premiumNote">Notes of Premium</Label>
                    </Col>
                    <Col xs="12" md="6">
                      <Input type="textarea" name="premiumNote" value={data.premiumNote.en} 
                      onChange={(e)=>this.changeHandle2(e,"en")} placeholder="English.." />
                    </Col>
                    <Col xs="12" md="6">
                      <Input type="textarea" name="premiumNote" value={data.premiumNote.vn} 
                      onChange={(e)=>this.changeHandle2(e,"vn")} placeholder="Vietnamese.." />
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="12">
                      <Label htmlFor="exchangeRateNote">Notes of Exchange rates</Label>
                    </Col>
                    <Col xs="12" md="6">
                      <Input type="textarea" name="exchangeRateNote" value={data.exchangeRateNote.en} 
                      onChange={(e)=>this.changeHandle2(e,"en")} placeholder="English.." />
                    </Col>
                    <Col xs="12" md="6">
                      <Input type="textarea" name="exchangeRateNote" value={data.exchangeRateNote.vn} 
                      onChange={(e)=>this.changeHandle2(e,"vn")} placeholder="Vietnamese.." />
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="12">
                      <Label htmlFor="premiumTermNote">Premium term</Label>
                    </Col>
                    <Col xs="12" md="6">
                      <Input type="textarea" name="premiumTermNote" value={data.premiumTermNote.en} 
                      onChange={(e)=>this.changeHandle2(e,"en")} placeholder="English.." />
                    </Col>
                    <Col xs="12" md="6">
                      <Input type="textarea" name="premiumTermNote" value={data.premiumTermNote.vn} 
                      onChange={(e)=>this.changeHandle2(e,"vn")} placeholder="Vietnamese.." />
                    </Col>
                  </FormGroup>

                </Form>
    
         </div>
      );
   }
}


function mapStateToProps(state) {
    const { currentPolicy } = state.policy;
    const { user, userInfo } = state.authentication;
    return {
        currentPolicy,
        token: user, userInfo
    };
}
function registActions (dispatch, props) {
  return {
    dispatch,
    ...bindActionCreators({
        ...policyActions
      }, dispatch)
  }
}

const Page = connect(mapStateToProps, null)(PremiumForm);
export default Page; 