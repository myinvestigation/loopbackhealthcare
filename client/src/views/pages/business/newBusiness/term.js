import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'; 
import { policyActions } from '../../../../actions';
import { Link } from 'react-router-dom';

import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBlock,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton
} from "reactstrap";

class PremiumForm extends React.Component {
  constructor(props) {
      super(props);

      var defaultData = {
        waitingPeriod:{
          en: "",
          vn:""
        },
        termCondition: {
          en: "",
          vn: ""
        },
        territorical: {
          en: "",
          vn: ""
        }
       };
      
      this.state = {
        data: {
          ...defaultData
        },
        dataDefault: {
          ...defaultData
        },
        errorField:{},
        error_msg:"",
        message:"",
        isSync: true
      }
      this.changeHandle = this.changeHandle.bind(this)
      this.changeHandle2 = this.changeHandle2.bind(this)
      this.reset = this.reset.bind(this)
   };

  componentWillUpdate(nextProp, nextState) {
    console.log("Premium componentWillUpdate")
    if (!nextState.isSync) {
      var state = {
        ...nextState,
        isSync: true
      }

      this.setState(state);
      var policy = nextProp.currentPolicy;
      policy.termCondition = nextState.data;
      this.props.dispatch(policyActions.setCurrentPolicy(policy))
    }
  }

  componentWillReceiveProps(nextProp) {
    console.log("Premium componentWillReceiveProps")

    var data = nextProp.currentPolicy.termCondition;
    if (!data)
      data = this.state.dataDefault;

    this.setState({data:data})
  }



   changeHandle(e) {
      console.log(this.state)
      var requireField = ["name"];
      const { name, value } = e.target;
      var data = this.state.data;
      var errorField = this.state.errorField

      data[name] = value;
      errorField[name] = (requireField.indexOf(name) != -1 && value == "")

      this.setState({ ...this.state, data , errorField, isSync:false});
   }

   changeHandle2(e, child) {
      console.log(this.state)
      var requireField = ["name"];
      const { name, value } = e.target;
      var data = this.state.data;
      var errorField = this.state.errorField

      data[name][child] = value;
      if (!errorField[name])
        errorField[name] = {};
      errorField[name][child] = (requireField.indexOf(name) != -1 && value == "")

      this.setState({ ...this.state, data , errorField, isSync:false});
   }

   checkValidate() {
    var errorField = this.state.errorField;
    var data = this.state.data;
    var rs = true;

    ["name"].forEach(function(name) {
      let err = (data[name] == "");
      errorField[name] = err;
      rs &= !err;
    })

    this.setState({data, errorField})
    return rs;
   }

   reset() {
    console.log(this.state)
    this.setState({
      dataDefault: {...this.state.dataDefault},
      data: {...this.state.dataDefault},
      errorField: {}
    })

   }

   render() {
      var {data, errorField} = this.state;
      var {error_msg, message} = this.state;

      return (
         <div>
                <Form action="" method="post">
                  {
                    error_msg? (<div className="alert alert-danger">{error_msg}</div>):("")
                  }
                  {
                    message? (<div className="alert alert-primary">{message}</div>):("")
                  }

                  <FormGroup row>
                    <Col md="12">
                      <Label htmlFor="waitingPeriod">Waiting periods (*)</Label>
                    </Col>
                    <Col xs="12" md="6">
                      <Input type="textarea" name="waitingPeriod" value={data.waitingPeriod.en} 
                      onChange={(e)=>this.changeHandle2(e,"en")} placeholder="English.." className={errorField.waitingPeriod&&errorField.waitingPeriod.en?"is-invalid":""} />
                    </Col>
                    <Col xs="12" md="6">
                      <Input type="textarea" name="waitingPeriod" value={data.waitingPeriod.vn} 
                      onChange={(e)=>this.changeHandle2(e,"vn")} placeholder="Vietnamese.." className={errorField.waitingPeriod&&errorField.waitingPeriod.vn?"is-invalid":""} />
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="12">
                      <Label htmlFor="termCondition">Term and Conditions (*)</Label>
                    </Col>
                    <Col xs="12" md="6">
                      <Input type="textarea" name="termCondition" value={data.termCondition.en} 
                      onChange={(e)=>this.changeHandle2(e,"en")} placeholder="English.." className={errorField.termCondition&&errorField.termCondition.en?"is-invalid":""} />
                    </Col>
                    <Col xs="12" md="6">
                      <Input type="textarea" name="termCondition" value={data.termCondition.vn} 
                      onChange={(e)=>this.changeHandle2(e,"vn")} placeholder="Vietnamese.." className={errorField.termCondition&&errorField.termCondition.vn?"is-invalid":""} />
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="12">
                      <Label htmlFor="territorical">Territorical limit (*)</Label>
                    </Col>
                    <Col xs="12" md="6">
                      <Input type="textarea" name="territorical" value={data.territorical.en} 
                      onChange={(e)=>this.changeHandle2(e,"en")} placeholder="English.." className={errorField.territorical&&errorField.territorical.en?"is-invalid":""} />
                    </Col>
                    <Col xs="12" md="6">
                      <Input type="textarea" name="territorical" value={data.territorical.vn} 
                      onChange={(e)=>this.changeHandle2(e,"vn")} placeholder="Vietnamese.." className={errorField.territorical&&errorField.territorical.vn?"is-invalid":""} />
                    </Col>
                  </FormGroup>

                </Form>
    
         </div>
      );
   }
}


function mapStateToProps(state) {
    const { currentPolicy } = state.policy;
    const { user, userInfo } = state.authentication;
    return {
        currentPolicy,
        token: user, userInfo
    };
}
function registActions (dispatch, props) {
  return {
    dispatch,
    ...bindActionCreators({
        ...policyActions
      }, dispatch)
  }
}

const Page = connect(mapStateToProps, null)(PremiumForm);
export default Page; 