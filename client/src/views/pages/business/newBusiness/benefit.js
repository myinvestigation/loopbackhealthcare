import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'; 
import { policyActions } from '../../../../actions';
import { Link } from 'react-router-dom';
import { companyService } from '../../../../services'

import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBlock,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton,
  Modal, ModalHeader, ModalBody, ModalFooter
} from "reactstrap";

import AddEditForm from './formBenefit'
import SubBenefit from './subBenefit'

class PolicyBenefits extends React.Component {
  constructor(props) {
      super(props);

      var defaultData = {
        plans: [],
        benefitGroups: [],
        product:"",
        numberPlan: 1
      }
      
      this.state = {
        data: {
          ...defaultData
        },
        dataDefault: {
          ...defaultData
        },
        form: {
          isShow: false,
          type: "add",
          id: ""
        },
        errorField:{},
        error_msg:"",
        message:"",
        generated: false,
        isSync: false
      }
      this.changeHandle = this.changeHandle.bind(this);
      this.reset = this.reset.bind(this);
      this.toggleForm = this.toggleForm.bind(this);
      // this.calculatePath = this.calculatePath.bind(this);
      // this.calculate = this.calculatePath;
      console.log("INIT BENEFIT")
      this.state.data = props.currentPolicy.benefitConfig;
      
   };

  calculatePath(state, parentPath, pointer) {
    console.log("CALCULATE", parentPath, pointer)
    var this_t = this;
    pointer.map(function(be, idx){
      be.index = idx;
      be.path = [...parentPath, idx]
      if (!be.plans) {
        be.plans = [];
        state.data.plans.map(function(plan){
          be.plans.push({...plan})
        })
      }

      if (be.subBenefits) {
        this_t.calculatePath(state, [...be.path, "subBenefits"], be.subBenefits);
      }
    })
  }

  setContextBenefit(state) {
    var this_t = this;
    if (state.data) {
      state.data.benefitGroups.map(function(bgroup, index) {
        bgroup.path = [index];
        this_t.calculatePath(state, [...bgroup.path, "benefits"], bgroup.benefits);
      })
    }
    console.log("AFTER CALCULATE:", state)
  }

  convertData(benefitState) {
    var rs = {
      product: data.product,
      numberPlan: data.numberPlan,
      plans: [],
      benefitGroups: []
    }

    data.plans.map(function(plan){
      rs.plans.push({
        name: plan.name,
        code: plan.code
      })
    })



    return rs;
  }

  componentWillUpdate(nextProp, nextState) {
    console.log("NGHIA componentWillUpdate")
    this.setContextBenefit(nextState);
    if (!nextState.isSync) {
      var state = {
        ...nextState,
        isSync: true
      }
      var data = {...state.data}
      console.log("++DATA", data)
      // DELETE this_t attribute
      data.benefitGroups = [];
      console.log(nextState)
      if (nextState.data) {
        nextState.data.benefitGroups.map(function(group){
          var gr = {...group}

          gr.benefits = [];
          group.benefits.map(function(be, kb) {
            var beTmp = {...be};
            delete beTmp.this_t;
            beTmp.subBenefits = [];

            be.subBenefits.map(function(subbe, ksub) {
              var subTmp = {...subbe};
              delete subTmp.this_t;
              beTmp.subBenefits.push(subTmp);
              // and more
            });

            gr.benefits.push(beTmp);
          })
          data.benefitGroups .push(gr)

        })
      }
      console.log("NGHIA after remove this_t, plans", data)

      var policy = this.props.currentPolicy;
      policy.benefitConfig = data;

      this.setState(state);
      this.props.dispatch(policyActions.setCurrentPolicy(policy))
    }
    console.log("TOGGLE will", nextState.form.isShow, this.state.form.isShow)
  }
  

  componentWillReceiveProps(nextProp) {
    console.log("componentWillReceiveProps", nextProp)
    var state = this.state;
    state.data = nextProp.currentPolicy.benefitConfig;
    console.log(state)
    this.setState({...state, isSync:true});

  }


   changeHandle(e) {
      console.log(this.state)
      var requireField = ["name"];
      const { name, value } = e.target;
      var data = this.state.data;
      var errorField = this.state.errorField

      data[name] = value;
      errorField[name] = (requireField.indexOf(name) != -1 && value == "")

      this.setState({ ...this.state, data , errorField, isSync: false});
   }

   checkValidate() {
    var errorField = this.state.errorField;
    var data = this.state.data;
    var rs = true;

    ["name"].forEach(function(name) {
      let err = (data[name] == "");
      errorField[name] = err;
      rs &= !err;
    })

    this.setState({data, errorField})
    return rs;
   }

   getByPath(benefitGroups, path) {
    var rs = benefitGroups;
    path.map(function(p){
      rs = rs[p]
    })
    return rs;
   }

   delete_benefit(e, row) {
    if (!confirm("You are sure?"))
      return;
    this.props.dispatch(policyActions.deleteBenefitCurrentPolicy(row.path));
   }


   reset() {
    console.log(this.state)
    this.setState({
      dataDefault: {...this.state.dataDefault},
      data: {...this.state.dataDefault},
      errorField: {}
    })

   }

  showEditForm(e, row) {
    console.log("STATE", this.state)
    var form = {...this.state.form};
    form.isShow = true;
    form.type = "edit";
    form.path = row.path;
    this.setState({...this.state, form})
  }

  showAddForm(e, pGroup) {
    console.log("STATE", this.state)
    var form = {...this.state.form};
    form.isShow = true;
    form.type = "add";
    form.path = pGroup.path;
    this.setState({...this.state, form})
  }

  closeFormCallback() {
    console.log("CloseFormCallback ")
    this.toggleForm();
    // this.setState({isSync:false})
  }

   addBenefit(pGroupIndex, groupIndex, benefit) {
    var data = this.state.data;
    console.log("BEFORE DATA", data)
    var ben = {...benefit};
    data.benefitGroups[groupIndex].benefits.push(benefit);
    data.benefitGroups[groupIndex].benefits.map(function(b, index){
      b.index = index;
    })
    this.setState({data, isSync:false})
   }

   editBenefit(pGroupIndex, groupIndex, benefit) {
    console.log("editBenefit", benefit, this.state.data)
    this.setState({...this.state, isSync:false})
   }

  calculatePlan(benefit, plans) {
    if (!benefit.plans)
      benefit.plans =[];
    plans.map(function(plan) {
      let found =false;
      benefit.plans.map(function(bePlan){
        if (bePlan.code == plan.code)
          found = true;
      })
      if (!found) {
        benefit.plans.push({...plan})
      }
    })
    var this_t = this;
    if (typeof benefit.benefits == 'array') {
      benefit.benefits.map(function(be){
        this_t.calculatePlan(be, plans)
      })
    }
    if (typeof benefit.subBenefits == 'array') {
      benefit.subBenefits.map(function(be){
        this_t.calculatePlan(be, plans)
      })
    }
  }


   generatePlans(e) {
    console.log("generatePlans")
    var this_t = this;
    var state = this.state;
    var num = this.state.data.numberPlan;
    console.log(Array(num).keys())
    state.data.plans = [];
    for(let i=1 ; i<=num ; i+=1) {
      state.data.plans.push({
        code:"plan"+i,
        name: "Plan "+i
      })
    }
    state.data.benefitGroups.map(function(g){
      g.benefits.map(function(be) {
        this_t.calculatePlan(be, state.data.plans);
      })
    })
    

    state.isSync = false;
    console.log(state)
    this.setState({state})
   }

   toggleForm() {
    console.log("TOGGLE: ", !this.state.form.isShow)
    var form = {...this.state.form};
    form.isShow = !form.isShow
    this.setState({...this.state, form})
   }

  isExpandableRow(row) {
    return true;
  }

  expandComponent(row) {

    console.log("ExpandComponent: ROW: ", row, this)
      return (
        <SubBenefit parent={row.this_t} plans={row.this_t.state.data.plans} 
          benefit={row} subBenefits={row.subBenefits} />
      );
  }


  onBeforeSaveCellPlan(row, cellName, cellValue) {
    console.log("onBeforeSaveCell: row", row, "cellName: ", cellName, "cellValue: ",cellValue)
    var data = row.this_t.state.data;
    data.plans.map(function(plan) {
      if(plan.code == row.code) {
        plan[cellName] = cellValue;
      }
    })
    row.this_t.setState({data})
  }

  onAfterSaveCellPlan(row, cellName, cellValue) {
    console.log("onAfterSaveCell: row", row, "cellName: ", cellName, "cellValue: ",cellValue)
  }

   editFormatter(cell, row, extra) {
    var this_t = extra.this_t;
    return (

      <div>
      <a className="badge badge-pill badge-danger" 
      onClick={(e)=>this_t.delete_benefit(e, row)}>Delete</a>
      <a className="badge badge-pill badge-info" onClick={(e)=>this_t.showEditForm(e, row)}>Edit</a>
      <a className="badge badge-pill badge-success" onClick={(e)=>row.this_t.showAddForm(e, row)}>Add sub</a>
      </div>
    );
   }
   // format for plan
   planFormatter(cell, row, extra) {
      // console.log("editFormatter", cell, row, extra)
      var plan = {}
      row.plans.map(function(p, key) {
        if (p.code == extra.planCode)
          plan = p;
      })
      var preminum = plan.preminum || "";
      var sum = plan.sumInsured || ""
      return (preminum+ " - "+sum)
   }


   render() {
    console.log("TOGGLE ren: ", this.state.form.isShow)
      var {data, errorField} = this.state;
      var {error_msg, message} = this.state;
      var groups = [
          {id: "basic", name: "Basic"},
          {id: "additional", name: "Additional"}
        ];
      var benefitGroups = this.state.data.benefitGroups
      var plansState = this.state.data.plans;
      console.log("benefitGroups", benefitGroups)

      var this_t = this;
      var {token} = this.props;

      const cellEditPlanProp = {
        mode: 'click',
        blurToSave: true,
        beforeSaveCell: this.onBeforeSaveCellPlan, // a hook for before saving cell
        afterSaveCell: this.onAfterSaveCellPlan  // a hook for after saving cell
      };

      var plans = [];
      plansState.map(function(plan) {
        plans.push({
          ...plan,
          this_t: this_t
        })
      })

      // var benefitGroups = [];
      // groupsState.map(function(group){
      //   var gr = {
      //     ...group
      //   }

      //   gr.benefits = [];
      //   group.benefits.map(function(be, kb) {
      //     var beTmp = {...be};
      //     beTmp.subBenefits = [];

      //     be.subBenefits.map(function(subbe, ksub) {
      //       var subTmp = {...subbe};
      //       beTmp.subBenefits.push(subTmp);
      //       // and more
      //     });

      //     gr.benefits.push(beTmp);
      //   })
      //   benefitGroups.push(gr)

      // })

      return (
        <div>
          <Row>
            <Col sm="9">
              <FormGroup row>
                <Col md="3">
                  <Label htmlFor="product">Product Name (*)</Label>
                </Col>
                <Col xs="12" md="9">
                  <Input type="text" id="product" name="product" value={data.product} 
                  onChange={this.changeHandle} className={errorField.product?"is-invalid":""}/>
                </Col>

                
              </FormGroup>

              <FormGroup row>
                <Col md="3">
                  <Label htmlFor="numberPlan">Number of plans</Label>
                </Col>
                <Col xs="12" md="3">
                  <Input type="number" id="numberPlan" name="numberPlan" value={data.numberPlan} 
                  onChange={this.changeHandle} className={errorField.numberPlan?"is-invalid":""}/>
                  {
                    errorField.saleName?(<FormText color="muted">Please enter sale persion name</FormText>):("")
                  }
                </Col>

                <Button className="btn btn-primary active" onClick={this.generatePlans.bind(this)}>Generate</Button>
              </FormGroup>
              </Col>
              <Col sm="3">
                <BootstrapTable data={plans} hover striped className=" table-sm" cellEdit={ cellEditPlanProp }>
                    <TableHeaderColumn isKey dataField='code' hidden={true}>Code</TableHeaderColumn>
                    <TableHeaderColumn dataField='name' >Plan name</TableHeaderColumn>
                </BootstrapTable>
              </Col>
            </Row>

            {
              groups.map(function(group, gkey) {
                return ( 
                  <div key={gkey}>
                    <br/>
                    <h4>{group.name}</h4>
                    {
                      benefitGroups.map( function(bgroup, bgroupIndex) {
                        
                          if (bgroup.groupType==group.id) {
                            bgroup.benefits.map(function(be){
                              be.this_t = this_t;
                            })
                            return ( 
                              <div key={bgroupIndex}>
                                <br/>
                                <Col sm="12">
                                  <a className="text-primary pull-right" onClick={(e)=>this_t.showAddForm(e, bgroup)} >
                                    <i className="fa fa-plus" aria-hidden="true"></i>
                                    Add benefit
                                  </a>
                                  <h6>{bgroup.groupName}</h6>
                                </Col>
                                <BootstrapTable data={bgroup.benefits} hover striped className=" table-sm"
                                  expandableRow={ this_t.isExpandableRow }
                                  expandComponent={ this_t.expandComponent }
                                >
                                    <TableHeaderColumn isKey dataField='index' hidden={true}>index</TableHeaderColumn>
                                    <TableHeaderColumn dataField='nameEn' >Name En</TableHeaderColumn>
                                    <TableHeaderColumn dataField='nameVn' >Name Vn</TableHeaderColumn>

                                    {
                                      plans.map(function(plan, key) {
                                        return (
                                          <TableHeaderColumn key={key} dataFormat={ this_t.planFormatter }
                                            formatExtraData={{planCode:plan.code, this_t:this_t}}
                                          >
                                          {plan.name}</TableHeaderColumn>
                                        )
                                      })
                                    }

                                    <TableHeaderColumn dataFormat={ this_t.editFormatter } 
                                      formatExtraData={{this_t, groupIndex: bgroupIndex}}>
                                      Operations</TableHeaderColumn>
                                </BootstrapTable>
                              </div>
                            )

                          }

                      })
                    }
                  </div>
                )
              })
            }

          <Modal isOpen={this.state.form.isShow} toggle={this.toggleForm}
                 className={'modal-success ' + this.props.className} >
            <ModalHeader toggle={this.toggleForm}>{this.state.form.type=="add"?"Add benefit":"Edit benefit"}</ModalHeader>
            <ModalBody>
              <AddEditForm token={token} closeCallback={this_t.closeFormCallback.bind(this_t)}
               formConfig={this.state.form} />
            </ModalBody>
          </Modal>
                  
        </div>
      );
   }
}

// export default PolicyBenefits; 

function mapStateToProps(state) {
    const { currentPolicy } = state.policy;
    const { user, userInfo } = state.authentication;
    return {
        currentPolicy,
        token: user, userInfo
    };
}
function registActions (dispatch, props) {
  return {
    dispatch,
    ...bindActionCreators({
        ...policyActions
      }, dispatch)
  }
}

const Page = connect(mapStateToProps, null)(PolicyBenefits);
export default Page; 
