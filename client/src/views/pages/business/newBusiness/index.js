import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'; 
import { policyActions, policyPlansActions } from '../../../../actions';
import { 
  TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col,
  CardHeader, CardBody,
  FormGroup, Label, Input,
} from 'reactstrap';
import classnames from 'classnames';

import { ToastContainer, toast } from 'react-toastify';

import PolicyHeader from "./policyHeader"
import PolicyBenefits from "./benefit"
import Premium from "./premium"
import Term from "./term"
import Clauses from "./clauses"
import defaultDataForm from './_defaultData'


class FormPolicy extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      activeTab: '1',
      data: {},
      errorField: {},
      id: "",
      isSync:true
    };
    this.toggle = this.toggle.bind(this);
    this.submit = this.submit.bind(this);
    this.handleChange = this.handleChange.bind(this)
    
    console.log("======PROPS",props);
    this.setDataDefault()
    if (props.match.params.id) {
      this.state.id = props.match.params.id;
      this.loadEditPolicy(props.match.params.id);
    }
  }

  componentWillUpdate(nextProp, nextState) {
    console.log("NGHIA componentWillUpdate")
    if (!nextState.isSync) {
      var state = {
        ...nextState,
        isSync: true
      }

      var policy = this.props.currentPolicy;
      policy.policyCode = state.data.policyCode;
      console.log(policy)

      this.setState(state);
      this.props.dispatch(policyActions.setCurrentPolicy(policy))
    }
  }

  loadEditPolicy(id) {
    console.log("loadEditPolicy", id)
    this.props.dispatch(policyActions.load_edit_policy(this.props.token, id, this.callbackLoadPolicy.bind(this)))
  }

  callbackLoadPolicy(success, response) {
    console.log("callbackLoadPolicy")
    if (!success)
      return
    var currentPolicy = {...this.props.currentPolicy};
    var policy = {...response}
    policy.benefitConfig = defaultDataForm.benefitConfig;
    this.props.dispatch(policyActions.setCurrentPolicy(policy));
    this.props.dispatch(policyPlansActions.getPolicyPlans(this.props.token, policy.id, this.callbackLoadPolicyPlans.bind(this)))
  }

  callbackLoadPolicyPlans(success, response) {
    console.log("callbackLoadPolicyPlans")
    if (!success)
      return
    var currentPolicy = {...this.props.currentPolicy};
    var benefitConfig = {}
    var plans = [...response]
    var this_t = this

    console.log("####### PLANS: ", plans);
    benefitConfig.numberPlan = plans.length;
    benefitConfig.plans = [];
    plans.map(function(plan){
      benefitConfig.plans.push({
        code: plan.code,
        name: plan.name
      })
    })

    benefitConfig.benefitGroups = [];
    plans[0].benefitGroups.map(function(g, key) {
      benefitConfig.benefitGroups.push({
        groupName: g.groupName,
        groupType: g.groupType,
        benefits: this_t.cloneBenefitLoad(g.benefits, [key, "benefits"]),
      })
    })

    // collect primium/sum plans
    benefitConfig.benefitGroups.map(function(g){
      g.benefits.map(function(be) {
        this_t.collectPlanWhenLoad(be, plans);
      })
    })
    console.log("currentPolicy", currentPolicy) 

    currentPolicy.benefitConfig = benefitConfig;
    this.props.dispatch(policyActions.setCurrentPolicy(currentPolicy));
  }

  collectPlanWhenLoad(benefit, plans) {
    benefit.plans = [];
    var this_t = this;
    plans.map(function(plan){
      let pointer = this_t.getByPath(plan.benefitGroups, benefit.path);
      benefit.plans.push({
        code: plan.code,
        preminum: pointer.premium,
        sumInsured: pointer.sumInsured
      })
    })

    if (benefit.subBenefits) {
      benefit.subBenefits.map(function(sub){
        this_t.collectPlanWhenLoad(sub, plans)
      })
    }
  }

  getByPath(benefitGroups, path) {
    var rs = benefitGroups;
    path.map(function(p){
      rs = rs[p]
    })
    return rs;
  }


  cloneBenefitLoad(benefits, parentPath) {
    var rs = []
    var this_t = this
    benefits.map(function(be, key) {
      var tmp = {
         nameVn: be.name.vn,
         nameEn: be.name.en,
         path: [...parentPath, key],
         subBenefits: []
      }
      if (be.subBenefits)
        tmp.subBenefits = this_t.cloneBenefitLoad(be.subBenefits, [...tmp.path, "subBenefits"]);
      rs.push(tmp)
    })
    return rs
  }

  setDataDefault() {
    var defaultData = defaultDataForm
    this.props.dispatch(policyActions.setCurrentPolicy(defaultData));
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  validate() {
    console.log("validate form")
    return true;
  }

  callbackCreate(success, rs) {
    console.log("callbackCreate", success, rs);
    if (success) {
      toast("Creating successfully", { autoClose: 3000 ,type: toast.TYPE.SUCCESS})
      this.props.history.push("/admin/businesses")
    }
    else {
      console.log("show notify error")
      toast("Creating failure", { autoClose: 3000 ,type: toast.TYPE.ERROR})
    }
  }

  callbackUpdate(success, rs) {
    console.log("callbackCreate", success, rs);
    if (success) {
      toast("Creating successfully", { autoClose: 3000 ,type: toast.TYPE.SUCCESS})
      this.props.history.push("/admin/businesses")
    }
    else {
      console.log("show notify error")
      toast("Creating failure", { autoClose: 3000 ,type: toast.TYPE.ERROR})
    }
  }

  fillterBenefitByPlan(benefits, planCode) {
    var rs = [];
    var this_t = this;
    benefits.map(function(be) {
      let benefit = {
        name: {
          en: be.nameEn,
          vn: be.nameVn
        },
        premium: "",
        sumInsured: "",
        subBenefits: []
      }
      if (be.code) 
        benefit.code = be.code;
      // set premium/sumInsured
      be.plans.map(function(plan) {
        if (plan.code == planCode) {
          benefit.premium = plan.preminum || "",
          benefit.sumInsured = plan.sumInsured || ""
        }
      });
      // clone subBenefit
      if (be.subBenefits) {
        benefit.subBenefits = this_t.fillterBenefitByPlan(be.subBenefits, planCode)
      }

      rs.push(benefit);
    })

    return rs;
  }

  getPlanParam() {
    var {currentPolicy} = this.props;
    var {benefitGroups} = currentPolicy.benefitConfig
    var cPlans = currentPolicy.benefitConfig.plans;
    var plans = [];
    var this_t = this;
    cPlans.map(function(cplan) {
      let plan = {
        name: cplan.name,
        code: cplan.code,
        benefitGroups: []
      }

      benefitGroups.map(function(group) {
        plan.benefitGroups.push({
          groupName: group.groupName,
          groupType: group.groupType,
          benefits: this_t.fillterBenefitByPlan(group.benefits, cplan.code)
        })
      })

      plans.push(plan);

    })
    console.log("getPlanParam:", plans)
    return plans;
  }

  getPolicyParams() {
    var {currentPolicy} = this.props;
    var {userInfo} = this.props;
    var policy = {};
    policy.header = {...currentPolicy.header};
    policy.premium = {...currentPolicy.premium};
    policy.termCondition = {...currentPolicy.termCondition};
    policy.policyCode = currentPolicy.policyCode;
    policy.createBy = {...userInfo}
    policy.clauses = [];
    currentPolicy.clauses.map(function(clause) {
      policy.clauses.push({
        vn: clause.vn,
        en: clause.en
      })
    });
    console.log("getPolicyParams:", policy)
    return policy;
  }

  submit(e) {
    
    console.log("Sumit form")

    if (!this.validate())
      return;

    var {token, currentPolicy} = this.props
    var policy = this.getPolicyParams();
    var plans = this.getPlanParam();

    if (this.state.id) {
      this.props.dispatch(policyActions.update_policy(token, this.state.id, policy, plans, this.callbackUpdate.bind(this)));
    }
    else {
      this.props.dispatch(policyActions.create_policy(token, policy, plans, this.callbackCreate.bind(this)));
    }
  }

  handleChange(e) {
    console.log(this.state)
    var requireField = ["policyCode"];
    const { name, value } = e.target;
    var data = this.state.data;
    var errorField = this.state.errorField

    data[name] = value;
    errorField[name] = (requireField.indexOf(name) != -1 && value == "")

    this.setState({ ...this.state, data , errorField, isSync: false});
  }
  render() {
    return (

      <div className="animated fadeIn">
          <Row>
            <Col>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i> New Policy
                </CardHeader>
                <CardBody className="card-body">

                  <FormGroup row>
                    <Col sm="6"/>
                    <Col sm="2" className="text-right">
                      <Label htmlFor="nameEn" >Policy code</Label>
                    </Col>
                    <Col sm="2">
                      <Input type="text" id="policyCode" name="policyCode" placeholder="Policy code.."
                        onChange={this.handleChange}/>
                    </Col>
                    <Col sm="2">
                      <button onClick={this.submit} className="btn btn-primary active pull-right">Submit</button>
                    </Col>
                  </FormGroup>
                    
                  <Row>
                    <Col>
                      <div>
                        <Nav tabs>
                          <NavItem>
                            <NavLink
                              className={classnames({ active: this.state.activeTab === '1' })}
                              onClick={() => { this.toggle('1'); }}
                            >
                              Header
                            </NavLink>
                          </NavItem>
                          <NavItem>
                            <NavLink
                              className={classnames({ active: this.state.activeTab === '2' })}
                              onClick={() => { this.toggle('2'); }}
                            >
                              Benefits
                            </NavLink>
                          </NavItem>
                          <NavItem>
                            <NavLink
                              className={classnames({ active: this.state.activeTab === '3' })}
                              onClick={() => { this.toggle('3'); }}
                            >
                              Premium
                            </NavLink>
                          </NavItem>
                          <NavItem>
                            <NavLink
                              className={classnames({ active: this.state.activeTab === '4' })}
                              onClick={() => { this.toggle('4'); }}
                            >
                              Term & Condition
                            </NavLink>
                          </NavItem>
                          <NavItem>
                            <NavLink
                              className={classnames({ active: this.state.activeTab === '5' })}
                              onClick={() => { this.toggle('5'); }}
                            >
                              Clauses
                            </NavLink>
                          </NavItem>
                          <NavItem>
                            <NavLink
                              className={classnames({ active: this.state.activeTab === '6' })}
                              onClick={() => { this.toggle('6'); }}
                            >
                              Certification
                            </NavLink>
                          </NavItem>
                        </Nav>
                        <TabContent activeTab={this.state.activeTab}>
                          <TabPane tabId="1">
                            <Row>
                              <Col sm="12">
                                <PolicyHeader/>
                              </Col>
                            </Row>
                          </TabPane>
                          <TabPane tabId="2">
                            <Row>
                              <Col sm="12">
                                <PolicyBenefits/>
                              </Col>
                            </Row>
                          </TabPane>
                          <TabPane tabId="3">
                            <Row>
                              <Col sm="12">
                                <Premium/>
                              </Col>
                            </Row>
                          </TabPane>
                          <TabPane tabId="4">
                            <Row>
                              <Col sm="12">
                                <Term/>
                              </Col>
                            </Row>
                          </TabPane>
                          <TabPane tabId="5">
                            <Row>
                              <Col sm="12">
                                <Clauses/>
                              </Col>
                            </Row>
                          </TabPane>
                          <TabPane tabId="6">
                            <Row>

                            </Row>
                          </TabPane>
                        </TabContent>
                      </div>
                    </Col>
                  </Row>


                </CardBody>
              </Card>
            </Col>
          </Row>
          <ToastContainer autoClose={5000} />
        </div>


      
    );
  }
}

// export default PolicyBenefits; 

function mapStateToProps(state) {
    const { currentPolicy, error } = state.policy;
    const { user, userInfo } = state.authentication;
    return {
        currentPolicy, error,
        token: user, userInfo
    };
}
function registActions (dispatch, props) {
  return {
    dispatch
  }
}

const Page = connect(mapStateToProps, null)(FormPolicy);
export default Page; 