export default {
  benefitConfig: {
    product:"",
    numberPlan: 2,
    plans: [
      {
        name: "Plan 1", code:"plan1"
      },
      {
        name: "Plan 2", code:"plan2"
      }
    ],
    benefitGroups: [
      {
        groupName: "Medical Expenses due to Accident",
        groupType: "basic",
        // path: [0],
        benefits: []
      },
      {
        groupName: "Inpatient Treatment",
        groupType: "basic",
        // path: [0],
        benefits: []
      },
      {
        groupName: "Outpatient Treatment",
        groupType: "additional",
        // path: [1],
        benefits: []
      },
      {
        groupName: "Comprehensive Dental Care",
        groupType: "additional",
        // path: [1],
        benefits: []
      },
      {
        groupName: "Death & Disability",
        groupType: "additional",
        // path: [1],
        benefits: []
      },
      {
        groupName: "Salary Compensation",
        groupType: "additional",
        // path: [1],
        benefits: []
      },
      {
        groupName: "Emergency medical evacuation",
        groupType: "additional",
        // path: [1],
        benefits: []
      }
    ]
  },
  premium: {
    currency: "USD",
    exchangeRate: "",
    discountRate: "",
    premiumMode: "Monthly",
    sumAssured: {
      USD: "",
      VND: ""
    },
    grossPremium: {
      USD: "",
      VND: ""
    },
    discountAmount: {
      USD: "",
      VND: ""
    },
    netPremium: {
      USD: "",
      VND: ""
    },
    premiumNote: {
      en: "",
      vn: ""
    },
    exchangeRateNote: {
      en: "",
      vn: ""
    },
    premiumTermNote: {
      en: "",
      vn: ""
    }
  },
  termCondition: {
    waitingPeriod:{
      en: "",
      vn:""
    },
    termCondition: {
      en: "",
      vn: ""
    },
    territorical: {
      en: "",
      vn: ""
    }
  },
  clauses: []
 };