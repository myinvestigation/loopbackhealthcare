import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'; 
import { policyActions } from '../../../../actions';
import { Link } from 'react-router-dom';
import { deseaseService } from '../../../../services'


import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton
} from "reactstrap";

class FormBenefit extends React.Component {
  constructor(props) {
      super(props);
      
      this.state = {
         data:{
          nameEn: "",
          nameVn:"",
          plans: []
         },
         dataDefault:{
          nameEn: "",
          nameVn:"",
          plans: []
         },
         errorField:{},
         error_msg:"",
         message:"",
         currentPolicy: this.props.currentPolicy
      }
      this.changeHandle = this.changeHandle.bind(this)
      this.submit = this.submit.bind(this)
      this.reset = this.reset.bind(this)

      var {currentPolicy} = this.props;
      const {formConfig} = this.props;
      console.log("FORMCONFIG",formConfig)
      if (formConfig.type == "add") {
        this.state.data.plans = [];
        this.state.dataDefault.plans = [];
        var this_t = this;
        currentPolicy.benefitConfig.plans.map(function(plan){
          this_t.state.data.plans.push({...plan})
          this_t.state.dataDefault.plans.push({...plan})
        })
      }
      else if (formConfig.type == "edit") {
        this.state.data = {...this.getByPath(currentPolicy.benefitConfig.benefitGroups, formConfig.path)};
        this.state.dataDefault = {...this.state.data};
      }

      console.log("AFTER iint state", this.state)
   };

   getByPath(benefitGroups, path, ignore=0) {
    var rs = benefitGroups;
    for(var i=0 ; i<(path.length-ignore) ; i++) {
      rs =rs[path[i]]
    }
    return rs;
   }


   load_data() {
    const {token, formConfig} = this.props
    deseaseService.get_desease(token, formConfig.id).then(
      data => {
        console.log("load_data success: ", data)
        this.setState({data, dataDefault: data})
      },
      error => {
        console.log("load_data error: ", error)
        this.setState({error_msg: "Get user info failure"})
        this.props.closeCallback(false, "Get desease info failure");
      }
    );
   }

   changeHandle(e) {
      console.log(this.state)
      var requireField = ["name", "code"];
      const { name, value } = e.target;
      var data = this.state.data;
      var errorField = this.state.errorField

      data[name] = value;
      errorField[name] = (requireField.indexOf(name) != -1 && value == "")

      this.setState({ ...this.state, data , errorField});
   }
   changeHandlePlan(e, planCode, type) {
    console.log("changeHandlePlan", type, planCode)

    var data = this.state.data;
    var value = e.target.value;
    data.plans.map(function(plan, key){
      if (plan.code==planCode) {
        plan[type] = value;
      }
    })
    this.setState({data})
   }

   checkValidate() {
    var errorField = this.state.errorField;
    var data = this.state.data;
    var rs = true;

    ["name", "code"].forEach(function(name) {
      let err = (data[name] == "");
      errorField[name] = err;
      rs &= !err;
    })

    this.setState({data, errorField})
    return rs;
   }

   submit() {
    var {formConfig} = this.props
    if (formConfig.type == "add") 
      this.actionAdd();
    else if (formConfig.type == "edit")
      this.actionEdit()
   }

    getDataParams() {
    var data = this.state.data;
    return {
      nameVn: data.nameVn,
      nameEn: data.nameEn,
      plans: data.plans
    }
   }

   copyParamToSave(src, dst) {
      dst.nameVn = src.nameVn;
      dst.nameEn = src.nameEn;
      dst.plans = [];
      src.plans.map(function(plan, index){
        dst.plans.push({...plan})
      })
      console.log("COPY:", src, dst.plans, dst)
   }

   actionAdd() {
    console.log("ACTION ADD")
    // var policy = this.state.currentPolicy;
    // var pointer = this.getByPath(policy.benefitConfig.benefitGroups, this.props.formConfig.path);
    var benefit = this.getDataParams();
    benefit.subBenefits = [];
    // console.log("pointer", pointer)
    // if (pointer.benefits)
    //   pointer.benefits.push(benefit);
    // else if (pointer.subBenefits) {
    //   pointer.subBenefits.push(benefit);
    // }
    // console.log("Save: ",pointer, policy)
    console.log(this.props.formConfig)
    this.props.dispatch(policyActions.AddBenefitCurrentPolicy(this.props.formConfig.path, benefit));
    this.props.closeCallback();
    this.reset()
   }

   actionEdit() {
    var policy = this.props.currentPolicy;
    console.log("policy",policy)
    var pointer = this.getByPath(policy.benefitConfig.benefitGroups, this.props.formConfig.path);
    console.log("pointer",pointer)
    this.copyParamToSave(this.state.data, pointer);
    console.log("EDIT, this benefite:",pointer, "Policy tmp:",policy, "state",this.state);
    this.props.dispatch(policyActions.setCurrentPolicy(policy));
    this.props.closeCallback();
   }

  

   reset() {
    console.log(this.state)
    this.setState({
      dataDefault: {...this.state.dataDefault},
      data: {...this.state.dataDefault},
      errorField: {}
    })

   }

   render() {
      var {data, errorField} = this.state;
      var {error_msg, message} = this.state;
      var this_t = this;
      var plans = data.plans


      console.log("PLANS: ", plans)

      return (
         <div>
          <Card>
              <CardBody className="card-body">
                <Form action="" method="post">
                  {
                    error_msg? (<div className="alert alert-danger">{error_msg}</div>):("")
                  }
                  {
                    message? (<div className="alert alert-primary">{message}</div>):("")
                  }
                  
                  <FormGroup>
                    <Label htmlFor="nameEn">Name En</Label>
                    <Input type="textarea" id="nameEn" name="nameEn" value={data.nameEn} placeholder="English.."
                      onChange={this.changeHandle} className={errorField.nameEn?"is-invalid":""}/>
                  </FormGroup>

                  <FormGroup>
                    <Label htmlFor="nameVn">Name Vn</Label>
                    <Input type="textarea" id="nameVn" name="nameVn" value={data.nameVn} placeholder="English.."
                      onChange={this.changeHandle} className={errorField.nameVn?"is-invalid":""}/>
                  </FormGroup>

                  {
                    plans.map(function(plan, index){
                      return (
                        <div key={index}>
                          <h5>{plan.name}</h5>
                          <FormGroup row>
                            <Col sm="6">
                              <Label htmlFor={plan.code}>Preminum</Label>
                              <Input type="number" id={plan.code} name="preminum" value={plan.preminum}
                                onChange={(e)=>this_t.changeHandlePlan(e, plan.code, "preminum")}/>
                            </Col>
                            <Col sm="6">
                              <Label htmlFor={plan.code}>Sum Insured</Label>
                              <Input type="number" id={plan.code} name="sumInsured" value={plan.sumInsured}
                                onChange={(e)=>this_t.changeHandlePlan(e, plan.code, "sumInsured")}/>
                            </Col>
                          </FormGroup>
                        </div>
                      )
                    })
                  }

                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" onClick={this.submit}><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger" onClick={this.reset}><i className="fa fa-ban"></i> Reset</Button>
                <Button size="sm" color="primary" className="active pull-right" onClick={(e)=>this.props.closeCallback(false)}>Cancel</Button>
              </CardFooter>

            </Card>

         </div>
      );
   }
}

// export default FormDesease; 

// export default PolicyBenefits; 

function mapStateToProps(state) {
    const { currentPolicy } = state.policy;
    const { user, userInfo } = state.authentication;
    return {
        currentPolicy,
        token: user, userInfo
    };
}
function registActions (dispatch, props) {
  return {
    dispatch,
    ...bindActionCreators({
        ...policyActions
      }, dispatch)
  }
}

const Page = connect(mapStateToProps, null)(FormBenefit);
export default Page; 