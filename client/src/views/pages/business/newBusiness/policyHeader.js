import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'; 
import { policyActions } from '../../../../actions';
import { Link } from 'react-router-dom';


import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBlock,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton
} from "reactstrap";

class HeaderForm extends React.Component {
  constructor(props) {
      super(props);

      var defaultData = {
        agentId:"",
        agentName:"",
        clientId:"",
        clientName:"",
        effectiveDate:"",
        expiryDate:"",
        nodeInusredEn:"",
        nodeInusredVn:"",
        nodeSalaryEn:"",
        nodeSalaryVn:"",
        numberDependant:"",
        numberEmployee:"",
        numberInusred:"",
        productName:"",
        saleName:"",
        totalSalary:""
       };
      
      this.state = {
        data: {
          ...defaultData
        },
        dataDefault: {
          ...defaultData
        },
         errorField:{},
         error_msg:"",
         message:"",
         isSync: false
      }
      this.changeHandle = this.changeHandle.bind(this)
      this.reset = this.reset.bind(this)
   };

  componentWillUpdate(nextProp, nextState) {
    console.log("Premium componentWillUpdate")
    if (!nextState.isSync) {
      var state = {
        ...nextState,
        isSync: true
      }

      this.setState(state);
      var policy = nextProp.currentPolicy;
      policy.header = nextState.data;
      this.props.dispatch(policyActions.setCurrentPolicy(policy))
    }
  }

  componentWillReceiveProps(nextProp) {
    console.log("Premium componentWillReceiveProps")

    var data = nextProp.currentPolicy.header;
    if (!data)
      data = this.state.dataDefault;

    this.setState({data:data})
  }


   changeHandle(e) {
      console.log(this.state)
      var requireField = ["name"];
      const { name, value } = e.target;
      var data = this.state.data;
      var errorField = this.state.errorField

      data[name] = value;
      errorField[name] = (requireField.indexOf(name) != -1 && value == "")

      this.setState({ ...this.state, data , errorField, isSync:false});
   }

   checkValidate() {
    var errorField = this.state.errorField;
    var data = this.state.data;
    var rs = true;

    ["name"].forEach(function(name) {
      let err = (data[name] == "");
      errorField[name] = err;
      rs &= !err;
    })

    this.setState({data, errorField})
    return rs;
   }




   reset() {
    console.log(this.state)
    this.setState({
      dataDefault: {...this.state.dataDefault},
      data: {...this.state.dataDefault},
      errorField: {}
    })

   }

   render() {
      var {data, errorField} = this.state;
      var {error_msg, message} = this.state;

      return (
         <div>
                <Form action="" method="post">
                  {
                    error_msg? (<div className="alert alert-danger">{error_msg}</div>):("")
                  }
                  {
                    message? (<div className="alert alert-primary">{message}</div>):("")
                  }
                  
                  <FormGroup row>
                    <Col md="2">
                      <Label htmlFor="productName">Product Name (*)</Label>
                    </Col>
                    <Col xs="12" md="6">
                      <Input type="text" id="productName" name="productName" value={data.productName} 
                      onChange={this.changeHandle} className={errorField.productName?"is-invalid":""}/>
                      {
                        errorField.productName?(<FormText color="muted">Please enter product name</FormText>):("")
                      }
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="2">
                      <Label htmlFor="clientName">Client Name (*)</Label>
                    </Col>
                    <Col xs="12" md="6">
                      <Input type="text" id="clientName" name="clientName" value={data.clientName}
                      onChange={this.changeHandle} className={errorField.clientName?"is-invalid":""}/>
                      {
                        errorField.productName?(<FormText color="muted">Please enter product name</FormText>):("")
                      }
                    </Col>
                    <Col md="2">
                      <Label htmlFor="clientId">Client ID (*)</Label>
                    </Col>
                    <Col xs="12" md="2">
                      <Input type="text" id="clientId" name="clientId" value={data.clientId}
                      onChange={this.changeHandle} className={errorField.clientId?"is-invalid":""}/>
                      {
                        errorField.clientId?(<FormText color="muted">Please enter client ID</FormText>):("")
                      }
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="2">
                      <Label htmlFor="agentName">Broker/Agent Name(*)</Label>
                    </Col>
                    <Col xs="12" md="6">
                      <Input type="text" id="agentName" name="agentName" value={data.agentName}
                      onChange={this.changeHandle} className={errorField.agentName?"is-invalid":""}/>
                      {
                        errorField.agentName?(<FormText color="muted">Please enter Agent name</FormText>):("")
                      }
                    </Col>
                    <Col md="2">
                      <Label htmlFor="agentId">Agent ID (*)</Label>
                    </Col>
                    <Col xs="12" md="2">
                      <Input type="text" id="agentId" name="agentId" value={data.agentId}
                      onChange={this.changeHandle} className={errorField.agentId?"is-invalid":""}/>
                      {
                        errorField.agentId?(<FormText color="muted">Please enter Agent ID</FormText>):("")
                      }
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="2">
                      <Label htmlFor="saleName">Sale Name (*)</Label>
                    </Col>
                    <Col xs="12" md="6">
                      <Input type="text" id="saleName" name="saleName" value={data.saleName} 
                      onChange={this.changeHandle} className={errorField.saleName?"is-invalid":""}/>
                      {
                        errorField.saleName?(<FormText color="muted">Please enter sale persion name</FormText>):("")
                      }
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="2">
                      <Label htmlFor="effectiveDate">Effective date (*)</Label>
                    </Col>
                    <Col xs="12" md="2">
                      <Input type="text" id="effectiveDate" name="effectiveDate" value={data.effectiveDate} 
                      onChange={this.changeHandle} className={errorField.effectiveDate?"is-invalid":""}/>
                      {
                        errorField.effectiveDate?(<FormText color="muted">Please enter sale effective date</FormText>):("")
                      }
                    </Col>

                    <Col md="2">
                      <Label htmlFor="expiryDate">Expiry Date (*)</Label>
                    </Col>
                    <Col xs="12" md="2">
                      <Input type="text" id="expiryDate" name="expiryDate" value={data.expiryDate} 
                      onChange={this.changeHandle} className={errorField.expiryDate?"is-invalid":""}/>
                      {
                        errorField.expiryDate?(<FormText color="muted">Please enter sale expiry date</FormText>):("")
                      }
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="2">
                      <Label htmlFor="numberEmployee">Number of employee</Label>
                    </Col>
                    <Col xs="12" md="2">
                      <Input type="number" id="numberEmployee" name="numberEmployee" value={data.numberEmployee} 
                      onChange={this.changeHandle} className={errorField.numberEmployee?"is-invalid":""}/>
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="2">
                      <Label htmlFor="numberDependant">Number of Dependant</Label>
                    </Col>
                    <Col xs="12" md="2">
                      <Input type="number" id="numberDependant" name="numberDependant" value={data.numberDependant} 
                      onChange={this.changeHandle} className={errorField.numberDependant?"is-invalid":""}/>
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="2">
                      <Label htmlFor="numberInusred">Number of Inusred</Label>
                    </Col>
                    <Col xs="12" md="2">
                      <Input type="number" id="numberInusred" name="numberInusred" value={data.numberInusred} 
                      onChange={this.changeHandle} className={errorField.numberInusred?"is-invalid":""}/>
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="2">
                      <Label htmlFor="nodeInusredEn">Note of Inusred</Label>
                    </Col>
                    <Col xs="12" md="6">
                      <Input type="textarea" id="nodeInusredEn" name="nodeInusredEn" value={data.nodeInusredEn} placeholder="English.."
                      onChange={this.changeHandle} className={errorField.nodeInusredEn?"is-invalid":""}/>
                    </Col>

                    <Col xs="12" md="4">
                      <Input type="textarea" id="nodeInusredVn" name="nodeInusredVn" value={data.nodeInusredVn} placeholder="Vietnamese.."
                      onChange={this.changeHandle} className={errorField.nodeInusredVn?"is-invalid":""}/>
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="2">
                      <Label htmlFor="totalSalary">Total Monthly Salary</Label>
                    </Col>
                    <Col xs="12" md="2">
                      <Input type="number" id="totalSalary" name="totalSalary" value={data.totalSalary} 
                      onChange={this.changeHandle} className={errorField.totalSalary?"is-invalid":""}/>
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="2">
                      <Label htmlFor="nodeSalaryEn">Note of Salary</Label>
                    </Col>
                    <Col xs="12" md="6">
                      <Input type="textarea" id="nodeSalaryEn" name="nodeSalaryEn" value={data.nodeSalaryEn} placeholder="English.."
                      onChange={this.changeHandle} className={errorField.nodeSalaryEn?"is-invalid":""}/>
                    </Col>

                    <Col xs="12" md="4">
                      <Input type="textarea" id="nodeSalaryVn" name="nodeSalaryVn" value={data.nodeSalaryVn} placeholder="Vietnamese.."
                      onChange={this.changeHandle} className={errorField.nodeSalaryVn?"is-invalid":""}/>
                    </Col>
                  </FormGroup>



                </Form>
    
         </div>
      );
   }
}

function mapStateToProps(state) {
    const { currentPolicy } = state.policy;
    const { user, userInfo } = state.authentication;
    return {
        currentPolicy,
        token: user, userInfo
    };
}
function registActions (dispatch, props) {
  return {
    dispatch,
    ...bindActionCreators({
        ...policyActions
      }, dispatch)
  }
}

const Page = connect(mapStateToProps, null)(HeaderForm);
export default Page;