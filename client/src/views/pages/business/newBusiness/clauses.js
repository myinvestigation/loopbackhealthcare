import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'; 
import { policyActions } from '../../../../actions';
import { Link } from 'react-router-dom';

import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBlock,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton
} from "reactstrap";

class PremiumForm extends React.Component {
  constructor(props) {
      super(props);

      var defaultData = [];
      
      this.state = {
        data: [
          ...defaultData
        ],
        dataDefault: [
          ...defaultData
        ],
        errorField:{},
        error_msg:"",
        message:"",
        isSync: false
      }
      this.changeHandle = this.changeHandle.bind(this)
      this.changeHandle2 = this.changeHandle2.bind(this)
      this.reset = this.reset.bind(this)
   };

  componentWillUpdate(nextProp, nextState) {
    console.log("clauses componentWillUpdate")
    if (!nextState.isSync) {
      var state = {
        ...nextState,
        isSync: true
      }

      this.setState(state);
      var policy = nextProp.currentPolicy;
      policy.clauses = [];
      nextState.data.map(function(clause) {
        policy.clauses.push({
          en: clause.en,
          vn: clause.vn
        })
      })
      console.log(policy)

      this.props.dispatch(policyActions.setCurrentPolicy(policy))
    }
  }

  componentWillReceiveProps(nextProp) {
    console.log("clauses componentWillReceiveProps")

    var data = nextProp.currentPolicy.clauses;
    if (!data)
      data = this.state.dataDefault;

    this.setState({data:data})
  }



   changeHandle(e) {
      console.log(this.state)
      var requireField = ["name"];
      const { name, value } = e.target;
      var data = this.state.data;
      var errorField = this.state.errorField

      data[name] = value;
      errorField[name] = (requireField.indexOf(name) != -1 && value == "")

      this.setState({ ...this.state, data , errorField, isSync:false});
   }

   changeHandle2(e, child) {
      console.log(this.state)
      var requireField = ["name"];
      const { name, value } = e.target;
      var data = this.state.data;
      var errorField = this.state.errorField

      data[name][child] = value;
      if (!errorField[name])
        errorField[name] = {};
      errorField[name][child] = (requireField.indexOf(name) != -1 && value == "")

      this.setState({ ...this.state, data , errorField, isSync:false});
   }

   checkValidate() {
    var errorField = this.state.errorField;
    var data = this.state.data;
    var rs = true;

    ["name"].forEach(function(name) {
      let err = (data[name] == "");
      errorField[name] = err;
      rs &= !err;
    })

    this.setState({data, errorField})
    return rs;
   }

   reset() {
    console.log(this.state)
    this.setState({
      dataDefault: {...this.state.dataDefault},
      data: {...this.state.dataDefault},
      errorField: {}
    })
   }

  addClause(type) {
    var data = this.state.data;
    data.push({
      clauseType: type,
      en: "(Enter a title)",
      vn: "(Enter a title)"
    })
    this.setState({data, isSync:false})
  }

  delete(row) {
    var data = row.this_t.state.data;
    data.splice(row.index, 1);
    this.setState({data, isSync: false});
  }

  onBeforeSaveCell(row, cellName, cellValue) {
    var data = row.this_t.state.data;
    data[row.index].en = row.en;
    data[row.index].vn = row.vn;

    row.this_t.setState({data, isSync: false});

    return true;
  }

  editFormatter(cell, row) {
    return (

      <div>
      <a className="badge badge-pill badge-danger" onClick={(e)=>row.this_t.delete(row)}>Delete</a>
      </div>
    );
  }

   render() {
      var {data, errorField} = this.state;
      var {error_msg, message} = this.state;
      var this_t = this;
      var additionClauses = [];
      var exclusionClause = [];
      data.map(function(clause, key){
        clause.index = key;
        clause.this_t = this_t;
        if (clause.clauseType == 1) {
          clause.order = additionClauses.length;
          additionClauses.push(clause)
        }
        else {
          clause.order = exclusionClause.length;
          exclusionClause.push(clause)
        }
      });

      const cellEditProp = {
        mode: 'click',
        blurToSave: true,
        beforeSaveCell: this.onBeforeSaveCell, // a hook for before saving cell
        afterSaveCell: this.onAfterSaveCell  // a hook for after saving cell
      };

      return (
         <div>
            <a className="text-primary pull-right" onClick={(e)=>this_t.addClause(1)} >
              <i className="fa fa-plus" aria-hidden="true"></i>
              Add Additional Clause
            </a>
            <h5>Additional Clauses</h5>
            <BootstrapTable data={additionClauses} hover striped className=" table-sm"
              cellEdit={ cellEditProp }
            >
                      
                <TableHeaderColumn isKey dataField='order' hidden={true} ></TableHeaderColumn>
                <TableHeaderColumn dataField='en' editable={ { type: 'textarea' } }>Title In English</TableHeaderColumn>
                <TableHeaderColumn dataField='vn' editable={ { type: 'textarea' } }>Title In Vietnamese</TableHeaderColumn>
                <TableHeaderColumn editable={false} dataFormat={ this.editFormatter }>Operation</TableHeaderColumn>
            </BootstrapTable>

            <br/>
            <a className="text-primary pull-right" onClick={(e)=>this_t.addClause(2)} >
              <i className="fa fa-plus" aria-hidden="true"></i>
              Add Exclusion Clause
            </a>
            <h5>Exclusion Clauses</h5>
            <BootstrapTable data={exclusionClause} hover striped className=" table-sm"
              cellEdit={ cellEditProp }
            >
                      
                <TableHeaderColumn isKey dataField='order' hidden={true} ></TableHeaderColumn>
                <TableHeaderColumn dataField='en' editable={ { type: 'textarea' } }>Title In English</TableHeaderColumn>
                <TableHeaderColumn dataField='vn' editable={ { type: 'textarea' } }>Title In Vietnamese</TableHeaderColumn>
                <TableHeaderColumn editable={false} dataFormat={ this.editFormatter }>Operation</TableHeaderColumn>
            </BootstrapTable>
    
         </div>
      );
   }
}

// connect
function mapStateToProps(state) {
    const { currentPolicy } = state.policy;
    const { user, userInfo } = state.authentication;
    return {
        currentPolicy,
        token: user, userInfo
    };
}
function registActions (dispatch, props) {
  return {
    dispatch,
    ...bindActionCreators({
        ...policyActions
      }, dispatch)
  }
}

const Page = connect(mapStateToProps, null)(PremiumForm);
export default Page; 