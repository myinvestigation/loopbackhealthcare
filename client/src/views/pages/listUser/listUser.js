import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'; 
import $ from 'jquery'
import { Link } from 'react-router-dom';
import {
  Badge,
  Row,
  Col,
  Card,
  CardHeader,
  CardBlock,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink
} from "reactstrap";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";
// var reactNotice = require('react-notice-message');
import { ToastContainer, toast } from 'react-toastify';


import { userActions } from '../../../actions';

import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';

import EditForm from './editForm'
import AddForm from './addForm'

class ListUser extends React.Component {

   constructor(props) {
      super(props);
      
      this.state = {
         data: [],
         toggleEditForm: false,
         toggleAddForm: false,
      }
      this.loadUsers = this.loadUsers.bind(this);
      this.toggleAdd = this.toggleAdd.bind(this);
      this.toggleEdit = this.toggleEdit.bind(this);

      this.loadUsers();
   };

    toggleEdit() {
    this.setState({
      toggleEditForm: !this.state.toggleEditForm
    });
    console.log(this.props)
  }

   loadUsers() {
    console.log("loadUsers");
    this.props.dispatch(userActions.load_users());
    const { users } = this.props;
    // reactNotice.show(<p>messages</p>, {closeTime:"3000"});
   }

   delete_user(e) {
      console.log("delete_user: "+ id);
      let id = e.target.attributes['data-id'].value;
      if (!id)
        return;
      this.props.dispatch(userActions.delete_user(id, this.deleteCallback.bind(this)));
      
   }

   deleteCallback(success, rs) {
      if (success) {
        toast("Deleting successfully", { autoClose: 3000 ,type: toast.TYPE.SUCCESS})
      }
      else {
        console.log("show notify error")
        toast("Deleting failure", { autoClose: 3000 ,type: toast.TYPE.ERROR})
      }
   }

   showEditForm(e, user) {

    this.setState({
      ...this.state,
      toggleEditForm: !this.state.toggleEditForm,
      userEdit: user
    })
   }

   editFormCloseCallback(success) {
    console.log("editFormCloseCallback ", success)
    this.toggleEdit();
    if (success){
      this.loadUsers()
      toast("Updating successfully", { autoClose: 3000 ,type: toast.TYPE.SUCCESS})
    }
   }

   toggleAdd() {
    this.setState({...this.state, toggleAddForm: !this.state.toggleAddForm})
   }

   addFormCloseCallback(success) {
    this.toggleAdd();
    if (success){
      this.loadUsers();
      toast("Adding successfully", { autoClose: 3000 ,type: toast.TYPE.SUCCESS})
    }
    
   }

   editFormatter(cell, row) {

    return (

      <div>
      <a className="badge badge-pill badge-danger" onClick={(e)=>row.this_t.delete_user(e)} data-id={row.id}>Delete</a>
      <a className="badge badge-pill badge-info" onClick={(e)=>row.this_t.showEditForm(e, row)}>Edit</a>
      </div>
    );
   }
   render() {
      var this_t = this;
      var { users, token, userInfo } = this.props;
      users.map(function(u){
        u.this_t = this_t;
      })

      return (

        <div className="animated fadeIn">
          <Row>
            <Col>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i> List Account
                  <button onClick={this.loadUsers} className="pull-right btn">Refresh</button>
                  <button onClick={this.toggleAdd} className="btn btn-primary active pull-right">Add user</button>

                </CardHeader>
                <CardBlock className="card-body">
                  <BootstrapTable data={users} hover striped search className=" table-sm">
                      <TableHeaderColumn isKey dataField='username'>User Name</TableHeaderColumn>
                      <TableHeaderColumn dataField='email'>Email</TableHeaderColumn>
                      <TableHeaderColumn dataField='fullname'>Fullname</TableHeaderColumn>
                      <TableHeaderColumn dataFormat={ this.editFormatter }>Edit</TableHeaderColumn>
                  </BootstrapTable>
                  
                </CardBlock>
              </Card>
            </Col>
          </Row>

          <Modal isOpen={this.state.toggleEditForm} toggle={this.toggleEdit}
                 className={'modal-success ' + this.props.className} >
            <ModalHeader toggle={this.toggleEdit}>Edit user</ModalHeader>
            <ModalBody>
              <EditForm userEdit={this.state.userEdit} token={token} closeCallback={this.editFormCloseCallback.bind(this)}/>
            </ModalBody>
          </Modal>

          <Modal isOpen={this.state.toggleAddForm} toggle={this.toggleAdd}
                 className={'modal-success ' + this.props.className} >
            <ModalHeader toggle={this.toggleAdd}>Add user</ModalHeader>
            <ModalBody>
              <AddForm token={token} closeCallback={this.addFormCloseCallback.bind(this)}/>
            </ModalBody>
          </Modal>

          <ToastContainer autoClose={5000} />
         </div>
      );
   }
}




function mapStateToProps(state) {
    const { users } = state.user;
    const { user, userInfo } = state.authentication;
    return {
        users,
        token: user,
        userInfo
    };
}
function registActions (dispatch, props) {
  return {
    dispatch,
    ...bindActionCreators({
        ...userActions
      }, dispatch)
  }
}

const connectedLoginPage = connect(mapStateToProps, registActions)(ListUser);
export default connectedLoginPage; 