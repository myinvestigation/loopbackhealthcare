import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'; 
import { userActions } from '../../../actions';
import {userService} from '../../../services'
import { Link } from 'react-router-dom';

import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBlock,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton
} from "reactstrap";

class FormUser extends React.Component {
  constructor(props) {
      super(props);

      console.log("###############")
      console.log("PROPS: ", props)

      var userInfo = props.userEdit || {};
      var token = props.token || {};
      
      this.state = {
         // user_id: props.match.params.user_id,
         user_id: props.user,
         userInfo: userInfo,
         token: token,
         user:{
          fullname:"",
          username:"",
          email:"",
          password:"",
          confirm_password:""
         },
         userDefault:{
          fullname:"",
          username:"",
          email:"",
          password:"",
          confirm_password:""
         },
         errorField:{},
         error_msg: "",
         message: ""
      }
      this.changeHandle = this.changeHandle.bind(this)
      this.submit = this.submit.bind(this)
      this.reset = this.reset.bind(this)
      this.load_user();
   };

   componentWillUpdate() {}

   load_user() {
    const {userInfo, token} = this.state
    userService.get_user(token, userInfo.id).then(
      user => {
        console.log("get success: ", user)
        this.setState({user, userDefault: user})

      },
      error => {
        console.log("get error: ", error)
        this.setState({error_msg: "Get user info failure"})
      }
    );
   }

   update_state(user) {
    console.log("============")
    console.log("USER: ", user)
    console.log("============")
   }

   changeHandle(e) {
      console.log(this.state)
      var requireField = ["username", "email"];
      const { name, value } = e.target;
      var user = this.state.user;
      var errorField = this.state.errorField

      user[name] = value;
      errorField[name] = (requireField.indexOf(name) != -1 && value == "")

      if (name == "password") {
        user["confirm_password"] = "";
        errorField["confirm_password"] = false;
        if (user["password"] != "") {
          errorField["confirm_password"] = (user["confirm_password"] != user["password"]);
        }
      }

      this.setState({ ...this.state, user , errorField});
   }

   checkValidate() {
    var errorField = this.state.errorField;
    var user = this.state.user;
    var rs = true;

    ["username", "email"].forEach(function(name) {
      let err = (user[name] == "");
      errorField[name] = err;
      rs &= !err;
    })

    if (user["password"] != user["confirm_password"] && user["password"] != "") {
      errorField["confirm_password"] = true;
      rs = false;
    }
    this.setState({user, errorField})
    return rs;
   }

   submit() {
    console.log("submit")
    if (!this.checkValidate())
      return;
    var user = this.state.user;
    var u = {
      username: user.username,
      fullname: user.fullname,
      email: user.email
    }

    if (user.password) {
      u.password = user.password;
    }

    // this.props.dispatch(userActions.create_user(u))
    userService.update_user(this.state.userInfo.id, u)
        .then(
          user => {
            this.setState({message: "Update success"})
            this.load_user();
            this.props.closeCallback(true);
          },
          error => {
            this.setState({error_msg: "Update failure"})
          }
        );
   }

   reset() {
    console.log(this.state)
    this.setState({
      userDefault: {...this.state.userDefault},
      user: {...this.state.userDefault},
      errorField: {}
    })

   }

   render() {
      var {user, errorField} = this.state;
      var {error_msg, message} = this.state;

      return (
         <div>


          <Card>
              <CardBlock className="card-body">
                <Form action="" method="post">
                  {
                    error_msg? (<div className="alert alert-danger">{error_msg}</div>):("")
                  }
                  {
                    message? (<div className="alert alert-primary">{message}</div>):("")
                  }
                  <FormGroup>
                    <Label htmlFor="fullname">Fullname</Label>
                    <Input type="text" id="fullname" name="fullname" value={user.fullname} placeholder="Enter fullname.."
                     onChange={this.changeHandle} className={errorField.fullname?"is-invalid":""}/>
                     {
                      errorField.fullname?(<span className="help-block">Please enter fullname</span>):("")
                     }
                    
                  </FormGroup>

                  <FormGroup>
                    <Label htmlFor="username">Username</Label>
                    <Input type="text" id="username" name="username" value={user.username} placeholder="Enter fullname.."
                      onChange={this.changeHandle} className={errorField.username?"is-invalid":""}/>
                    {
                      errorField.username?(<span className="help-block">Please enter username</span>):("")
                    }
                  </FormGroup>

                  <FormGroup>
                    <Label htmlFor="email">Email</Label>
                    <Input type="email" id="email" name="email" value={user.email} placeholder="Enter Email.."
                      onChange={this.changeHandle} className={errorField.email?"is-invalid":""}/>
                    {
                      errorField.email?(<span className="help-block">Please enter your email</span>):("")
                    }
                  </FormGroup>

                  <h4>Change password (Optional)</h4>

                  <FormGroup>
                    <Label htmlFor="password">Password</Label>
                    <Input type="password" id="password" name="password" value={user.password?user.password:""} placeholder=""
                      onChange={this.changeHandle} className={errorField.password?"is-invalid":""}/>
                    {
                      errorField.password?(<span className="help-block">Please enter your email</span>):("")
                    }
                  </FormGroup>

                  <FormGroup>
                    <Label htmlFor="confirm_password">Confirm password</Label>
                    <Input type="password" id="confirm_password" name="confirm_password" value={user.confirm_password?user.confirm_password:""} placeholder=""
                      onChange={this.changeHandle} className={errorField.confirm_password?"is-invalid":""}/>
                    {
                      errorField.confirm_password?(<span className="help-block">Confirm is not match</span>):("")
                    }
                  </FormGroup>

                  

                </Form>
              </CardBlock>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" onClick={this.submit}><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger" onClick={this.reset}><i className="fa fa-ban"></i> Reset</Button>
                <Button size="sm" color="primary" onClick={(e) => this.props.closeCallback(false)} className="active pull-right"> Cancel</Button>
              </CardFooter>

            </Card>

         </div>
      );
   }
}


function mapStateToProps(state) {
    const { message, error_msg } = state.user;
    return {
        message,
        error_msg
    };
}
function registActions (dispatch, props) {
  return {
    dispatch,
    ...bindActionCreators({
        ...userActions
      }, dispatch)
  }
}

const EditForm = connect(mapStateToProps, registActions)(FormUser);
export default EditForm; 