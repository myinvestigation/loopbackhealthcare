import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'; 
import { userActions } from '../../../actions';
import { Link } from 'react-router-dom';
import { userService } from '../../../services'

import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBlock,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton
} from "reactstrap";

class FormUser extends React.Component {
  constructor(props) {
      super(props);
      
      this.state = {
         user:{
          fullname:"",
          username:"",
          email:"",
          password:"",
          confirm_password:""
         },
         userDefault:{
          fullname:"",
          username:"",
          email:"",
          password:"",
          confirm_password:""
         },
         errorField:{},
         error_msg:"",
         message:""
      }
      this.changeHandle = this.changeHandle.bind(this)
      this.submit = this.submit.bind(this)
      this.reset = this.reset.bind(this)
   };


   componentWillUpdate() {
    // if (this.props.message != "") {
    //   this.reset();
    // }

   }

   changeHandle(e) {
      console.log(this.state)
      var requireField = ["username", "password", "email"];
      const { name, value } = e.target;
      var user = this.state.user;
      var errorField = this.state.errorField

      user[name] = value;
      errorField[name] = (requireField.indexOf(name) != -1 && value == "")

      if (name == "password") {
        user["confirm_password"] = "";
        errorField["confirm_password"] = false;
      }

      if (!errorField[name] && name == "confirm_password") {
        errorField[name] = (user["confirm_password"] != user["password"]);
      }

      this.setState({ ...this.state, user , errorField});
   }

   checkValidate() {
    var errorField = this.state.errorField;
    var user = this.state.user;
    var rs = true;

    ["username", "password", "email"].forEach(function(name) {
      let err = (user[name] == "");
      errorField[name] = err;
      rs &= !err;
    })

    if (user["password"] != user["confirm_password"] && user["password"] != "") {
      errorField["confirm_password"] = true;
      rs = false;
    }
    this.setState({user, errorField})
    return rs;
   }

   submit() {
    console.log("submit")
    if (!this.checkValidate())
      return;
    var user = this.state.user;
    var u = {
      username: user.username,
      fullname: user.fullname,
      email: user.email,
      password: user.password
    }

    // this.props.dispatch(userActions.create_user(u))

    userService.create_user(this.props.token.id, u)
      .then(
          user => {
            this.setState({message: "Adding success"})
            this.reset();
            this.props.closeCallback(true);
          },
          error => {
            console.log(error);
            this.setState({error_msg: "Adding failure"})
          }
        );
   }

   reset() {
    console.log(this.state)
    this.setState({
      userDefault: {...this.state.userDefault},
      user: {...this.state.userDefault},
      errorField: {}
    })

   }

   render() {
      var {user, errorField} = this.state;
      var {error_msg, message} = this.state;


      return (
         <div>


          <Card>
              <CardBlock className="card-body">
                <Form action="" method="post">
                  {
                    error_msg? (<div className="alert alert-danger">{error_msg}</div>):("")
                  }
                  {
                    message? (<div className="alert alert-primary">{message}</div>):("")
                  }
                  <FormGroup>
                    <Label htmlFor="fullname">Fullname</Label>
                    <Input type="text" id="fullname" name="fullname" value={user.fullname} placeholder="Enter fullname.."
                     onChange={this.changeHandle} className={errorField.fullname?"is-invalid":""}/>
                     {
                      errorField.fullname?(<span className="help-block">Please enter fullname</span>):("")
                     }
                    
                  </FormGroup>

                  <FormGroup>
                    <Label htmlFor="username">Username</Label>
                    <Input type="text" id="username" name="username" value={user.username} placeholder="Enter fullname.."
                      onChange={this.changeHandle} className={errorField.username?"is-invalid":""}/>
                    {
                      errorField.username?(<span className="help-block">Please enter username</span>):("")
                    }
                  </FormGroup>

                  <FormGroup>
                    <Label htmlFor="email">Email</Label>
                    <Input type="email" id="email" name="email" value={user.email} placeholder="Enter Email.."
                      onChange={this.changeHandle} className={errorField.email?"is-invalid":""}/>
                    {
                      errorField.email?(<span className="help-block">Please enter your email</span>):("")
                    }
                  </FormGroup>

                  <FormGroup>
                    <Label htmlFor="nf-password">Password</Label>
                    <Input type="password" id="password" name="password" value={user.password} placeholder="Enter Password.."
                      onChange={this.changeHandle} className={errorField.password?"is-invalid":""}/>
                    {
                      errorField.password?(<span className="help-block">Please enter your password</span>):("")
                    }
                  </FormGroup>

                  <FormGroup>
                    <Label htmlFor="confirm_password">Confirm Password</Label>
                    <Input type="password" id="confirm_password" name="confirm_password" value={user.confirm_password} placeholder="Enter Password.."
                      onChange={this.changeHandle} className={errorField.confirm_password?"is-invalid":""}/>
                    {
                      errorField.confirm_password?(<span className="help-block">Password not match</span>):("")
                    }
                  </FormGroup>

                </Form>
              </CardBlock>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" onClick={this.submit}><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger" onClick={this.reset}><i className="fa fa-ban"></i> Reset</Button>
                <Button size="sm" color="primary" className="active pull-right" onClick={(e)=>this.props.closeCallback(false)}>Cancel</Button>
              </CardFooter>

            </Card>

         </div>
      );
   }
}

export default FormUser; 