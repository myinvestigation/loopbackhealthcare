import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'; 
import { Link } from 'react-router-dom';
import { companyService } from '../../../services'
import { departmentService } from '../../../services'

import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBlock,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton
} from "reactstrap";

class FormDepartment extends React.Component {
  constructor(props) {
      super(props);
      
      this.state = {
         data:{
          name:"",
          companyId:""
         },
         dataDefault:{
          name:"",
          companyId:""
         },
         companies: [],
         errorField:{},
         error_msg:"",
         message:""
      }
      this.changeHandle = this.changeHandle.bind(this)
      this.submit = this.submit.bind(this)
      this.reset = this.reset.bind(this)
      const {formConfig} = this.props;

      if (formConfig.type == "edit") {
        this.load_data();
      }
      this.load_company();
   };

   load_data() {
    const {token, formConfig} = this.props
    departmentService.get_department(token, formConfig.id).then(
      data => {
        console.log("load_data success: ", data)
        this.setState({data, dataDefault: data})
      },
      error => {
        console.log("load_data error: ", error)
        this.setState({error_msg: "Get user info failure"})
        this.props.closeCallback(false, "Get department info failure");
      }
    );
   }

   load_company() {
    const {token} = this.props
    companyService.load_companies(token.id).then(
      companies => {
        console.log("load companies success: ", companies)
        this.setState({companies})
      },
      error => {
        console.log("load companies error: ", error)
        this.setState({error_msg: "Get user info failure"})
        this.props.closeCallback(false, "Get list companies failure");
      }
    );
   }

   changeHandle(e) {
      console.log(this.state)
      var requireField = ["ten", "companyId"];
      const { name, value } = e.target;
      var data = this.state.data;
      var errorField = this.state.errorField

      data[name] = value;
      errorField[name] = (requireField.indexOf(name) != -1 && value == "")

      this.setState({ ...this.state, data , errorField});
   }

   checkValidate() {
    var errorField = this.state.errorField;
    var data = this.state.data;
    var rs = true;

    ["name", "companyId"].forEach(function(name) {
      let err = (data[name] == "");
      errorField[name] = err;
      rs &= !err;
    })

    this.setState({data, errorField})
    return rs;
   }

   submit() {
    if (this.props.formConfig.type == "add") {
      this.addAction()
    }
    else {
      this.editAction()
    }
   }

   getDataParams() {
    var data = this.state.data;
    return {
      name: data.name,
      companyId: data.companyId
    }
   }

   addAction() {
    console.log("addAction")
    if (!this.checkValidate())
      return;
    var d = this.getDataParams();
    departmentService.create_department(this.props.token, d)
      .then(
          department => {
            this.setState({message: "Adding success"})
            this.reset();
            this.props.closeCallback(true, "Adding successfully");
          },
          error => {S
            console.log(error);
            this.setState({error_msg: "Adding failure"})
          }
        );
   }

   editAction() {
    console.log("editAction")
    if (!this.checkValidate())
      return;
    var data = this.getDataParams();
    var {formConfig, token} = this.props;
    departmentService.update_department(token, formConfig.id, data)
      .then(
          department => {
            this.setState({message: "Updating success"})
            this.reset();
            this.props.closeCallback(true, "Updating successfully");
          },
          error => {
            console.log(error);
            this.setState({error_msg: "Updating failure"})
          }
        );
   }

   reset() {
    console.log(this.state)
    this.setState({
      dataDefault: {...this.state.dataDefault},
      data: {...this.state.dataDefault},
      errorField: {}
    })

   }

   render() {
      var { companies, data, errorField} = this.state;
      var {error_msg, message} = this.state;

      console.log("COMPANIES: ", companies)

      return (
         <div>
          <Card>
              <CardBlock className="card-body">
                <Form action="" method="post">
                  {
                    error_msg? (<div className="alert alert-danger">{error_msg}</div>):("")
                  }
                  {
                    message? (<div className="alert alert-primary">{message}</div>):("")
                  }
                  
                  <FormGroup>
                    <Label htmlFor="name">Name</Label>
                    <Input type="text" id="name" name="name" value={data.name} placeholder="Enter name.."
                      onChange={this.changeHandle} className={errorField.name?"is-invalid":""}/>
                    {
                      errorField.name?(<span className="help-block">Please enter name</span>):("")
                    }
                  </FormGroup>

                  <FormGroup>
                    <Label htmlFor="email">Company</Label>
                    <Input type="select" id="companyId" name="companyId" value={data.companyId} placeholder="Enter Company.."
                      onChange={this.changeHandle} className={errorField.email?"is-invalid":""}>
                       <option></option>
                        {
                          companies.map(function(c, k){
                            return <option value={c.id} key={k}>{c.name}</option>;
                          })
                        }
                    </Input>
                    {
                      errorField.companyId?(<span className="help-block">Please select Company</span>):("")
                    }
                  </FormGroup>

                  
                </Form>
              </CardBlock>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" onClick={this.submit}><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger" onClick={this.reset}><i className="fa fa-ban"></i> Reset</Button>
                <Button size="sm" color="primary" className="active pull-right" onClick={(e)=>this.props.closeCallback(false)}>Cancel</Button>
              </CardFooter>

            </Card>

         </div>
      );
   }
}

export default FormDepartment; 