import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'; 
import $ from 'jquery'
import { Link } from 'react-router-dom';
import {
  Badge,
  Row,
  Col,
  Card,
  CardHeader,
  CardBlock,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink
} from "reactstrap";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";
// var reactNotice = require('react-notice-message');
import { ToastContainer, toast } from 'react-toastify';


import { departmentActions } from '../../../actions';
import { companyActions } from '../../../actions';

import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';

// import EditForm from './editForm'
import AddEditForm from './form'

class DepartmentsList extends React.Component {

   constructor(props) {
      super(props);
      
      this.state = {
         data: [],
         toggleEditForm: false,
         isShowForm: false,
         form: {
          isShow: false,
          type: "add",
          id: ""
         }
      }
      this.loadDataList = this.loadDataList.bind(this);
      this.toggleForm = this.toggleForm.bind(this);
      this.toggleEdit = this.toggleEdit.bind(this);
      this.loadDataList();
   };

    toggleEdit() {
    this.setState({
      toggleEditForm: !this.state.toggleEditForm
    });
    console.log(this.props)
  }

   loadDataList() {
    console.log("loadDataList");

    this.props.dispatch(departmentActions.load_departments(this.props.token));
    this.props.dispatch(companyActions.load_companies(this.props.token));
    // reactNotice.show(<p>messages</p>, {closeTime:"3000"});
   }

   delete_department(e, id) {
      if (!confirm("You are sure?")) {
        return;
      }
      if (!id)
        return;
      this.props.dispatch(departmentActions.delete_department(this.props.token, id, this.deleteCallback.bind(this)));
      
   }

   deleteCallback(success, rs) {
      if (success) {
        toast("Deleting successfully", { autoClose: 3000 ,type: toast.TYPE.SUCCESS})
      }
      else {
        console.log("show notify error")
        toast("Deleting failure", { autoClose: 3000 ,type: toast.TYPE.ERROR})
      }
   }

   closeFormCallback(success, msg) {
    console.log("closeFormCallback ", success)
    this.toggleForm();
    var option = {autoClose:3000}
    if (success) {
      this.loadDataList()
      option.type = toast.TYPE.SUCCESS;
    }
    else {
      option.type = toast.TYPE.ERROR;
    }
    toast(msg, option)
   }

   toggleForm() {
    var form = {...this.state.form};
    form.isShow = !form.isShow
    this.setState({...this.state, form})
   }

   showEditForm(e, row) {
    var form = {...this.state.form};
    form.isShow = true;
    form.id = row.id;
    form.type = "edit";
    this.setState({...this.state, form})
   }

   showAddForm() {
    var form = {...this.state.form};
    form.isShow = true;
    form.id = null;
    form.type = "add";
    this.setState({...this.state, form})
   }

   editFormatter(cell, row) {

    return (

      <div>
      <a className="badge badge-pill badge-danger" onClick={(e)=>row.this_t.delete_department(e, row.id)} data-id={row.id}>Delete</a>
      <a className="badge badge-pill badge-info" onClick={(e)=>row.this_t.showEditForm(e, row)}>Edit</a>
      </div>
    );
   }

   companyFormatter(cell, row) {
    return (cell.name);
   }
   render() {
      var this_t = this;
      var { departments, token, userInfo, companies } = this.props;
      
      console.log("BEFORE MAP: ", companies)
      departments.map(function(d){
        d.this_t = this_t;
        d.company = {}
        companies.map(function(c) {
          if (d.companyId == c.id)
            d.company  = c;
        })
      })
      console.log(departments);
      

      return (

        <div className="animated fadeIn">
          <Row>
            <Col>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i> departments list
                  <button onClick={this.loadDataList} className="pull-right btn">Refresh</button>
                  <button onClick={this.showAddForm.bind(this)} className="btn btn-primary active pull-right">Add department</button>

                </CardHeader>
                <CardBlock className="card-body">
                  <BootstrapTable data={departments} hover striped search className=" table-sm">
                      <TableHeaderColumn isKey dataField='id' hidden={true}>id</TableHeaderColumn>
                      <TableHeaderColumn dataField='name'>Name</TableHeaderColumn>
                      <TableHeaderColumn dataField='company' dataFormat={ this.companyFormatter }>Company</TableHeaderColumn>
                      <TableHeaderColumn dataFormat={ this.editFormatter }>Edit</TableHeaderColumn>
                  </BootstrapTable>
                  
                </CardBlock>
              </Card>
            </Col>
          </Row>

          <Modal isOpen={this.state.form.isShow} toggle={this.toggleForm}
                 className={'modal-success ' + this.props.className} >
            <ModalHeader toggle={this.toggleForm}>{this.state.form.type=="add"?"Add department":"Edit department"}</ModalHeader>
            <ModalBody>
              <AddEditForm token={token} closeCallback={this.closeFormCallback.bind(this)}
               formConfig={this.state.form}/>
            </ModalBody>
          </Modal>

          <ToastContainer autoClose={5000} />
         </div>
      );
   }
}




function mapStateToProps(state) {
    const { companies } = state.company;
    const { departments } = state.department;
    const { user, userInfo } = state.authentication;
    return {
        departments,
        token: user, userInfo,
        companies
    };
}
function registActions (dispatch, props) {
  return {
    dispatch,
    ...bindActionCreators({
        ...departmentActions,
        ...companyActions
      }, dispatch)
  }
}

const connectedLoginPage = connect(mapStateToProps, registActions)(DepartmentsList);
export default connectedLoginPage; 