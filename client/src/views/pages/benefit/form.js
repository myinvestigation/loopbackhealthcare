import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'; 
import { benefitActions } from '../../../actions';
import { Link } from 'react-router-dom';
import { benefitService } from '../../../services'

import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton
} from "reactstrap";

class FormBenefit extends React.Component {
  constructor(props) {
      super(props);
      
      this.state = {
         data:{
          code: "",
          minSumInsured: "",
          maxSumInsured: "",
          nameVn: "",
          nameEn: "",
          benefitType: ""
         },
         dataDefault:{
          code: "",
          minSumInsured: "",
          maxSumInsured: "",
          nameVn: "",
          nameEn: "",
          benefitType: ""
         },
         errorField:{},
         error_msg:"",
         message:""
      }
      this.changeHandle = this.changeHandle.bind(this)
      this.reset = this.reset.bind(this)
      const {formConfig} = this.props;

      if (formConfig.type == "edit") {
        this.load_data();
      }
      else if (formConfig.type == "edit_sub") {
        this.state.data = formConfig.benefit;
        this.state.dataDefault = formConfig.benefit;
        this.state.rootBenefit = formConfig.rootBenefit;

        console.log("CONSTRUCTOR FORM, state: ",this.state)
      }
      else if (formConfig.type == "add_sub") {
        this.state.rootBenefit = formConfig.rootBenefit;
      }
   };

   load_data() {
    const {token, formConfig} = this.props
    benefitService.get_benefit(token, formConfig.id).then(
      data => {
        console.log("load_data success: ", data)
        var d = {
          code: data.code,
          minSumInsured: data.minSumInsured,
          benefitType: data.benefitType,
          maxSumInsured: data.maxSumInsured,
          name: data.name,
          nameEn: data.name.en,
          nameVn: data.name.vn,
          subBenefits: data.subBenefits
        }
        this.setState({data:d, dataDefault: d})
      },
      error => {
        console.log("load_data error: ", error)
        this.setState({error_msg: "Get user info failure"})
        this.props.closeCallback(false, "Get benefit info failure");
      }
    );
   }

   changeHandle(e) {
      console.log(this.state)
      var requireField = ["name", "code"];
      const { name, value } = e.target;
      var data = this.state.data;
      var errorField = this.state.errorField

      data[name] = value;
      errorField[name] = (requireField.indexOf(name) != -1 && value == "")

      this.setState({ ...this.state, data , errorField});
   }

   checkValidate() {
    var errorField = this.state.errorField;
    var data = this.state.data;
    var rs = true;

    ["name", "code"].forEach(function(name) {
      let err = (data[name] == "");
      errorField[name] = err;
      rs &= !err;
    })

    this.setState({data, errorField})
    return rs;
   }

   submit(e, data) {
    console.log("STATE: ", this.state)
    console.log("STATE2: ", data)

    var {formConfig} = this.props
    if (formConfig.type == "add") {
      this.addAction()
    }
    else if (formConfig.type == "edit") {
      this.editAction()
    }
    else if (formConfig.type == "edit_sub") {
      this.editSubAction(data);
    }
    else if (formConfig.type == "add_sub") {
      this.addSubAction();
    }
    else {
      console.log("Unknow form type")
    }
   }

   convertDataParam(data) {
     return {
        code: data.code,
        minSumInsured: data.minSumInsured,
        maxSumInsured: data.maxSumInsured,
        benefitType: data.benefitType,
        name: {
          vn: data.nameVn,
          en: data.nameEn
        }
     }
   }

   getDataParams() {
    var data = this.state.data;
    return this.convertDataParam(data)
   }

   addAction() {
    console.log("addAction")
    if (!this.checkValidate())
      return;
    var d = this.getDataParams();
    benefitService.create_benefit(this.props.token, d)
      .then(
          benefit => {
            this.setState({message: "Adding success"})
            this.reset();
            this.props.closeCallback(true, "Adding successfully");
          },
          error => {S
            console.log(error);
            this.setState({error_msg: "Adding failure"})
          }
        );
   }

   editAction() {
    console.log("editAction")
    if (!this.checkValidate())
      return;
    var data = this.getDataParams();
    data.subBenefits = this.state.data.subBenefits;
    var {formConfig, token} = this.props;
    benefitService.update_benefit(token, formConfig.id, data)
      .then(
          benefit => {
            this.setState({message: "Updating success"})
            this.reset();
            this.props.closeCallback(true, "Updating successfully");
          },
          error => {
            console.log(error);
            this.setState({error_msg: "Updating failure"})
          }
        );
   }

   editSubAction(dataForm) {
    console.log("editSubAction()")
    var rootBenefit = this.state.rootBenefit;
    var stateBenefit = dataForm;//this.getDataParams();
    // var subs = rootBenefit.subBenefits;

    console.log("Sub: dataForm:", dataForm, "stateBenefit: ", stateBenefit);
    var subBenefits = []
    var this_t = this;
    rootBenefit.subBenefits.map(function(be, key){
      if (key == stateBenefit.context.key) {
        be = {...stateBenefit};
      }
      subBenefits.push(this_t.convertDataParam(be));
    })

    var data = this.convertDataParam(rootBenefit);

    data.subBenefits = subBenefits;
    this.props.dispatch(benefitActions.update_benefit(this.props.token, rootBenefit.id, data, this.callbackUdateSub.bind(this)) )
  
   }

  addSubAction() {
    console.log("addSubAction()")
    var rootBenefit = this.state.rootBenefit;
    var stateBenefit = this.convertDataParam(this.state.data);
    // var subs = rootBenefit.subBenefits;

    console.log("Sub: ", this.rootBenefit);
    var subBenefits = []
    var this_t = this;
    rootBenefit.subBenefits = rootBenefit.subBenefits || [];
    rootBenefit.subBenefits.map(function(be, key){
      subBenefits.push(this_t.convertDataParam(be));
    })
    subBenefits.push(stateBenefit);

    var data = this.convertDataParam(rootBenefit);
    data.subBenefits = subBenefits;
    this.props.dispatch(benefitActions.update_benefit(this.props.token, rootBenefit.id, data, this.callbackAddSub.bind(this)) )

   }

   callbackUdateSub(success, rs) {
    console.log("callbackUdateSub")
    if (success) {
      this.props.closeCallback(true, "Updating sub-benefit successfully");
    }
    else {
      console.log(error);
      this.setState({error_msg: "Updating sub-benefit failure"})
    }
   }

   callbackAddSub(success, rs) {
    console.log("callbackUdateSub")
    if (success) {
      this.props.closeCallback(true, "Adding sub-benefit successfully");
    }
    else {
      console.log(error);
      this.setState({error_msg: "Adding sub-benefit failure"})
    }
   }

   reset() {
    console.log(this.state)
    this.setState({
      dataDefault: {...this.state.dataDefault},
      data: {...this.state.dataDefault},
      errorField: {}
    })

   }

   render() {
      var {data, errorField} = this.state;
      var {error_msg, message} = this.state;
      console.log("RENDER data: ", data)

      return (
         <div>
          <Card>
              <CardBody className="card-body">
                <Form action="" method="post">
                  {
                    error_msg? (<div className="alert alert-danger">{error_msg}</div>):("")
                  }
                  {
                    message? (<div className="alert alert-primary">{message}</div>):("")
                  }

                  <FormGroup>
                    <Label htmlFor="code">Code</Label>
                    <Input type="text" id="code" name="code" value={data.code} placeholder="Enter code.."
                      onChange={this.changeHandle} className={errorField.code?"is-invalid":""}/>
                    {
                      errorField.code?(<span className="help-block">Please enter code</span>):("")
                    }
                  </FormGroup>
                  
                  <FormGroup>
                    <Label htmlFor="nameEn">Name En</Label>
                    <Input type="textarea" id="nameEn" name="nameEn" value={data.nameEn} placeholder="Enter English name.."
                      onChange={this.changeHandle} className={errorField.nameEn?"is-invalid":""}/>
                    {
                      errorField.nameEn?(<span className="help-block">Please enter English name</span>):("")
                    }
                  </FormGroup>

                  <FormGroup>
                    <Label htmlFor="nameVn">Name Vn</Label>
                    <Input type="textarea" id="nameVn" name="nameVn" value={data.nameVn} placeholder="Enter Vietnamese name.."
                      onChange={this.changeHandle} className={errorField.nameVn?"is-invalid":""}/>
                    {
                      errorField.nameVn?(<span className="help-block">Please enter Vietnamese name</span>):("")
                    }
                  </FormGroup>

                  <FormGroup>
                    <Label htmlFor="minSumInsured">Min sum Insured</Label>
                    <Input type="number" id="minSumInsured" name="minSumInsured" value={data.minSumInsured} placeholder="Enter Min sum Insured.."
                      onChange={this.changeHandle} className={errorField.minSumInsured?"is-invalid":""}/>
                    {
                      errorField.minSumInsured?(<span className="help-block">Please enter Min sum Insured</span>):("")
                    }
                  </FormGroup>

                  <FormGroup>
                    <Label htmlFor="maxSumInsured">Max sum Insured</Label>
                    <Input type="number" id="maxSumInsured" name="maxSumInsured" value={data.maxSumInsured} placeholder="Enter Max sum Insured.."
                      onChange={this.changeHandle} className={errorField.maxSumInsured?"is-invalid":""}/>
                    {
                      errorField.maxSumInsured?(<span className="help-block">Please enter Max sum Insured</span>):("")
                    }
                  </FormGroup>

                  <FormGroup>
                    <Label htmlFor="benefitType">Benefit type</Label>
                    <Input type="text" id="benefitType" name="benefitType" value={data.benefitType} placeholder="Enter Benefit type.."
                      onChange={this.changeHandle} className={errorField.benefitType?"is-invalid":""}/>
                    {
                      errorField.benefitType?(<span className="help-block">Please enter Benefit type</span>):("")
                    }
                  </FormGroup>

                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" onClick={(e)=>this.submit(e, {...data})}><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger" onClick={this.reset}><i className="fa fa-ban"></i> Reset</Button>
                <Button size="sm" color="primary" className="active pull-right" onClick={(e)=>this.props.closeCallback(false)}>Cancel</Button>
              </CardFooter>

            </Card>

         </div>
      );
   }
}

export default FormBenefit; 
