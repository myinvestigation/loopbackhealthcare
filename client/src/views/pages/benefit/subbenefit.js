import React from 'react';
import { Link } from 'react-router-dom';
import {
  Badge,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink
} from "reactstrap";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";
// var reactNotice = require('react-notice-message');
import { ToastContainer, toast } from 'react-toastify';


import { benefitActions } from '../../../actions';

import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';

// import EditForm from './editForm'
import AddEditForm from './form'

class SubBenefit extends React.Component {

   constructor(props) {
      super(props);
      
      this.state = {
         benefits: this.props.subBenefits || []
      }

      console.log("SUBBENEFIT: props: ", this.props)
   };


   showEditSubForm(rootBenefit, row) {
    console.log("SUBBENEFIT: showEditSubForm()")
    row.context.this_t.props.parent.showEditSubForm(rootBenefit, row)
   }

  componentWillReceiveProps(nextProp) {
    console.log("componentWillReceiveProps");
    var benefits = nextProp.subBenefits || []
    this.setState({benefits})
  }

  onBeforeSaveCell(row, cellName, cellValue) {

    console.log("onBeforeSaveCell: row", row, "cellName: ", cellName, "cellValue: ",cellValue)
    var rootBenefit = row.context.this_t.props.benefit;
    // console.log("STATE: ", row.this_t.state)
    var benefits = row.context.this_t.state.benefits;

    var subBenefits = []
    benefits.map(function(be, key) {
      if (key == row.context.key) {
        be[cellName] = cellValue;

        if (cellName == "nameEn") 
          be.name.en = cellValue;
        if (cellName == "nameVn")
          be.name.vn = cellValue;
      }
      // convert
      let data = {
        code: be.code,
        name: {
          en: be.nameEn,
          vn: be.nameVn
        },
        minSumInsured: be.minSumInsured,
        maxSumInsured: be.maxSumInsured,
        benefitType: be.benefitType
      }
      subBenefits.push(data)
    })

    row.context.this_t.setState({benefits})

    console.log("PROPS: ", row.context.this_t.props)
    var parent = row.context.this_t.props.parent;
    var token = parent.props.token;

    var data = {
      code: rootBenefit.code,
      name: {
        en: rootBenefit.nameEn,
        vn: rootBenefit.nameVn
      },
      minSumInsured: rootBenefit.minSumInsured,
      maxSumInsured: rootBenefit.maxSumInsured,
      benefitType: rootBenefit.benefitType,
      subBenefits: subBenefits
    }
    console.log("DATA: ", data)
    console.log("JSON: ", JSON.stringify(data))
    parent.props.dispatch(benefitActions.update_benefit(token, rootBenefit.id, data, 
      row.context.this_t.props.callbackEditCell))

    return true;
  }

  delete_sub(row) {
    console.log("Delete sub")
    if (!confirm("You are sure?"))
      return;
    var rootBenefit = row.context.this_t.props.benefit;
    // console.log("STATE: ", row.this_t.state)
    var benefits = row.context.this_t.state.benefits;

    var subBenefits = []
    benefits.map(function(be, key) {
      if (be.code != row.code) {
        let data = {
        code: be.code,
        name: {
          en: be.nameEn,
          vn: be.nameVn
        },
        minSumInsured: be.minSumInsured,
        maxSumInsured: be.maxSumInsured,
        benefitType: be.benefitType
      }
      subBenefits.push(data)
      }
      else {
        // delete benefits[key]
      }
    })

    row.context.this_t.setState({benefits})

    console.log("PROPS: ", row.context.this_t.props)
    var parent = row.context.this_t.props.parent;
    var token = parent.props.token;

    var data = {
      code: rootBenefit.code,
      name: {
        en: rootBenefit.nameEn,
        vn: rootBenefit.nameVn
      },
      minSumInsured: rootBenefit.minSumInsured,
      maxSumInsured: rootBenefit.maxSumInsured,
      benefitType: rootBenefit.benefitType,
      subBenefits: subBenefits
    }
    console.log("DATA: ", data)
    console.log("JSON: ", JSON.stringify(data))
    parent.props.dispatch(benefitActions.update_benefit(token, rootBenefit.id, data, 
      row.context.this_t.props.callbackEditCell))

  }

  onAfterSaveCell(row, cellName, cellValue) {
    console.log("onAfterSaveCell: row", row, "cellName: ", cellName, "cellValue: ",cellValue)
  }

  nameVnFormatter(cell, row) {
    console.log("NAME VN: ",cell)
    return cell.vn
  }
  nameEnFormatter(cell, row) {
    return cell.en
  }

  editFormatter(cell, row) {
    var rootBenefit = row.context.this_t.props.benefit;
    return (

      <div>
      <a className="badge badge-pill badge-danger" onClick={(e)=>row.context.this_t.delete_sub(row)}>Delete</a>
      <a className="badge badge-pill badge-info" 
      onClick={(e)=>row.context.this_t.showEditSubForm(rootBenefit, row)}>Edit</a>
      </div>
    );
  }

   render() {
      
      var context = {
        this_t: this
      };
      // var { token, userInfo } = this.props;
      var { benefits } = this.state;
      
      benefits.forEach(function(u, key){
        u.context = {...context};
        u.context.key = key
        u.nameEn = u.name.en;
        u.nameVn = u.name.vn;
      })

      const cellEditProp = {
        mode: 'click',
        blurToSave: true,
        beforeSaveCell: this.onBeforeSaveCell, // a hook for before saving cell
        afterSaveCell: this.onAfterSaveCell  // a hook for after saving cell
      };
      return (

        <div className="animated fadeIn">
          <Row>
            <Col>
              <Card>
                
                <CardBody className="card-body">
                  <BootstrapTable data={benefits} hover striped className=" table-sm"
                    cellEdit={ cellEditProp }
                  >
                      
                      <TableHeaderColumn isKey dataField='code' editable={ { type: 'text' } }>Code</TableHeaderColumn>
                      <TableHeaderColumn dataField='nameEn' editable={ { type: 'textarea' } }>Name En</TableHeaderColumn>
                      <TableHeaderColumn dataField='nameVn' editable={ { type: 'textarea' } }>Name Vn</TableHeaderColumn>
                      <TableHeaderColumn dataField='minSumInsured' editable={ { type: 'number' } }>Min Sum Insured</TableHeaderColumn>
                      <TableHeaderColumn dataField='maxSumInsured' editable={ { type: 'number' } }>Max Sum Insured</TableHeaderColumn>
                      <TableHeaderColumn dataField='benefitType' editable={ { type: 'text' } }>Benefit type</TableHeaderColumn>
                      <TableHeaderColumn editable={false} dataFormat={ this.editFormatter }>Edit</TableHeaderColumn>
                  </BootstrapTable>
                  
                </CardBody>
              </Card>
            </Col>
          </Row>

         </div>
      );
   }
}

export default SubBenefit; 