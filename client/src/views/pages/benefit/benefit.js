import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'; 
import { Link } from 'react-router-dom';
import {
  Badge,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink
} from "reactstrap";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";
// var reactNotice = require('react-notice-message');
import { ToastContainer, toast } from 'react-toastify';


import { benefitActions } from '../../../actions';

import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';

// import EditForm from './editForm'
import AddEditForm from './form'
import SubBenefit from './subbenefit'

class BenefitsList extends React.Component {

   constructor(props) {
      super(props);
      
      this.state = {
         data: [],
         toggleEditForm: false,
         isShowForm: false,
         form: {
          isShow: false,
          type: "add",
          id: ""
         }
      }
      this.loadDataList = this.loadDataList.bind(this);
      this.toggleForm = this.toggleForm.bind(this);
      this.toggleEdit = this.toggleEdit.bind(this);
      this.loadDataList();
   };

    toggleEdit() {
    this.setState({
      toggleEditForm: !this.state.toggleEditForm
    });
    console.log(this.props)
  }

   loadDataList() {
    console.log("loadDataList");

    this.props.dispatch(benefitActions.load_benefits(this.props.token));
    // reactNotice.show(<p>messages</p>, {closeTime:"3000"});
   }

   delete_benefit(e, id) {
      if (!confirm("You are sure?")) {
        return;
      }
      if (!id)
        return;
      this.props.dispatch(benefitActions.delete_benefit(this.props.token, id, this.deleteCallback.bind(this)));
      
   }

   deleteCallback(success, rs) {
      if (success) {
        toast("Deleting successfully", { autoClose: 3000 ,type: toast.TYPE.SUCCESS})
      }
      else {
        console.log("show notify error")
        toast("Deleting failure", { autoClose: 3000 ,type: toast.TYPE.ERROR})
      }
   }

   closeFormCallback(success, msg) {
    console.log("closeFormCallback ", success)
    this.toggleForm();
    var option = {autoClose:3000}
    if (success) {
      this.loadDataList()
      option.type = toast.TYPE.SUCCESS;
    }
    else {
      option.type = toast.TYPE.ERROR;
    }
    toast(msg, option)
   }

   toggleForm() {
    var form = {...this.state.form};
    form.isShow = !form.isShow
    this.setState({...this.state, form})
   }

   showEditForm(e, row) {
    var form = {...this.state.form};
    form.isShow = true;
    form.id = row.id;
    form.type = "edit";
    this.setState({...this.state, form})
   }

   showAddForm() {
    var form = {...this.state.form};
    form.isShow = true;
    form.id = null;
    form.type = "add";
    this.setState({...this.state, form})
   }


   // subtable
   callbackEditCell(success, res) {
      console.log("callbackEditCell")
      if (success) {
        toast("Updating successfully", { autoClose: 2000 ,type: toast.TYPE.SUCCESS})
      }
      else {
        console.log("show notify error")
        toast("Updating failure", { autoClose: 3000 ,type: toast.TYPE.ERROR})
      }
   }

   showEditSubForm(rootBenefit, row) {
    console.log("PARENT BENEFIT: showEditSubForm()")
    // row.context.this_t.props.parent.showEditSubForm(rootBenefit, row)
    var form = {...this.state.form};
      form.isShow = true;
      form.id = null;
      form.type = "edit_sub";
      form.rootBenefit = rootBenefit,
      form.benefit = row,
      this.setState({...this.state, form})
     }

   isExpandableRow(row) {
      return true;
   }

   showAddSubForm(e, row) {
    var form = {...this.state.form};
    form.isShow = true;
    form.id = null;
    form.type = "add_sub";
    form.rootBenefit = row,
    this.setState({...this.state, form})
     
   }

   expandComponent(row) {
    console.log("ExpandComponent: ROW: ", row, this)
      return (
        <SubBenefit parent={row.this_t} benefit={row} subBenefits={row.subBenefits} callbackEditCell={row.this_t.callbackEditCell}/>
      );
   }



   editFormatter(cell, row) {

    return (


      <div>
      <a className="badge badge-pill badge-danger" onClick={(e)=>row.this_t.delete_benefit(e, row.id)} data-id={row.id}>Delete</a>
      <a className="badge badge-pill badge-info" onClick={(e)=>row.this_t.showEditForm(e, row)}>Edit</a>
      <a className="badge badge-pill badge-success" onClick={(e)=>row.this_t.showAddSubForm(e, row)}>Add sub</a>
      </div>
    );
   }
   render() {
      var this_t = this;
      var { benefits, token, userInfo } = this.props;
      
      benefits.map(function(u){
        u.this_t = this_t;
        u.nameEn = u.name.en;
        u.nameVn = u.name.vn;
      })
      

      return (

        <div className="animated fadeIn">
          <Row>
            <Col>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i> Benefits list
                  <button onClick={this.loadDataList} className="pull-right btn">Refresh</button>
                  <button onClick={this.showAddForm.bind(this)} className="btn btn-primary active pull-right">Add benefit</button>

                </CardHeader>
                <CardBody className="card-body">
                  <BootstrapTable data={benefits} hover striped search className=" table-sm"
                    expandableRow={ this.isExpandableRow }
                    expandComponent={ this.expandComponent }
                  >
                      <TableHeaderColumn isKey dataField='id' hidden={true}>id</TableHeaderColumn>
                      <TableHeaderColumn dataField='code'>Code</TableHeaderColumn>
                      <TableHeaderColumn dataField='nameEn' >Name En</TableHeaderColumn>
                      <TableHeaderColumn dataField='nameVn' >Name Vn</TableHeaderColumn>
                      <TableHeaderColumn dataField='minSumInsured'>Min Sum Insured</TableHeaderColumn>
                      <TableHeaderColumn dataField='maxSumInsured'>Max Sum Insured</TableHeaderColumn>
                      <TableHeaderColumn dataField='benefitType'>Benefit type</TableHeaderColumn>
                      <TableHeaderColumn dataFormat={ this.editFormatter } expandable={false} editable={false}>Edit</TableHeaderColumn>
                  </BootstrapTable>
                  
                </CardBody>
              </Card>
            </Col>
          </Row>

          <Modal isOpen={this.state.form.isShow} toggle={this.toggleForm}
                 className={'modal-success ' + this.props.className} >
            <ModalHeader toggle={this.toggleForm}>{this.state.form.type=="add"?"Add benefit":"Edit benefit"}</ModalHeader>
            <ModalBody>
              <AddEditForm dispatch={this.props.dispatch} token={token} closeCallback={this.closeFormCallback.bind(this)}
               formConfig={this.state.form} />
            </ModalBody>
          </Modal>

          <ToastContainer autoClose={5000} />
         </div>
      );
   }
}




function mapStateToProps(state) {
    const { benefits, error_msg } = state.benefit;
    const { user, userInfo } = state.authentication;
    return {
        benefits, error_msg,
        token: user, userInfo
    };
}
function registActions (dispatch, props) {
  return {
    dispatch,
    ...bindActionCreators({
        ...benefitActions
      }, dispatch)
  }
}

const Page = connect(mapStateToProps, registActions)(BenefitsList);
export default Page; 
