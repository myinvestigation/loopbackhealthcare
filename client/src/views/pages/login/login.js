import React, {Component} from "react";
import {Container, Row, Col, CardGroup, Card, CardBlock, Button, Input, InputGroup, InputGroupAddon} from "reactstrap";
import { Link } from 'react-router-dom';
import { authActions } from '../../../actions';
import { authentication } from '../../../reducers';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'; 

class Login extends Component {

  constructor(props) {
      super(props);
       const { dispatch } = this.props;
      this.state = {
         username: "",
         password: "",
         error: {
          username:"",
          password:""
         },
         success: false
      }

      this.login = this.login.bind(this);
      this.handleChange = this.handleChange.bind(this);
   };

   componentWillUpdate (nextProps, nextState) {
    console.log("componentWillUpdate")
    const { loggedIn } = this.props

    if (nextProps.loggedIn){
      console.log("redirect to admin")
      this.props.history.push("/admin/")
    }
  }

   check_empty() {
      var msg = "This field is not empty";
      var error = {
        username: this.state.username? "":msg,
        password: this.state.password? "":msg
      }
      this.setState({...this.state, error})
      return this.state.username && this.state.password
   }

   login() {
    
    console.log("login")
    if (!this.check_empty())
     return;

    const { dispatch } = this.props;

    console.log(this.state);
    dispatch(authActions.login(this.state.username, this.state.password))

    
   }

  handleChange(e) {
      const { name, value } = e.target;
      let msg = (value? "":"This field is not empty")
      var error = this.state.error || {};
      error[name] = msg;

      this.setState({ [name]: value , error});
      
  }

  render() {
    const { loggingIn, user, userInfo, msgError } = this.props;
    const {username, password, error} = this.state;


    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup className="mb-0">
                <Card className="p-4">
                  <CardBlock className="card-body">
                    <h1>Login</h1>
                    <p className="text-muted">Sign In to your account</p>
                    <div className=" has-error">{error.username}</div>
                    <InputGroup className="mb-3">
                      <InputGroupAddon><i className="icon-user"></i></InputGroupAddon>
                      <Input type="text" value={username} name="username" onChange={this.handleChange}/>
                    </InputGroup>
                    <div className=" has-error">{error.password}</div>
                    <InputGroup className="mb-4">
                      <InputGroupAddon><i className="icon-lock"></i></InputGroupAddon>
                      <Input type="password" value={password} name="password" onChange={this.handleChange}/>
                    </InputGroup>
                    
                    <div>{msgError}</div>
                    <Row>
                      <Col xs="6">
                        <Button color="primary" className="px-4" onClick={this.login}>Login</Button>
                      </Col>
                      <Col xs="6" className="text-right">
                        <Button color="link" className="px-0">Forgot password?</Button>
                      </Col>
                    </Row>
                  </CardBlock>
                </Card>
                <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: 44 + '%' }}>
                  <CardBlock className="card-body text-center">
                    <div>
                      <h2>Sign up</h2>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua.</p>
                      <Link to="/auth/register" className="mt-3 btn btn-primary active">Register Now!</Link>
                    </div>
                  </CardBlock>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>

      </div>
    );
  }
}


function mapStateToProps(state) {
    const { loggedIn, user, userInfo, msgError } = state.authentication;
    return {
        loggedIn, user, userInfo, msgError
    };
}
function registActions (dispatch, props) {
  return {
    dispatch,
    ...bindActionCreators({
        ...authActions
      }, dispatch)
  }
}

const Page = connect(mapStateToProps, registActions)(Login);
export default Page; 
