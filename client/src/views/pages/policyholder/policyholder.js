import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'; 
import $ from 'jquery'
import { Link } from 'react-router-dom';
import {
  Badge,
  Row,
  Col,
  Card,
  CardHeader,
  CardBlock,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink
} from "reactstrap";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";
// var reactNotice = require('react-notice-message');
import { ToastContainer, toast } from 'react-toastify';


import { policyholderActions } from '../../../actions';

import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';

// import EditForm from './editForm'
import AddEditForm from './form'

class PolicyholdersList extends React.Component {

   constructor(props) {
      super(props);
      
      this.state = {
         data: [],
         toggleEditForm: false,
         isShowForm: false,
         form: {
          isShow: false,
          type: "add",
          id: ""
         }
      }
      this.loadDataList = this.loadDataList.bind(this);
      this.toggleForm = this.toggleForm.bind(this);
      this.toggleEdit = this.toggleEdit.bind(this);
      this.loadDataList();
   };

    toggleEdit() {
    this.setState({
      toggleEditForm: !this.state.toggleEditForm
    });
    console.log(this.props)
  }

   loadDataList() {
    console.log("loadDataList");

    this.props.dispatch(policyholderActions.load_policyholders(this.props.token));
    // reactNotice.show(<p>messages</p>, {closeTime:"3000"});
   }

   delete_policyholder(e, id) {
      if (!confirm("You are sure?")) {
        return;
      }
      if (!id)
        return;
      this.props.dispatch(policyholderActions.delete_policyholder(this.props.token, id, this.deleteCallback.bind(this)));
      
   }

   deleteCallback(success, rs) {
      if (success) {
        toast("Deleting successfully", { autoClose: 3000 ,type: toast.TYPE.SUCCESS})
      }
      else {
        console.log("show notify error")
        toast("Deleting failure", { autoClose: 3000 ,type: toast.TYPE.ERROR})
      }
   }

   closeFormCallback(success, msg) {
    console.log("closeFormCallback ", success)
    this.toggleForm();
    var option = {autoClose:3000}
    if (success) {
      this.loadDataList()
      option.type = toast.TYPE.SUCCESS;
    }
    else {
      option.type = toast.TYPE.ERROR;
    }
    toast(msg, option)
   }

   toggleForm() {
    var form = {...this.state.form};
    form.isShow = !form.isShow
    this.setState({...this.state, form})
   }

   showEditForm(e, row) {
    var form = {...this.state.form};
    form.isShow = true;
    form.id = row.id;
    form.type = "edit";
    this.setState({...this.state, form})
   }

   showAddForm() {
    var form = {...this.state.form};
    form.isShow = true;
    form.id = null;
    form.type = "add";
    this.setState({...this.state, form})
   }

   editFormatter(cell, row) {

    return (

      <div>
      <a className="badge badge-pill badge-danger" onClick={(e)=>row.this_t.delete_policyholder(e, row.id)} data-id={row.id}>Delete</a>
      <a className="badge badge-pill badge-info" onClick={(e)=>row.this_t.showEditForm(e, row)}>Edit</a>
      </div>
    );
   }
   render() {
      var this_t = this;
      var { policyholders, token, userInfo } = this.props;
      
      policyholders.map(function(u){
        u.this_t = this_t;
      })
      

      return (

        <div className="animated fadeIn">
          <Row>
            <Col>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i> Policy Holders list
                  <button onClick={this.loadDataList} className="pull-right btn">Refresh</button>
                  <button onClick={this.showAddForm.bind(this)} className="btn btn-primary active pull-right">Add policyholder</button>

                </CardHeader>
                <CardBlock className="card-body">
                  <BootstrapTable data={policyholders} hover striped search className=" table-sm">
                      <TableHeaderColumn isKey dataField='id' hidden={true}>id</TableHeaderColumn>
                      <TableHeaderColumn dataSort dataField='name'>Name</TableHeaderColumn>
                      <TableHeaderColumn dataSort dataField='taxCode'>Tax code</TableHeaderColumn>
                      <TableHeaderColumn dataSort dataField='address'>Address</TableHeaderColumn>
                      <TableHeaderColumn dataSort dataField='telephone'>Telephone</TableHeaderColumn>
                      <TableHeaderColumn dataSort dataField='activities'>Activities</TableHeaderColumn>
                      <TableHeaderColumn dataSort dataField='contactPerson'>Contact person</TableHeaderColumn>
                      <TableHeaderColumn dataFormat={ this.editFormatter }>Edit</TableHeaderColumn>
                  </BootstrapTable>
                  
                </CardBlock>
              </Card>
            </Col>
          </Row>

          <Modal isOpen={this.state.form.isShow} toggle={this.toggleForm}
                 className={'modal-success ' + this.props.className} >
            <ModalHeader toggle={this.toggleForm}>{this.state.form.type=="add"?"Add Policy Holder":"Edit Policy Holder"}</ModalHeader>
            <ModalBody>
              <AddEditForm token={token} closeCallback={this.closeFormCallback.bind(this)}
               formConfig={this.state.form} />
            </ModalBody>
          </Modal>

          <ToastContainer autoClose={5000} />
         </div>
      );
   }
}




function mapStateToProps(state) {
    const { policyholders, error_msg } = state.policyholder;
    const { user, userInfo } = state.authentication;
    return {
        policyholders, error_msg,
        token: user, userInfo
    };
}
function registActions (dispatch, props) {
  return {
    dispatch,
    ...bindActionCreators({
        ...policyholderActions
      }, dispatch)
  }
}

const Page = connect(mapStateToProps, registActions)(PolicyholdersList);
export default Page; 