import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'; 
import $ from 'jquery'
import { Link } from 'react-router-dom';
import {
  Badge,
  Row,
  Col,
  Card,
  CardHeader,
  CardBlock,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink
} from "reactstrap";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";
// var reactNotice = require('react-notice-message');
import { ToastContainer, toast } from 'react-toastify';


import { hospitalActions } from '../../../actions';

import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';

// import EditForm from './editForm'
import AddEditForm from './form'

class HospitalsList extends React.Component {

   constructor(props) {
      super(props);
      
      this.state = {
         data: [],
         toggleEditForm: false,
         isShowForm: false,
         form: {
          isShow: false,
          type: "add",
          id: ""
         }
      }
      this.loadDataList = this.loadDataList.bind(this);
      this.toggleForm = this.toggleForm.bind(this);
      this.toggleEdit = this.toggleEdit.bind(this);
      this.loadDataList();
   };

    toggleEdit() {
    this.setState({
      toggleEditForm: !this.state.toggleEditForm
    });
    console.log(this.props)
  }

   loadDataList() {
    console.log("loadDataList");

    this.props.dispatch(hospitalActions.load_hospitals(this.props.token));
    // reactNotice.show(<p>messages</p>, {closeTime:"3000"});
   }

   delete_hospital(e, id) {
      if (!confirm("You are sure?")) {
        return;
      }
      if (!id)
        return;
      this.props.dispatch(hospitalActions.delete_hospital(this.props.token, id, this.deleteCallback.bind(this)));
      
   }

   deleteCallback(success, rs) {
      if (success) {
        toast("Deleting successfully", { autoClose: 3000 ,type: toast.TYPE.SUCCESS})
      }
      else {
        console.log("show notify error")
        toast("Deleting failure", { autoClose: 3000 ,type: toast.TYPE.ERROR})
      }
   }

   closeFormCallback(success, msg) {
    console.log("closeFormCallback ", success)
    this.toggleForm();
    var option = {autoClose:3000}
    if (success) {
      this.loadDataList()
      option.type = toast.TYPE.SUCCESS;
    }
    else {
      option.type = toast.TYPE.ERROR;
    }
    toast(msg, option)
   }

   toggleForm() {
    var form = {...this.state.form};
    form.isShow = !form.isShow
    this.setState({...this.state, form})
   }

   showEditForm(e, row) {
    var form = {...this.state.form};
    form.isShow = true;
    form.id = row.id;
    form.type = "edit";
    this.setState({...this.state, form})
   }

   showAddForm() {
    var form = {...this.state.form};
    form.isShow = true;
    form.id = null;
    form.type = "add";
    this.setState({...this.state, form})
   }

   editFormatter(cell, row) {

    return (

      <div>
      <a className="badge badge-pill badge-danger" onClick={(e)=>row.this_t.delete_hospital(e, row.id)} data-id={row.id}>Delete</a>
      <a className="badge badge-pill badge-info" onClick={(e)=>row.this_t.showEditForm(e, row)}>Edit</a>
      </div>
    );
   }
   render() {
      var this_t = this;
      var { hospitals, token, userInfo } = this.props;
      
      hospitals.map(function(u){
        u.this_t = this_t;
      })
      

      return (

        <div className="animated fadeIn">
          <Row>
            <Col>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i> Hospitals list
                  <button onClick={this.loadDataList} className="pull-right btn">Refresh</button>
                  <button onClick={this.showAddForm.bind(this)} className="btn btn-primary active pull-right">Add hospital</button>

                </CardHeader>
                <CardBlock className="card-body">
                  <BootstrapTable data={hospitals} hover striped search className=" table-sm">
                      <TableHeaderColumn isKey dataField='id' hidden={true}>id</TableHeaderColumn>
                      <TableHeaderColumn dataField='code'>Code</TableHeaderColumn>
                      <TableHeaderColumn dataField='name'>Name</TableHeaderColumn>
                      <TableHeaderColumn dataField='address'>Address</TableHeaderColumn>
                      <TableHeaderColumn dataField='telephone'>Telephone</TableHeaderColumn>
                      <TableHeaderColumn dataFormat={ this.editFormatter }>Edit</TableHeaderColumn>
                  </BootstrapTable>
                  
                </CardBlock>
              </Card>
            </Col>
          </Row>

          <Modal isOpen={this.state.form.isShow} toggle={this.toggleForm}
                 className={'modal-success ' + this.props.className} >
            <ModalHeader toggle={this.toggleForm}>{this.state.form.type=="add"?"Add hospital":"Edit hospital"}</ModalHeader>
            <ModalBody>
              <AddEditForm token={token} closeCallback={this.closeFormCallback.bind(this)}
               formConfig={this.state.form} />
            </ModalBody>
          </Modal>

          <ToastContainer autoClose={5000} />
         </div>
      );
   }
}




function mapStateToProps(state) {
    const { hospitals, error_msg } = state.hospital;
    const { user, userInfo } = state.authentication;
    return {
        hospitals, error_msg,
        token: user, userInfo
    };
}
function registActions (dispatch, props) {
  return {
    dispatch,
    ...bindActionCreators({
        ...hospitalActions
      }, dispatch)
  }
}

const Page = connect(mapStateToProps, registActions)(HospitalsList);
export default Page; 