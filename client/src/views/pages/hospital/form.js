import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'; 
import { hospitalActions } from '../../../actions';
import { Link } from 'react-router-dom';
import { hospitalService } from '../../../services'

import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBlock,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton
} from "reactstrap";

class FormHospital extends React.Component {
  constructor(props) {
      super(props);
      
      this.state = {
         data:{
          code: "",
          address:"",
          name:"",
          telephone:""
         },
         dataDefault:{
          code: "",
          address:"",
          name:"",
          telephone:""
         },
         errorField:{},
         error_msg:"",
         message:""
      }
      this.changeHandle = this.changeHandle.bind(this)
      this.submit = this.submit.bind(this)
      this.reset = this.reset.bind(this)
      const {formConfig} = this.props;

      if (formConfig.type == "edit") {
        this.load_data();
      }
   };

   load_data() {
    const {token, formConfig} = this.props
    hospitalService.get_hospital(token, formConfig.id).then(
      data => {
        console.log("load_data success: ", data)
        this.setState({data, dataDefault: data})
      },
      error => {
        console.log("load_data error: ", error)
        this.setState({error_msg: "Get user info failure"})
        this.props.closeCallback(false, "Get hospital info failure");
      }
    );
   }

   changeHandle(e) {
      console.log(this.state)
      var requireField = ["name", "code"];
      const { name, value } = e.target;
      var data = this.state.data;
      var errorField = this.state.errorField

      data[name] = value;
      errorField[name] = (requireField.indexOf(name) != -1 && value == "")

      this.setState({ ...this.state, data , errorField});
   }

   checkValidate() {
    var errorField = this.state.errorField;
    var data = this.state.data;
    var rs = true;

    ["name", "code"].forEach(function(name) {
      let err = (data[name] == "");
      errorField[name] = err;
      rs &= !err;
    })

    this.setState({data, errorField})
    return rs;
   }

   submit() {
    if (this.props.formConfig.type == "add") {
      this.addAction()
    }
    else {
      this.editAction()
    }
   }

   getDataParams() {
    var data = this.state.data;
    return {
      code: data.code,
      name: data.name,
      address: data.address,
      telephone: data.telephone
    }
   }

   addAction() {
    console.log("addAction")
    if (!this.checkValidate())
      return;
    var d = this.getDataParams();
    hospitalService.create_hospital(this.props.token, d)
      .then(
          hospital => {
            this.setState({message: "Adding success"})
            this.reset();
            this.props.closeCallback(true, "Adding successfully");
          },
          error => {S
            console.log(error);
            this.setState({error_msg: "Adding failure"})
          }
        );
   }

   editAction() {
    console.log("editAction")
    if (!this.checkValidate())
      return;
    var data = this.getDataParams();
    var {formConfig, token} = this.props;
    hospitalService.update_hospital(token, formConfig.id, data)
      .then(
          hospital => {
            this.setState({message: "Updating success"})
            this.reset();
            this.props.closeCallback(true, "Updating successfully");
          },
          error => {
            console.log(error);
            this.setState({error_msg: "Updating failure"})
          }
        );
   }

   reset() {
    console.log(this.state)
    this.setState({
      dataDefault: {...this.state.dataDefault},
      data: {...this.state.dataDefault},
      errorField: {}
    })

   }

   render() {
      var {data, errorField} = this.state;
      var {error_msg, message} = this.state;

      return (
         <div>
          <Card>
              <CardBlock className="card-body">
                <Form action="" method="post">
                  {
                    error_msg? (<div className="alert alert-danger">{error_msg}</div>):("")
                  }
                  {
                    message? (<div className="alert alert-primary">{message}</div>):("")
                  }

                  <FormGroup>
                    <Label htmlFor="code">Code</Label>
                    <Input type="text" id="code" name="code" value={data.code} placeholder="Enter code.."
                      onChange={this.changeHandle} className={errorField.code?"is-invalid":""}/>
                    {
                      errorField.code?(<span className="help-block">Please enter code</span>):("")
                    }
                  </FormGroup>
                  
                  <FormGroup>
                    <Label htmlFor="name">Name</Label>
                    <Input type="text" id="name" name="name" value={data.name} placeholder="Enter name.."
                      onChange={this.changeHandle} className={errorField.name?"is-invalid":""}/>
                    {
                      errorField.name?(<span className="help-block">Please enter name</span>):("")
                    }
                  </FormGroup>

                  <FormGroup>
                    <Label htmlFor="address">Address</Label>
                    <Input type="text" id="address" name="address" value={data.address} placeholder="Enter address.."
                     onChange={this.changeHandle} className={errorField.address?"is-invalid":""}/>
                     {
                      errorField.address?(<span className="help-block">Please enter address</span>):("")
                     }
                    
                  </FormGroup>

                  <FormGroup>
                    <Label htmlFor="telephone">Telephone</Label>
                    <Input type="text" id="telephone" name="telephone" value={data.telephone} placeholder="Enter telephone.."
                     onChange={this.changeHandle} className={errorField.telephone?"is-invalid":""}/>
                     {
                      errorField.telephone?(<span className="help-block">Please enter telephone</span>):("")
                     }
                    
                  </FormGroup>

                </Form>
              </CardBlock>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" onClick={this.submit}><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger" onClick={this.reset}><i className="fa fa-ban"></i> Reset</Button>
                <Button size="sm" color="primary" className="active pull-right" onClick={(e)=>this.props.closeCallback(false)}>Cancel</Button>
              </CardFooter>

            </Card>

         </div>
      );
   }
}

export default FormHospital; 