import {benefitConstants} from '../constants'
import {benefitService} from '../services'

export const benefitActions = {
    load_benefits,
    delete_benefit,
    update_benefit
};


function load_benefits(token) {
    console.log("action load_benefits");
    return dispatch => {
        console.log("===action load");
        benefitService.load_benefits(token.id)
            .then( benefits => {
                dispatch(load_success(benefits))
            }, error=>{
                dispatch(load_failure(error));
                console.log("action load benefit error")
            })
    }
}

function delete_benefit(token, id, callback=null) {
    console.log("action delete_benefit");
    return dispatch => {
        console.log("===action delete_benefit");
        benefitService.delete_benefit(token.id, id)
            .then( count => {
                dispatch(delete_success(id))
                console.log("Delete done");
                if (callback)
                    callback(true)
            }, error=>{
                // dispatch({});
                if (callback)
                    callback(false, error)
                console.log("action delete_benefit error")
            })
    }
}

function update_benefit(token, id, benefit, callback=null) {
    console.log("action update_benefit");
    return dispatch => {
        console.log("===action update_benefit");
        benefitService.update_benefit(token, id, benefit)
            .then( benefits => {
                // dispatch(load_success(benefits))
                console.log("action update_benefit success")
                if (callback) 
                    callback(true, benefits);
                // load benefit after edit
                benefitService.load_benefits(token.id)
                    .then( benefits => {
                        dispatch(load_success(benefits))
                    }, error=>{
                        dispatch(load_failure(error));
                        console.log("action load benefit error")
                    })
            }, error=>{
                // dispatch(load_failure(error));
                console.log("action update_benefit error")
                if (callback) 
                    callback(false, error);
            })
    }
}

function load_success(benefits) {
    return { type: benefitConstants.LOAD_BENEFITS_SUCCESS, benefits }
}

function load_failure(error) {
    console.log("load_failure: ", error);
    return { type: benefitConstants.LOAD_BENEFITS_FAILURE, error_msg:"Load benefits failure" }
}

function delete_success(id) {
    return { type: benefitConstants.DELETE_BENEFIT_SUCCESS, id:id }
}
