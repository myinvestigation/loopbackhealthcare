import {authConstants} from '../constants'
import {authService} from '../services'

export const authActions = {
    login,
    logout,
    get_info
};

function get_info(token, id) {
    console.log("get_info")
    return dispatch => {

        console.log("get_info dispatch")
        authService.get_info(token, id)
            .then(
                userInfo => {
                    console.log(userInfo)
                    dispatch({ type: authConstants.USER_INFO_REQUEST_SUCCESS, userInfo: userInfo })
                }, 
                error => {
                    console.log("get_info error")
                    dispatch({ type: authConstants.USER_INFO_REQUEST_FAILURE })
                }
            )
    }
}

function login(username, password) {
    console.log("login action")
    return dispatch =>  {
        dispatch(request({ username }));
        authService.login(username, password)
            .then(
                user => { 
                    // history.push('/');
                    // login successful if there's a jwt token in the response
                    if (user && user.id) {
                        dispatch(success(user));
                        // store user details and jwt token in local storage to keep user logged in between page refreshes
                        localStorage.setItem('user', JSON.stringify(user));
                        console.log(user);
                        console.log("UserInfo:")
                        authService.get_info(user.id, user.userId)
                            .then(
                                userInfo => {
                                    console.log(userInfo)
                                    dispatch({ type: authConstants.USER_INFO_REQUEST_SUCCESS, userInfo: userInfo })
                                }, 
                                error => {
                                    console.log("get_info error")
                                    dispatch({ type: authConstants.USER_INFO_REQUEST_FAILURE })
                                }
                            )
                    }
                    else {
                        dispatch(failure(error));
                    }
                },
                error => {
                    dispatch(failure(error));
                    // dispatch(alertActions.error(error));
                }
            );
    };

    function request(user) { return { type: authConstants.LOGIN_REQUEST, user } }
    function success(user) { return { type: authConstants.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: authConstants.LOGIN_FAILURE, error } }
}

function logout(token) {
    console.log("logout action")
    return dispatch =>  {
        authService.logout(token)
            .then(
                rs => { 
                    console.log("Logout success")
                    dispatch({type: authConstants.LOGOUT_SUCCESS});
                    localStorage.clear();
                },
                error => {
                    console.log("Logout fails")
                    // dispatch(failure(error));
                    // dispatch(alertActions.error(error));
                }
            );
    };
}