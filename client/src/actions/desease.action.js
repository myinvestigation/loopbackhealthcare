import {deseaseConstants} from '../constants'
import {deseaseService} from '../services'

export const deseaseActions = {
    load_deseases,
    delete_desease
};


function load_deseases(token) {
    console.log("action load_users");
    return dispatch => {
        console.log("===action load");
        deseaseService.load_deseases(token.id)
            .then( deseases => {
                dispatch(load_success(deseases))
            }, error=>{
                dispatch(load_failure(error));
                console.log("action load user error")
            })
    }
}

function delete_desease(token, id, callback=null) {
    console.log("action delete_desease");
    return dispatch => {
        console.log("===action delete_desease");
        deseaseService.delete_desease(token.id, id)
            .then( count => {
                dispatch(delete_success(id))
                console.log("Delete done");
                if (callback)
                    callback(true)
            }, error=>{
                // dispatch({});
                if (callback)
                    callback(false, error)
                console.log("action delete_desease error")
            })
    }
}

function load_success(deseases) {
    return { type: deseaseConstants.LOAD_DESEASES_SUCCESS, deseases }
}

function load_failure(error) {
    console.log("load_failure: ", error);
    return { type: deseaseConstants.LOAD_DESEASES_FAILURE, error_msg:"Load deseases failure" }
}

function delete_success(id) {
    return { type: deseaseConstants.DELETE_DESEASE_SUCCESS, id:id }
}
