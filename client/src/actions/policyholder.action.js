import {policyholderConstants} from '../constants'
import {policyholderService} from '../services'

export const policyholderActions = {
    load_policyholders,
    delete_policyholder
};


function load_policyholders(token) {
    console.log("action load_users");
    return dispatch => {
        console.log("===action load");
        policyholderService.load_policyholders(token.id)
            .then( policyholders => {
                dispatch(load_success(policyholders))
            }, error=>{
                dispatch(load_failure(error));
                console.log("action load user error")
            })
    }
}

function delete_policyholder(token, id, callback=null) {
    console.log("action delete_policyholder");
    return dispatch => {
        console.log("===action delete_policyholder");
        policyholderService.delete_policyholder(token.id, id)
            .then( count => {
                dispatch(delete_success(id))
                console.log("Delete done");
                if (callback)
                    callback(true)
            }, error=>{
                // dispatch({});
                if (callback)
                    callback(false, error)
                console.log("action delete_policyholder error")
            })
    }
}

function load_success(policyholders) {
    return { type: policyholderConstants.LOAD_POLICYHOLDERS_SUCCESS, policyholders }
}

function load_failure(error) {
    console.log("load_failure: ", error);
    return { type: policyholderConstants.LOAD_POLICYHOLDERS_FAILURE, error_msg:"Load policyholders failure" }
}

function delete_success(id) {
    return { type: policyholderConstants.DELETE_POLICYHOLDER_SUCCESS, id:id }
}
