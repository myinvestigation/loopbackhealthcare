import {companyConstants} from '../constants'
import {companyService} from '../services'

export const companyActions = {
    load_companies,
    delete_company
};


function load_companies(token) {
    console.log("action load_users");
    return dispatch => {
        console.log("===action load");
        companyService.load_companies(token.id)
            .then( companies => {
                dispatch(load_success(companies))
            }, error=>{
                dispatch(load_failure(error));
                console.log("action load user error")
            })
    }
}

function delete_company(token, id, callback=null) {
    console.log("action delete_company");
    return dispatch => {
        console.log("===action delete_company");
        companyService.delete_company(token.id, id)
            .then( count => {
                dispatch(delete_success(id))
                console.log("Delete done");
                if (callback)
                    callback(true)
            }, error=>{
                // dispatch({});
                if (callback)
                    callback(false, error)
                console.log("action delete_company error")
            })
    }
}

function load_success(companies) {
    return { type: companyConstants.LOAD_COMPANIES_SUCCESS, companies }
}

function load_failure(error) {
    console.log("load_failure: ", error);
    return { type: companyConstants.LOAD_COMPANIES_FAILURE, error_msg:"Load companies failure" }
}

function delete_success(id) {
    return { type: companyConstants.DELETE_COMPANY_SUCCESS, id:id }
}
