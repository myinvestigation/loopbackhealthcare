import {departmentConstants} from '../constants'
import {departmentService} from '../services'

export const departmentActions = {
    load_departments,
    delete_department
};


function load_departments(token) {
    console.log("action load_departments");
    return dispatch => {
        console.log("===action load");
        departmentService.load_departments(token.id)
            .then( departments => {
                dispatch(load_success(departments))
            }, error=>{
                dispatch(load_failure(error));
                console.log("action load department error")
            })
    }
}

function delete_department(token, id, callback=null) {
    console.log("action delete_department");
    return dispatch => {
        console.log("===action delete_department");
        departmentService.delete_department(token.id, id)
            .then( count => {
                dispatch(delete_success(id))
                console.log("Delete done");
                if (callback)
                    callback(true)
            }, error=>{
                // dispatch({});
                if (callback)
                    callback(false, error)
                console.log("action delete_department error")
            })
    }
}

function load_success(departments) {
    return { type: departmentConstants.LOAD_DEPARTMENTS_SUCCESS, departments }
}

function load_failure(error) {
    console.log("load_failure: ", error);
    return { type: departmentConstants.LOAD_DEPARTMENTS_FAILURE, error_msg:"Load departments failure" }
}

function delete_success(id) {
    return { type: departmentConstants.DELETE_DEPARTMENT_SUCCESS, id:id }
}
