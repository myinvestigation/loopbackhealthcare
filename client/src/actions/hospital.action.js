import {hospitalConstants} from '../constants'
import {hospitalService} from '../services'

export const hospitalActions = {
    load_hospitals,
    delete_hospital
};


function load_hospitals(token) {
    console.log("action load_users");
    return dispatch => {
        console.log("===action load");
        hospitalService.load_hospitals(token.id)
            .then( hospitals => {
                dispatch(load_success(hospitals))
            }, error=>{
                dispatch(load_failure(error));
                console.log("action load user error")
            })
    }
}

function delete_hospital(token, id, callback=null) {
    console.log("action delete_hospital");
    return dispatch => {
        console.log("===action delete_hospital");
        hospitalService.delete_hospital(token.id, id)
            .then( count => {
                dispatch(delete_success(id))
                console.log("Delete done");
                if (callback)
                    callback(true)
            }, error=>{
                // dispatch({});
                if (callback)
                    callback(false, error)
                console.log("action delete_hospital error")
            })
    }
}

function load_success(hospitals) {
    return { type: hospitalConstants.LOAD_HOSPITALS_SUCCESS, hospitals }
}

function load_failure(error) {
    console.log("load_failure: ", error);
    return { type: hospitalConstants.LOAD_HOSPITALS_FAILURE, error_msg:"Load hospitals failure" }
}

function delete_success(id) {
    return { type: hospitalConstants.DELETE_HOSPITAL_SUCCESS, id:id }
}
