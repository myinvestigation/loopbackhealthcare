import {userConstants} from '../constants'
import {userService} from '../services'

export const userActions = {
    load_users,
    delete_user,
    create_user,
    find_user
};


function load_users() {
    console.log("action load_user");
    let user = JSON.parse(localStorage.getItem('user'));
    console.log(user);
    return dispatch => {
        console.log("===action load");
        userService.load_users(user.id)
            .then( users => {
                dispatch(load_success(users))
            }, error=>{
                // dispatch({});
                console.log("action load user error")
            })
    }
}
function delete_user(id, callback=null) {
    console.log("action delete_user");
    let token = JSON.parse(localStorage.getItem('user'));
    return dispatch => {
        console.log("===action delete_user");
        userService.delete_user(token.id, id)
            .then( count => {
                // dispatch(load_success(users))
                console.log("Delete done");
                console.log("Load after delete");
                if (callback)
                    callback(true)
                userService.load_users(token.id)
                    .then( users => {
                        dispatch(load_success(users))
                    }, error=>{
                        // dispatch({});
                        console.log("action load user error")
                    })

            }, error=>{
                // dispatch({});
                if (callback)
                    callback(false, error)
                console.log("action delete_user error")
            })
    }
}

function create_user(user) {
    console.log("action load_user");
    let token = JSON.parse(localStorage.getItem('user'));
    return dispatch => {
        console.log("===action create_user");
        userService.create_user(token.id, user)
            .then( user => {
                dispatch(create_success("User is created successful"))
                console.log("Create success user: ", user);
                // console.log("Load after create");
                // userService.load_users(token.id)
                //     .then( users => {
                //         dispatch(load_success(users))
                //     }, error=>{
                //         // dispatch({});
                //         console.log("action load user error")
                //     })
            }, error=>{
                dispatch(create_failure("Create failure"));
                console.log("action create_user error", error)
            })
    }
}

function find_user(id, callback) {
    console.log("action load_user");
    let token = JSON.parse(localStorage.getItem('user'));
    return dispatch => {
        console.log("===action create_user");
        callback({"user":"NGHIA"})
        // userService.create_user(token.id, user)
        //     .then( user => {
        //         dispatch(create_success("User is created successful"))
        //         console.log("Create success user: ", user);
        //         console.log("Load after create");
        //         userService.load_users(token.id)
        //             .then( users => {
        //                 dispatch(load_success(users))
        //             }, error=>{
        //                 // dispatch({});
        //                 console.log("action load user error")
        //             })
        //     }, error=>{
        //         dispatch(create_failure("Create failure"));
        //         console.log("action create_user error", error)
        //     })
    }
}

function load_success(users) {
    return { type: userConstants.LOAD_SUCCESS, users }
}

function create_success(message) {
    return { type: userConstants.CREATE_USER_SUCCESS, message }
}
function create_failure(error_msg) {
    return { type: userConstants.CREATE_USER_FAILURE, error_msg }
}

