import {policyConstants} from '../constants'
import {policyService, policyPlansService} from '../services'

export const policyActions = {
    load_policies,
    delete_policy,
    setCurrentPolicy,
    create_policy,
    AddBenefitCurrentPolicy,
    deleteBenefitCurrentPolicy,
    setEditFormInfo,
    load_edit_policy,
    update_policy
};


function load_policies(token) {
    console.log("action load_users");
    return dispatch => {
        console.log("===action load");
        policyService.load_policies(token.id)
            .then( policies => {
                dispatch(load_success(policies))
            }, error=>{
                dispatch(load_failure(error));
                console.log("action load user error")
            })
    }
}

function update_policy(token, id, data, plans, callback=null) {
    console.log("action update_policy");
    return dispatch => {
        console.log("===action update_policy");
        policyService.update_policy(token, id, data)
            .then( policy => {
                console.log("update_policy success")
                // if (callback) 
                //     callback(true, policy)
                policyPlansService.getPlansByPolicy(token, id)
                    .then( policyPlans => {
                        console.log("policyPlans", policyPlans)
                        policyPlans.map(function(policyPlan) {
                            policyPlansService.deletePolicyPlans(token, policyPlan.id)
                                .then(result=>{
                                    console.log("deletePolicyPlans success")
                                }, error=>{
                                    console.log("deletePolicyPlans error")
                                })
                        })
                    }, error=>{
                        console.log("getPlansByPolicy error")
                    })

                // TODO-NGHIA: 
                plans.map(function(plan){
                    plan.policyId = id
                })
                policyPlansService.addPlansPolicy(token, plans)
                    .then(policyPlans=>{
                        console.log("Add plans policy success");
                        if (callback)
                            callback(true, policy)
                    }, error => {
                        console.log("Add plans policy failure");
                        if (callback)
                            callback(false, error)
                    })

            }, error=>{
                // dispatch(load_failure(error));
                console.log("action update_policy error")
                if (callback) 
                    callback(false, error)
            })
    }
}

function load_edit_policy(token, id, callback=null) {
    console.log("action load_edit_policies", token, id);
    return dispatch => {
        console.log("===action load_edit_policies");
        policyService.get_policy(token, id)
            .then( policy => {

                if (callback) 
                    callback(true, policy);
                else
                    dispatch(setCurrentPolicy(policy))
            }, error=>{
                console.log("action load_edit_policies error")
                if (callback) 
                    callback(false, error);
            })
    }
}

function delete_policy(token, id, callback=null) {
    console.log("action delete_policy");
    return dispatch => {
        console.log("===action delete_policy");
        policyService.delete_policy(token.id, id)
            .then( count => {
                dispatch(delete_success(id))
                console.log("Delete done");
                if (callback)
                    callback(true)

                // TODO-NGHIA: Removing policyPlans should be handled on server
                policyPlansService.getPlansByPolicy(token, id)
                    .then( policyPlans => {
                        console.log("policyPlans", policyPlans)
                        policyPlans.map(function(policyPlan) {
                            policyPlansService.deletePolicyPlans(token, policyPlan.id)
                                .then(result=>{
                                    console.log("deletePolicyPlans success")
                                }, error=>{
                                    console.log("deletePolicyPlans error")
                                })
                        })
                    }, error=>{
                        console.log("getPlansByPolicy error")
                    })
            }, error=>{
                // dispatch({});
                if (callback)
                    callback(false, error)
                console.log("action delete_policy error")
            })
    }
}

function create_policy(token, policy, plans, callback=null) {
    console.log("action create_policy");
    return dispatch => {
        console.log("===action create_policy");
        policyService.create_policy(token, policy)
            .then( policy => {
                // dispatch(delete_success(id))
                console.log("Create policy success", policy);
                plans.map(function(plan){
                    plan.policyId = policy.id;
                })
                console.log("Plans", plans);
                policyPlansService.addPlansPolicy(token, plans)
                    .then(policyPlans=>{
                        console.log("Add plans policy success");
                        if (callback)
                            callback(true, policy)
                    }, error => {
                        console.log("Add plans policy failure");
                        if (callback)
                            callback(false, error)
                    })
                
            }, error=>{
                // dispatch({});
                if (callback)
                    callback(false, error)
                console.log("action create_policy error")
            })
    }
}

function AddBenefitCurrentPolicy(path, benefit) {
    console.log("action AddBenefitCurrentPolicy");
    return dispatch => {
        console.log("===action create_policy");
        dispatch(AddBenefit(path, benefit))
    }
}

function setEditFormInfo(id) {
    console.log("action setEditFormInfo");
    return dispatch => {
        console.log("===action setEditFormInfo");
        dispatch({ type: policyConstants.POLICY_SET_FORM_INFO, formInfo:{id, formType: "edit"} })
    }
}



function AddBenefit(path, benefit) {
    return { type: policyConstants.CURRENT_POLICY_ADD_BENEFIT, path, benefit }
}

function setCurrentPolicy(data) {
    return dispatch => {
        dispatch({ type: policyConstants.SET_CURRENT_POLICY, policy:data })
    }
}

function deleteBenefitCurrentPolicy(path) {
    return dispatch => {
        dispatch({ type: policyConstants.CURRENT_POLICY_DELETE_BENEFIT, path })
    }
}

function load_success(policies) {
    return { type: policyConstants.LOAD_POLICIES_SUCCESS, policies }
}

function load_failure(error) {
    console.log("load_failure: ", error);
    return { type: policyConstants.LOAD_POLICIES_FAILURE, error_msg:"Load policies failure" }
}

function delete_success(id) {
    return { type: policyConstants.DELETE_POLICY_SUCCESS, id:id }
}
