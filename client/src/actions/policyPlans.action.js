
import {policyPlansService} from '../services'

export const policyPlansActions = {
    getPolicyPlans
};


function getPolicyPlans(token, policyId, callback=null) {
    console.log("action get_policyPlans");
    return dispatch => {
        console.log("===action load");
        policyPlansService.getPlansByPolicy(token, policyId)
            .then( policyPlans => {
                console.log("action get_policyPlans success")
                if(callback) 
                    callback(true, policyPlans);
            }, error=>{
                console.log("action get_policyPlans error")
                if(callback) 
                    callback(false, policyPlans);
            })
    }
}



