import { hospitalConstants } from '../constants';

console.log("hospital.reducer.js")

var init = {
  hospitals:[]
}

export function hospital(state=init, action) {
  console.log("hospital reducer with STATE: ", state, action)
  switch (action.type) {
    case hospitalConstants.LOAD_HOSPITALS_SUCCESS:
      return {
        hospitals: action.hospitals
      };
    case hospitalConstants.LOAD_HOSPITALS_FAILURE:

      return {
        hospitals: [],
        error_msg: action.error_msg
      };
    case hospitalConstants.DELETE_HOSPITAL_SUCCESS:

      var hospitals = [...state.hospitals];
      var i = hospitals.length;
      while (i--) {
          if (hospitals[i].id == action.id) { 
              hospitals.splice(i, 1);
              break;
          } 
      }
      return {
        hospitals: hospitals
      };

    default:
      console.log("default")
      return state
  }
}