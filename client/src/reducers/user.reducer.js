import { userConstants } from '../constants';

console.log("User.reducer.js")

var init = {
  users:[],
  message: "",
  error_msg:""
}

export function user(state=init, action) {
  console.log("user reducer with state: ", state)
  switch (action.type) {
    case userConstants.LOAD_SUCCESS:
      return {
        users: action.users
      };
    case userConstants.CREATE_USER_SUCCESS:
      return {
        ...state,
        message: action.message,
        error_msg: ""
      };
    case userConstants.CREATE_USER_FAILURE:
      return {
        ...state,
        message: "",
        error_msg: action.error_msg
      };
    default:
      console.log("default")
      return state
  }
}