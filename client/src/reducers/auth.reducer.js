import { authConstants } from '../constants';

console.log("user local: ",localStorage.getItem('user'))
let user = JSON.parse(localStorage.getItem('user'));
const initialState = user ? { loggedIn: true, user , userInfo:{}} : {};

export function authentication(state=initialState, action) {
  console.log("authentication with state: ", state)
  switch (action.type) {
    case authConstants.LOGIN_REQUEST:
      return {
        loggingIn: true,
        user: action.user
      };
    case authConstants.LOGIN_SUCCESS:
      return {
        loggedIn: true,
        user: action.user
      };
    case authConstants.LOGIN_FAILURE:
      return {
        msgError: "Login faild"
      };
    case authConstants.LOGOUT_SUCCESS:
      return {loggedIn: false};
    case authConstants.USER_INFO_REQUEST_SUCCESS:
      return {...state, userInfo: action.userInfo}
    case authConstants.USER_INFO_REQUEST_FAILURE:
      return {...state, loggedIn: false}
    default:
      console.log("default")
      return state
  }
}