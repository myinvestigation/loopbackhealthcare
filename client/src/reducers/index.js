import { combineReducers } from 'redux';
import { authentication } from './auth.reducer'
import { user } from './user.reducer'
import { company } from './company.reducer'
import { department } from './department.reducer'
import { hospital } from './hospital.reducer'
import { desease } from './desease.reducer'
import { policyholder } from './policyholder.reducer'
import { benefit } from './benefit.reducer'
import { policy } from './policy.reducer'

const rootReducer = combineReducers({
  authentication,
  user,
  company,
  department,
  hospital,
  desease,
  policyholder,
  benefit,
  policy
});
export default rootReducer;