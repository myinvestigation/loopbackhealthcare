import { benefitConstants } from '../constants';

console.log("benefit.reducer.js")

var init = {
  benefits:[]
}

export function benefit(state=init, action) {
  console.log("benefit reducer with STATE: ", state, action)
  switch (action.type) {
    case benefitConstants.LOAD_BENEFITS_SUCCESS:
      return {
        benefits: action.benefits
      };
    case benefitConstants.LOAD_BENEFITS_FAILURE:

      return {
        benefits: [],
        error_msg: action.error_msg
      };
    case benefitConstants.DELETE_BENEFIT_SUCCESS:

      var benefits = [...state.benefits];
      var i = benefits.length;
      while (i--) {
          if (benefits[i].id == action.id) { 
              benefits.splice(i, 1);
              break;
          } 
      }
      return {
        benefits: benefits
      };

    default:
      console.log("default")
      return state
  }
}