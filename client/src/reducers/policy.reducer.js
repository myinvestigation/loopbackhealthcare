import { policyConstants } from '../constants';

console.log("policy.reducer.js")

var init = {
  policies:[],
  currentPolicy: {},
  formInfo: {}
}

export function policy(state=init, action) {
  console.log("policy reducer with STATE: ", state, action)
  switch (action.type) {
    case policyConstants.LOAD_POLICIES_SUCCESS:
      return {
        ...state,
        policies: action.policies
      };
    case policyConstants.LOAD_POLICIES_FAILURE:

      return {
        ...state,
        policies: [],
        error_msg: action.error_msg
      };
    case policyConstants.DELETE_POLICY_SUCCESS:

      var policies = [...state.policies];
      var i = policies.length;
      while (i--) {
          if (policies[i].id == action.id) { 
              policies.splice(i, 1);
              break;
          } 
      }
      return {
        ...state,
        policies: policies
      };
    case policyConstants.SET_CURRENT_POLICY:

      return {
        ...state,
        currentPolicy: action.policy
      }
    case policyConstants.CURRENT_POLICY_ADD_BENEFIT:
      return currentPolicyAddBenefit(state, action);
    case policyConstants.CURRENT_POLICY_DELETE_BENEFIT:
      return currentPolicyDeleteBenefit(state, action);
    case policyConstants.POLICY_SET_FORM_INFO:
      return {
        ...state,
        formInfo: action.formInfo
      }


    default:
      console.log("default")
      return state
  }
}

function getByPath(benefitGroups, path, ignore=0) {
  var rs = benefitGroups;
  for(var i=0 ; i<(path.length-ignore) ; i++) {
    rs =rs[path[i]]
  }
  return rs;
}

function currentPolicyAddBenefit(state, action) {
  var rs = {...state}
  var currentPolicy = state.currentPolicy;
  console.log("NGHIA: ",currentPolicy, action)
  var groups = [];
  currentPolicy.benefitConfig.benefitGroups.map(function(g){
    groups.push({...g})
  })
  var pointer = getByPath(groups, action.path)
  if (pointer.subBenefits)
    pointer.subBenefits.push(action.benefit);
  else if (pointer.benefits) 
    pointer.benefits.push(action.benefit);
  console.log("AFTER: ", pointer, currentPolicy, "group", groups)

  currentPolicy.benefitConfig.benefitGroups =groups;
  return {
    ...state,
    currentPolicy:{
      ...currentPolicy,
      benefitConfig: {
        ...currentPolicy.benefitConfig,
        benefitGroups: groups
      }
    }
  }
}

function currentPolicyDeleteBenefit(state, action) {
  var rs = {...state}
  var currentPolicy = state.currentPolicy;
  console.log("NGHIA: ",currentPolicy, action)
  var groups = [];
  currentPolicy.benefitConfig.benefitGroups.map(function(g){
    groups.push({...g})
  })
  var pointer = getByPath(groups, action.path, 2);
  var [last] = action.path;
  if (pointer.benefits) {
    pointer.benefits.splice(last,1)
  }
  else if (pointer.subBenefits) {
    pointer.subBenefits.splice(last,1)
  }
  // delete pointer[last]

  console.log("AFTER: ", pointer, currentPolicy, "group", groups)

  currentPolicy.benefitConfig.benefitGroups =groups;
  return {
    ...state,
    currentPolicy:{
      ...currentPolicy,
      benefitConfig: {
        ...currentPolicy.benefitConfig,
        benefitGroups: groups
      }
    }
  }
}