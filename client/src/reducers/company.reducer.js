import { companyConstants } from '../constants';

console.log("Company.reducer.js")

var init = {
  companies:[]
}

export function company(state=init, action) {
  console.log("Company reducer with STATE: ", state, action)
  switch (action.type) {
    case companyConstants.LOAD_COMPANIES_SUCCESS:
      return {
        companies: action.companies
      };
    case companyConstants.LOAD_COMPANIES_FAILURE:

      return {
        companies: [],
        error_msg: action.error_msg
      };
    case companyConstants.DELETE_COMPANY_SUCCESS:

      var companies = [...state.companies];
      var i = companies.length;
      while (i--) {
          if (companies[i].id == action.id) { 
              companies.splice(i, 1);
              break;
          } 
      }
      return {
        companies: companies
      };

    default:
      console.log("default")
      return state
  }
}