import { deseaseConstants } from '../constants';

console.log("desease.reducer.js")

var init = {
  deseases:[]
}

export function desease(state=init, action) {
  console.log("desease reducer with STATE: ", state, action)
  switch (action.type) {
    case deseaseConstants.LOAD_DESEASES_SUCCESS:
      return {
        deseases: action.deseases
      };
    case deseaseConstants.LOAD_DESEASES_FAILURE:

      return {
        deseases: [],
        error_msg: action.error_msg
      };
    case deseaseConstants.DELETE_DESEASE_SUCCESS:

      var deseases = [...state.deseases];
      var i = deseases.length;
      while (i--) {
          if (deseases[i].id == action.id) { 
              deseases.splice(i, 1);
              break;
          } 
      }
      return {
        deseases: deseases
      };

    default:
      console.log("default")
      return state
  }
}