import { departmentConstants } from '../constants';

console.log("Department.reducer.js")

var init = {
  departments:[]
}

export function department(state=init, action) {
  console.log("Department reducer with STATE: ", state, action)
  switch (action.type) {
    case departmentConstants.LOAD_DEPARTMENTS_SUCCESS:
      return {
        departments: action.departments
      };
    case departmentConstants.LOAD_DEPARTMENTS_FAILURE:

      return {
        departments: [],
        error_msg: action.error_msg
      };
    case departmentConstants.DELETE_DEPARTMENT_SUCCESS:

      var departments = [...state.departments];
      var i = departments.length;
      while (i--) {
          if (departments[i].id == action.id) { 
              departments.splice(i, 1);
              break;
          }
      }
      return {
        departments: departments
      };

    default:
      console.log("default")
      return state
  }
}