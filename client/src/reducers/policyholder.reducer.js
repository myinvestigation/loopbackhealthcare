import { policyholderConstants } from '../constants';

console.log("policyholder.reducer.js")

var init = {
  policyholders:[]
}

export function policyholder(state=init, action) {
  console.log("policyholder reducer with STATE: ", state, action)
  switch (action.type) {
    case policyholderConstants.LOAD_POLICYHOLDERS_SUCCESS:
      return {
        policyholders: action.policyholders
      };
    case policyholderConstants.LOAD_POLICYHOLDERS_FAILURE:

      return {
        policyholders: [],
        error_msg: action.error_msg
      };
    case policyholderConstants.DELETE_POLICYHOLDER_SUCCESS:

      var policyholders = [...state.policyholders];
      var i = policyholders.length;
      while (i--) {
          if (policyholders[i].id == action.id) { 
              policyholders.splice(i, 1);
              break;
          } 
      }
      return {
        policyholders: policyholders
      };

    default:
      console.log("default")
      return state
  }
}