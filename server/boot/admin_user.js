// Copyright IBM Corp. 2015,2016. All Rights Reserved.
// Node module: loopback-example-access-control
// This file is licensed under the Artistic License 2.0.
// License text available at https://opensource.org/licenses/Artistic-2.0

module.exports = function(app) {
  // var Account = app.models.accounts;
  // var Role = app.models.Role;
  // var RoleMapping = app.models.RoleMapping;

  // Account.create([
  //   {username: 'admin1', email: 'admin1@doe.com', password: 'admin1'},
  //   // {username: 'testuser', password: 'testuser', fullname: 'testuser'}
  // ], function(err, users) {
  //   if (err) return;

  //   console.log('Created users:', users);

  //   //create the admin role
  //   Role.create({
  //     name: 'admin'
  //   }, function(err, role) {
  //     if (err) return;

  //     console.log('Created role:', role);

  //     //make bob an admin
  //     role.principals.create({
  //       principalType: RoleMapping.USER,
  //       principalId: users[0].id
  //     }, function(err, principal) {
  //       if (err) return;

  //       console.log('Created principal:', principal);
  //     });
  //   });
  // });
};
