
var app = require('../server/server');
var request = require('supertest');
var assert = require('assert');
var loopback = require('loopback');

function json(verb, url) {
    return request(app)[verb](url)
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/);
  }
function logout(token, cb=null) {
  return json('post','/api/Accounts/logout?access_token='+token)
      .expect(204, function(err, res){
        assert.equal(res.body,"")
        if (cb) cb();
      });
}

//////////////////////////////////////////////

describe('Account API request', function() {
  before(function(done) {
    require('./start-server');
    done();
  });

  after(function(done) {
    app.removeAllListeners('started');
    app.removeAllListeners('loaded');
    done();
  });

  var user = {
        username: 'testuser',
        password: 'testuser'
      };

  it('Add account', function(done) {
    json('post', '/api/Accounts')
      .send(user)
      .expect(200, function(err, res) {
        assert(typeof res.body === 'object');
        assert.equal(res.body.username, user["testuser"]);
      })
      .expect(422, function(err, ress){
        done();
      });
  });


  it('Get account', function(done) {
    json('post', '/api/Accounts/login')
      .send(user)
      .expect(200, function(err, res) {
        // console.log(res.body)

        assert(typeof res.body === 'object');
        assert(res.body.id, 'must have an access token');
        var accessToken = res.body.id;
        var user_id = res.body.userId;
        json('get', '/api/Accounts/'+user_id+'?access_token=' + accessToken)
          .expect(200, function(err, res){
            // console.log(res.body)
            assert(typeof res.body === 'object');
            assert.equal(res.body.username, "testuser");
            assert.equal(res.body.id, user_id);
            // Logout
            logout(accessToken, done);
            
          });
        
      });
  });

  it('Edit account', function(done) {
    json('post', '/api/Accounts/login')
      .send(user)
      .expect(200, function(err, res) {
        // console.log(res.body)

        assert(typeof res.body === 'object');
        assert(res.body.id, 'must have an access token');
        var accessToken = res.body.id;
        var user_id = res.body.userId;

        // Edit
        var edit_data = {
          id: user_id,
          username: user["username"],
          password: user["password"],
          fullname: "User test"
        };
        json('put', '/api/Accounts/'+user_id+'?access_token=' + accessToken)
          .send(edit_data)
          .expect(200, function(err, res){
            assert(typeof res.body === 'object');

            assert.equal(res.body.id, edit_data["id"]);
            assert.equal(res.body.username, edit_data["username"]);
            assert.equal(res.body.fullname, edit_data["fullname"]);
            // Logout
            logout(accessToken, done);
          });
        
      });
  });


  it('Get list account', function(done) {
    json('post', '/api/Accounts/login')
      .send({username:"admin1", password:"admin1"})
      .expect(200, function(err, res) {
        // console.log(res.body)

        assert(typeof res.body === 'object');
        assert(res.body.id, 'must have an access token');
        var accessToken = res.body.id;
        var user_id = res.body.userId;
        json('get', '/api/Accounts?access_token=' + accessToken)
          .expect(200, function(err, res){
            assert(typeof res.body === 'object');
            assert(Array.isArray(res.body))
            // Logout
            logout(accessToken, done);
          });
        
      });
  });

});

// describe('Unexpected Usage', function(){
//   it('should not crash the server when posting a bad id', function(done){
//     json('post', '/api/users/foobar')
//       .send({})
//       .expect(404, done);
//   });
// });
